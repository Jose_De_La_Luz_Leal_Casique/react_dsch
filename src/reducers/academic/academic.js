import {
    GET_ACADEMIC_STATISTICS, FAIL_ACADEMIC_STATISTICS,
    GET_ACADEMIC_STATISTICS_TOTALS, FAIL_ACADEMIC_STATISTICS_TOTALS,
    LOGOUT_SUCCESS, AUTH_ERROR, GET_USERS_PRIVILEGES_VIEW, POST_USERS_PRIVILEGES_VIEW
} from '../../actions/types';


const initialState = {
    load: false,
    inProcess: false,
    statistics: [],
    totals: [],
    users: [],
    msg: ''
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_ACADEMIC_STATISTICS:
            return {
                ...state,
                load: true,
                statistics: action.payload,
                totals: []
            };

        case GET_USERS_PRIVILEGES_VIEW:
            return {
                ...state,
                users: action.payload
            }
        case POST_USERS_PRIVILEGES_VIEW:
            return {
                ...state,
                msg: action.msg
            }

        case GET_ACADEMIC_STATISTICS_TOTALS:
            return {
                ...state,
                load: true,
                totals: action.payload
            }

        case FAIL_ACADEMIC_STATISTICS:
            return {
                ...state,
                load: false,
                statistics: [],
            };

        case FAIL_ACADEMIC_STATISTICS_TOTALS:
            return {
                ...state,
                load: false,
                totals: []
            };

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                load: false,
                inProcess: false,
                statistics: [],
                totals: [],
                users: [],
                msg: ''
            }

        default:
            return {
                ...state
            }
    }
}