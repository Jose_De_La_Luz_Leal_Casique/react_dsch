import {
    FAIL_LOAD_DEPARTAMENT, SUCCESS_LOAD_DEPARTAMENT, LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    departaments: null,
    load: false
}


export default function (state = initialState, action){
    switch (action.type){

        case FAIL_LOAD_DEPARTAMENT:
            return {
                ...state,
                load: false
            };

        case SUCCESS_LOAD_DEPARTAMENT:
            return {
                ...state,
                load: true,
                departaments: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                departaments: null,
                load: false
            }

        default:
            return {
                ...state
            }
    }
}