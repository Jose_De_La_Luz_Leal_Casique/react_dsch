import {
    AUTH_ERROR,
    LOGOUT_SUCCESS,
    GET_MY_lIST_SABBATICAL,
    GET_DETAIL_SABBATICAL,
    GET_lIST_SABBATICAL_APPROVED,
    GET_lIST_SABBATICAL_REQUESTS,
    EDIT_REQUEST_SABBATICAL,
    SEND_DATA_APPROVAL_SABBATICAL,
    SEND_DATA_LIST_APPROVAL_SABBATICAL,
    GET_LIST_MODIFICATION_SABBATICAL,
} from '../../actions/types';


const initialState = {
    months: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
    ],
    list: [],
    sabbatical: null,
    request: [],
}

export default function (state = initialState, action) {
    switch (action.type){

        case SEND_DATA_LIST_APPROVAL_SABBATICAL:
        case GET_LIST_MODIFICATION_SABBATICAL:
            return {
                ...state,
                request: action.payload,
            }

        case GET_DETAIL_SABBATICAL:
        case EDIT_REQUEST_SABBATICAL:
        case SEND_DATA_APPROVAL_SABBATICAL:
            return {
                ...state,
                sabbatical: action.payload,
            }

        case GET_MY_lIST_SABBATICAL:
        case GET_lIST_SABBATICAL_APPROVED:
        case GET_lIST_SABBATICAL_REQUESTS:
            return {
                ...state,
                list: action.payload,
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: [],
                sabbatical: null,
                request: [],
            }

        default:
            return {
                ...state
            }
    }
}