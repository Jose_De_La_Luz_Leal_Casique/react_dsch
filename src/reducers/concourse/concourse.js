import {
    LOGOUT_SUCCESS, AUTH_ERROR, 
    GET_LIST_CONCOURSE, 
    GET_CONCOURSE, 
    ADD_CAUSAL_CONCOURSE,
    ADD_DICTUM_OR_NOTIFICATION_CONCOURSE, 
    ADD_FILE_EXTEND_CONCOURSE, 
    ADD_FIRST_EXTENSION_CONCOURSE,
    ADD_NUMBER_CONCOURSE, 
    ADD_OBSERVATION_CONCOURSE, 
    ADD_PUBLICATION_CONCOURSE, 
    CREATE_CONCOURSE,
    ADD_SECOND_EXTENSION_CONCOURSE, 
    CREATE_ASPIRANT, 
    ADD_FILE_GRADE_OR_ANNEXED_ASPIRANT,
    ADD_WINNER_TO_CONCOURSE, 
    DELETE_ASPIRANT_FOR_CONCOURSE,
    ACTIVE_OR_INACTIVE_CONCOURSE,
    ADD_TEACHER_FOR_CONCOURSE,
} from '../../actions/types';


const initialState = {
    list: [],
    edit: null,
    load: false,
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_CONCOURSE:
            return {
                ...state,
                list: action.payload,
                load: true
            }

        case GET_CONCOURSE:
        case ADD_CAUSAL_CONCOURSE:
        case ADD_DICTUM_OR_NOTIFICATION_CONCOURSE:
        case ADD_FILE_EXTEND_CONCOURSE:
        case ADD_FIRST_EXTENSION_CONCOURSE:
        case ADD_SECOND_EXTENSION_CONCOURSE:
        case ADD_NUMBER_CONCOURSE:
        case ADD_OBSERVATION_CONCOURSE:
        case ADD_PUBLICATION_CONCOURSE:
        case ACTIVE_OR_INACTIVE_CONCOURSE:
        case CREATE_ASPIRANT:
        case ADD_FILE_GRADE_OR_ANNEXED_ASPIRANT:
        case ADD_WINNER_TO_CONCOURSE:
        case DELETE_ASPIRANT_FOR_CONCOURSE:
        case ADD_TEACHER_FOR_CONCOURSE:
            return {
                ...state,
                edit: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: [],
                edit: null,
                load: false,
            }

        default:
            return {
                ...state
            }
    }
}
