import { combineReducers } from 'redux';
import auth from './authetication/auth';
import privileges from "./authetication/privileges";
import errors from './errors';
import messages from './messages';
import departament from "./academic/departaments";
import study from './academic/study';
import academic from './academic/academic';
import edit from './academic/edit';
import history from './general/history';
import years from './academic/years';
import scholarship from "./scholarship/scholarship";
import complete from "./scholarship/complete";
import editorial from './editorial/editorial';
import editorialpersonal from "./general/editorialpersonal";
import course from './academic/course';
import concourse from "./concourse/concourse";
import production from "./production/production";
import type_report from "./production/type_report";
import adjustments from "./adjustments/adjustments";
import investigation from "./investigation/investigation";
import sabbatical from "./sabbatical/sabbatical";


export default combineReducers({
    auth,
    privileges,
    errors,
    messages,
    departament,
    study,
    academic,
    edit,
    history,
    years,
    scholarship,
    complete,
    editorial,
    editorialpersonal,
    course,
    concourse,
    production,
    type_report,
    adjustments,
    investigation,
    sabbatical,
});
