import {
    GET_SCHOLARSHIP_YEAR, FAIL_SCHOLARSHIP_YEAR,
    GET_SCHOLARSHIP_NUMBER, FAIL_SCHOLARSHIP_NUMBER,
    GET_SCHOLARSHIP_OPTIONS, FAIL_SCHOLARSHIP_OPTIONS,
    LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    load: false,
    scholarship: [],
    options: []
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_SCHOLARSHIP_YEAR:
        case GET_SCHOLARSHIP_NUMBER:
            return {
                ...state,
                load: true,
                scholarship: action.payload
            };

        case FAIL_SCHOLARSHIP_YEAR:
        case FAIL_SCHOLARSHIP_NUMBER:
            return {
                ...state,
                load: false,
                scholarship: null
            };

        case GET_SCHOLARSHIP_OPTIONS:
            return {
                ...state,
                options: action.payload
            }

        case FAIL_SCHOLARSHIP_OPTIONS:
            return {
                ...state,
                options: []
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                load: false,
                scholarship: [],
                options: []
            }

        default:
            return {
                ...state
            }
    }
}