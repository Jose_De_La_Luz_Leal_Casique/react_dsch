import axios from 'axios';
import {
    ADD_INFORMATION_ADDITIONAL, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const add_information_additional = (pk, data) => (dispatch) => {

    axios.post(`${SERVER}/investigation/additional/${pk}/`,data,  configAuthToken())
        .then((res) => {
            dispatch({type: ADD_INFORMATION_ADDITIONAL});
            dispatch(createMessage({registerFile: res.data.msg}));
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 400:
                        if (err.response.data.register) dispatch(
                            returnErrors(
                                'No. de Registro de Proyecto: '
                                + err.response.data.register, 400));

                        if (err.response.data.session) dispatch(
                            returnErrors(
                                'No. De Sesión en la que fue aprobado: '
                                + err.response.data.session, 400));

                        if (err.response.data.agreement) dispatch(
                            returnErrors('No. De Acuerdo con el que se aprobó: '
                                + err.response.data.agreement, 400));

                        if (err.response.data.created) dispatch(
                            returnErrors('Fecha de aprobación: '
                                + err.response.data.created, 400));

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_information_additional;
