import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR,
    FINALIZED_PROCESS_EXTENSION
} from "../types";
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';


export const finalized_process_extension_to_project = (pk) => (dispatch) => {

    axios.delete(`${SERVER}/investigation/terminate/extension/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: FINALIZED_PROCESS_EXTENSION, payload: res.data,
            });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default finalized_process_extension_to_project;
