import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, AUTH_ERROR, ADD_ADVANCE_TO_PROJECT} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_advance_to_project = (data, pk) => (dispatch) => {

    axios.post(`${SERVER}/investigation/advance/project/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Informe Almacenado Correctamente');
            dispatch({ type: ADD_ADVANCE_TO_PROJECT, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default add_advance_to_project;
