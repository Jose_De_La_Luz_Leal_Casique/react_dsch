import axios from 'axios';
import {
    SERVER_CONNECTION_REFUSED, AUTH_ERROR, GET_LETTERS_PROJECT_INVESTIGATION
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_letters_project_investigation = (pk) => (dispatch) => {
    dispatch({
        type: GET_LETTERS_PROJECT_INVESTIGATION,
        payload: [],
        load: false
    })

    axios.get(`${SERVER}/investigation/file/letters/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_LETTERS_PROJECT_INVESTIGATION,
                payload: res.data,
                load: true
            })
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_letters_project_investigation;
