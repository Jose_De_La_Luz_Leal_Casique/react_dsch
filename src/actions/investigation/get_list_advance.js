import axios from 'axios';
import {
    SERVER_CONNECTION_REFUSED, AUTH_ERROR, GET_LIST_ADVANCE_TO_PROJECT
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_advance = (pk) => (dispatch) => {
    axios.get(`${SERVER}/investigation/advance/list/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_LIST_ADVANCE_TO_PROJECT,
                payload: res.data.results,
                load: true
            })
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_advance;
