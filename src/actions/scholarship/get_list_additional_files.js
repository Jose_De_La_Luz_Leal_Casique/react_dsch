import axios from 'axios';
import {
    LIST_FILES_ADDITIONAL, SERVER_CONNECTION_REFUSED, AUTH_ERROR, NOT_LIST_FILES_ADDITIONAL
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_additional_files = (pk) => (dispatch) => {
    axios.get(`${SERVER}/scholarship/list-file-additional/${pk}/`, configAuthToken())
        .then((res) => {
            switch (res.status){
                case 204:
                    dispatch({ type: NOT_LIST_FILES_ADDITIONAL});
                    break;

                case 200:
                    dispatch({ type: LIST_FILES_ADDITIONAL, payload: res.data });
                    break
                default:
                    console.log(res.data);
            }

        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 404:
                        break;
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break
                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_additional_files;
