import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {
    SERVER_CONNECTION_REFUSED, AUTH_ERROR, LIST_FILES_ADDITIONAL, NOT_LIST_FILES_ADDITIONAL
} from "../types";
import { SERVER } from '../server';
import {configAuthToken} from '../configAuthToken';


export const deleted_file_additional= (pk) => (dispatch) => {
    axios.delete(`${SERVER}/scholarship/delete-file-additional/${pk}/`, configAuthToken())
        .then((res) => {
            switch (res.status){
                case 200:
                    dispatch({ type: LIST_FILES_ADDITIONAL, payload: res.data});
                    break;
                case 204:
                    dispatch({ type: NOT_LIST_FILES_ADDITIONAL});
                    break;
                default:
                    console.log(res);
            }
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        if (err.response.status === 404) dispatch(
                            createMessage({NotFound: 'NO HAY COINCIDENCIAS'}));
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default deleted_file_additional;
