import axios from 'axios';
import {AUTH_ERROR, GET_EDIT_REGISTER_SCHOLARSHIP, SERVER_CONNECTION_REFUSED} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_scholarship = (pk) => (dispatch) => {

    const url = `${SERVER}/scholarship/retrieve-scholarship/${pk}/`;
    axios.get(url, configAuthToken())
        .then((res) => {
            dispatch({ type: GET_EDIT_REGISTER_SCHOLARSHIP, payload: res.data });
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 404:
                        dispatch(createMessage({NotFound: 'NO HAY COINCIDENCIAS'}))
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_scholarship;
