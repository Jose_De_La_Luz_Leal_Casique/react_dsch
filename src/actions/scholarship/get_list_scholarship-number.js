import axios from 'axios';
import {
    GET_SCHOLARSHIP_NUMBER, FAIL_SCHOLARSHIP_NUMBER, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_scholarship_number = (number, request) => (dispatch) => {

    const url = `${SERVER}/scholarship/list-scholarship-number/${number}/${request}/`;
    axios.get(url, configAuthToken())
        .then((res) => {
            dispatch({ type: GET_SCHOLARSHIP_NUMBER, payload: res.data.results });
        })
        .catch((err) => {
            try {
                dispatch({type: FAIL_SCHOLARSHIP_NUMBER});
                switch (err.response.status){
                    case 404:
                        dispatch(createMessage({NotFound: 'NO HAY COINCIDENCIAS'}))
                        break;
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break
                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_scholarship_number;
