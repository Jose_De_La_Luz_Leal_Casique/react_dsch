import axios from 'axios';
import { createMessage, returnErrors } from '../messages';
import {
    IN_PROCESS, LOGIN_SUCCESS, LOGIN_FAIL, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import { SERVER } from '../server';


export const login = (number, password) => (dispatch) => {
    dispatch({type: IN_PROCESS});
    const config = {
        'headers': {
            'Content-Type': 'application/json' }
    }

    const data = JSON.stringify({number, password});

    axios.post(`${SERVER}/authentication/login/`, data, config)
        .then((res) => {
            dispatch({ type: LOGIN_SUCCESS, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.number) dispatch(
                            returnErrors('No. Económico: '+err.response.data.number, 400));

                        if (err.response.data.password) dispatch(
                            returnErrors('Contraseña: '+err.response.data.password, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(createMessage({detail: 'Error'}));
                        break;

                    default:
                        console.log(err.response);
                    }
                dispatch({ type: LOGIN_FAIL });
            } catch (e){
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
};

export default login;


