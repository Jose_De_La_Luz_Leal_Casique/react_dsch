import axios from 'axios';
import {createMessage, returnErrors} from '../messages';
import {AUTH_ERROR, LOGOUT_SUCCESS, SERVER_CONNECTION_REFUSED} from '../types';
import { SERVER } from '../server';
import {configAuthToken} from "../configAuthToken";

export const logout = () => (dispatch) => {
    const refresh = localStorage.getItem('refresh');
    const data = JSON.stringify({refresh });

    axios.post(`${SERVER}/authentication/logout/`, data, configAuthToken())
        .then((res) => {
            dispatch({type: LOGOUT_SUCCESS});
        })
        .catch((err) =>{
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.refresh) dispatch(
                            returnErrors(err.response.data, err.response.status));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
};