import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const send_office_reception = (pk, data) => (dispatch) => {

    axios.post(`${SERVER}/sabbatical/report/office/reception/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Informe guardado correctamente'}));
        })
        .catch((err) => {
            try{
                switch (err.response.status){

                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));

                        if (err.response.data.office) dispatch(
                            returnErrors('Oficio: ' + err.response.data.office, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default send_office_reception;
