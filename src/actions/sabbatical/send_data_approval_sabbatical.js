import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR,
    SEND_DATA_APPROVAL_SABBATICAL,
    SEND_DATA_LIST_APPROVAL_SABBATICAL
} from "../types";

import { SERVER } from '../server';
import { configAuthToken  } from '../configAuthToken';


export const send_data_approval_sabbatical = (pk, data) => (dispatch) => {
    axios.post(`${SERVER}/sabbatical/data/approval/${pk}/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información guardada correctamente'}));

            if (res.status === 200){
                dispatch({
                    type: SEND_DATA_LIST_APPROVAL_SABBATICAL,
                    payload: res.data
                })

            } else if (res.status === 202){
                 dispatch({
                    type: SEND_DATA_APPROVAL_SABBATICAL,
                    payload: res.data,
                });
            }

        })
        .catch((err) => {
            try{
                switch (err.response.status){

                    case 400:
                        if (err.response.data.register) dispatch(
                            returnErrors(err.response.data.register, 401));

                        if (err.response.data.session) dispatch(
                            returnErrors(err.response.data.session, 401));

                        if (err.response.data.agreement) dispatch(
                            returnErrors(err.response.data.agreement, 401));

                        if (err.response.data.created) dispatch(
                            returnErrors(err.response.data.created, 401));

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default send_data_approval_sabbatical;
