import axios from 'axios';
import {
    AUTH_ERROR, GET_REPORT_PERSONAL_FOR_USER, SERVER_CONNECTION_REFUSED
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_report_personal = (year, user) => (dispatch) => {

    axios.get(`${SERVER}/production/get-report-personal/${year}/${user}`, configAuthToken())
        .then((res) => {
            dispatch({
                payload: res.data,
                type: GET_REPORT_PERSONAL_FOR_USER
            });
        })
        .catch((err) => {

            try {
                switch (err.response.status) {
                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response.data)
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_report_personal;
