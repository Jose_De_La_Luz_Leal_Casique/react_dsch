import axios from 'axios';
import {
    GET_HISTORY_ACADEMIC_FOR_USER, FAIL_HISTORY_ACADEMIC_FOR_USER, SERVER_CONNECTION_REFUSED
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_history_academic_for_user = () => (dispatch) => {

    axios.get(`${SERVER}/academic/list-history-acadedic-for-user/`, configAuthToken())
        .then((res) => {
            if (res.data.results.length > 0){
                dispatch({
                    payload: res.data.results,
                    type: GET_HISTORY_ACADEMIC_FOR_USER
                });
            }
        })
        .catch((err) => {

            try {
                if (err.response.status === 404){
                    dispatch(createMessage({ NotFound: 'NO HAY COINCIDENCIAS CON EL USUARIO'}));

                }else {
                    dispatch(returnErrors(err.response.data, err.response.status));
                    dispatch({type: FAIL_HISTORY_ACADEMIC_FOR_USER});
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_history_academic_for_user;
