import axios from 'axios';

import { returnErrors, createMessage } from '../messages';
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';
import {
    ADD_APPROACH_SUCCESS, ADD_APPROACH_ERROR, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from "../types";


export const add_approach = (data) => (dispatch) => {

    axios.post(`${SERVER}/adjustments/approach/add/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch({ type: ADD_APPROACH_SUCCESS });
            alert("Información Almacenada Correctamente");
            return true;
        })
        .catch((err) => {
            try{
                dispatch({ type: ADD_APPROACH_ERROR });
                switch (err.response.status){
                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    case 400:
                        if (err.response.data.detail) alert(
                            err.response.data.detail
                        );

                        if (err.response.data.code) alert( err.response.data.code);

                        if (err.response.data.description) alert(
                            "AGREGAR DESCRPCIÓN"
                        );
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break
                    case 406:
                        dispatch(createMessage({detailFile: err.response.data.file}));
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
                return false;
            }
        });


}

export default add_approach;
