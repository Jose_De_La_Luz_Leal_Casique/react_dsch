import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, ADD_PRODUCTION_BOOK_REVIEW } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_book_review = (data) => (dispatch) => {

    axios.post(`${SERVER}/production/review/add/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: res.data.detail}));
            dispatch({ type: ADD_PRODUCTION_BOOK_REVIEW });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.user) dispatch(
                            returnErrors("Autor: "+err.response.data.user, 400));

                        if (err.response.data.title) dispatch(
                            returnErrors("Título de reseña: "+err.response.data.title, 400));

                        if (err.response.data.publication) dispatch(
                            returnErrors("Nombre de revista: "+err.response.data.publication, 400));

                        if (err.response.data.book) dispatch(
                            returnErrors("Título de libro: "+err.response.data.book, 400));
                        break;

                    case 406:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 406));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_book_review;
