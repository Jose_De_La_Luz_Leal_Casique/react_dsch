import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, ADD_PRODUCTION_NEWSPAPER_PUBLICATION } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_newspaper_publication = (data) => (dispatch) => {

    axios.post(`${SERVER}/production/newspaper/add/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: res.data.detail}));
            dispatch({ type: ADD_PRODUCTION_NEWSPAPER_PUBLICATION });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.title) dispatch(
                            returnErrors("Título de artículo: "+err.response.data.title, 400));

                        if (err.response.data.newspaper_name) dispatch(
                            returnErrors("Nombre de Periódico: "+err.response.data.newspaper_name, 400));

                        if (err.response.data.date) dispatch(
                            returnErrors("Fecha: "+err.response.data.date, 400));

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 406:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 406));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_newspaper_publication;
