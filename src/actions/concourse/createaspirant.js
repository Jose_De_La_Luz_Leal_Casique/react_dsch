import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, AUTH_ERROR, CREATE_ASPIRANT} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const create_aspirant = (data, pk) => (dispatch) => {

    axios.post(`${SERVER}/concourse/aspirants/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Aspirante Creado Correctamente');
            dispatch({ type: CREATE_ASPIRANT, payload: res.data});
        })

        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.name) dispatch(
                            returnErrors(
                                "Folio de convocatoria: " + err.response.data.name, 400));

                        if (err.response.data.request) dispatch(
                            returnErrors(
                                "Descripción: " + err.response.data.request, 400));

                        if (err.response.data.errors) dispatch(
                            returnErrors(err.response.data.errors, 400));

                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información',
                            500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default create_aspirant;
