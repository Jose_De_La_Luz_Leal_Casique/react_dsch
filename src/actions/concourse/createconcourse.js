import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, AUTH_ERROR, CREATE_CONCOURSE} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const create_concourse = (data) => (dispatch) => {

    axios.post(`${SERVER}/concourse/create/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch({ type: CREATE_CONCOURSE, payload: res.data});
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
        })

        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.code) dispatch(
                            returnErrors(
                                "Folio de convocatoria: " + err.response.data.code, 400));

                        if (err.response.data.description) dispatch(
                            returnErrors(
                                "Descripción: " + err.response.data.description, 400));

                        if (err.response.data.date_created) dispatch(
                            returnErrors(err.response.data.date_created, 400));

                        if (err.response.data.date_send) dispatch(
                            returnErrors(err.response.data.date_send, 400));

                        if (err.response.data.announcement) dispatch(
                            returnErrors(err.response.data.announcement, 400));

                        if (err.response.data.errors) dispatch(
                            returnErrors(err.response.data.errors, 400));

                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información',
                            500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default create_concourse;
