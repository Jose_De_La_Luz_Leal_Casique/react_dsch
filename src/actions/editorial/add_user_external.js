import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';


export const add_user_external = (name, paternal_surname, maternal_surname, mail_personal) => (dispatch) => {

    const data = JSON.stringify({name, paternal_surname, maternal_surname, mail_personal});

    axios.post(`${SERVER}/editorial/add-profile-external/`, data, configAuthToken())
        .then((res) => {
            alert('Usuario '+res.data.name+' creado correctamente, Número Económico: '+res.data.user);
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.mail_personal) alert(
                            'Email Personal: '+err.response.data.mail_personal);

                        if (err.response.data.name) alert(
                            'Nombre de Usuario: '+err.response.data.name);

                        if (err.response.data.paternal_surname) alert(
                            'Apellido Paterno'+err.response.data.paternal_surname);

                        if (err.response.data.maternal_surname) alert(
                            'Apellido Materno: '+err.response.data.maternal_surname);

                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default add_user_external;
