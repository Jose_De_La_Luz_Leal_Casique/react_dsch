import axios from 'axios';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR } from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const create_request_editorial = (title, author, date_created) => (dispatch) => {

    const data = JSON.stringify({title, author, date_created});

    axios.post(`${SERVER}/editorial/create-request-book/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Guardado Correctamente'}));
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 400:
                        console.log(err.response)
                        if (err.response.data.errors) dispatch(
                            returnErrors(err.response.data.errors, 400));
                        if (err.response.data.title) dispatch(
                            returnErrors("Título de Obra: "+err.response.data.title, 400));
                        if (err.response.data.author) dispatch(
                            returnErrors("Autor de Obra: "+err.response.data.author, 400));
                        if (err.response.data.date_created) dispatch(
                            returnErrors("Fecha de Entrega: "+err.response.data.date_created, 400));
                        break

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default createMessage;
