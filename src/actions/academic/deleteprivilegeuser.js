import axios from 'axios';
import {DELETE_USERS_PRIVILEGES_VIEW, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const delete_user_privilege = (pk) => (dispatch) => {

    axios.delete(`${SERVER}/academic/user/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: DELETE_USERS_PRIVILEGES_VIEW
            });
            dispatch(createMessage({
                registerStatistics: 'Información Guardada Correctamente'}));
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default delete_user_privilege;
