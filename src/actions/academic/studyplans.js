import axios from 'axios';
import {SUCCESS_LOAD_STUDY_PLANS, FAIL_LOAD_STUDY_PLANS, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const loadStudyPlan = () => (dispatch) => {

    axios.get(`${SERVER}/academic/list-study-plan/`, configAuthToken())
        .then((res) => {
            dispatch({ type: SUCCESS_LOAD_STUDY_PLANS, payload: res.data.results });
        })
        .catch((err) => {
            try{
                dispatch({ type: FAIL_LOAD_STUDY_PLANS });
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default loadStudyPlan;
