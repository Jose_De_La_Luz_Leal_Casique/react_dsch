import axios from 'axios';
import {SUCCESS_LOAD_YEARS, FAIL_LOAD_YEARS, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';

export const loadYears = () => (dispatch) => {
    axios.get(`${SERVER}/academic/list-years/`, configAuthToken())
        .then((res) => {
            dispatch({ type: SUCCESS_LOAD_YEARS, payload: res.data.results });
        })
        .catch((err) => {
            try{
                dispatch({ type: FAIL_LOAD_YEARS });
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default loadYears;
