import axios from 'axios';
import {SUCCESS_LOAD_COURSES, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';

export const loadCourses = (filter) => (dispatch) => {
    axios.get(`${SERVER}/academic/list-course/${filter}/`, configAuthToken())
        .then((res) => {
            dispatch({ type: SUCCESS_LOAD_COURSES, payload: res.data });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        dispatch({ type: SUCCESS_LOAD_COURSES, payload: []});
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default loadCourses;
