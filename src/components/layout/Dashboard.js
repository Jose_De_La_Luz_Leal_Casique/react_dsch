import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class Dashboard extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    };

    render() {

        if (!this.props.auth.verified){
            return <Redirect to='/reset-password' />;

        } else {
            if (this.props.auth.permissions !== null){

                if (this.props.auth.permissions.includes('authentication.admin_exterior')) {
                    return <Redirect to='/view-history-editorial' />;

                } else {
                    return <Redirect to='/profile' />;
                }

            } else {
                return <Redirect to='/login' />;
            }
        }

        return (
            <div>
                <h2>Sistema de Información de la División de Ciencias Sociales y Humanidades</h2>
                <Link to="/view-history-academic">Ver Actividad Académica Personal</Link>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
});

export default connect(mapStateToProps, {})(Dashboard);