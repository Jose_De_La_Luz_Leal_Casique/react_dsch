import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminProduction extends Component {

    render() {

        return (
            <ul>
                <h5><strong>Producción Académica</strong></h5>
                <li>
                    <Link to="/production-file">+ Registrar Información</Link>
                </li>
                <li>
                    <Link to="/production">+ Ver Producción Académica</Link>
                </li>
            </ul>
        );
    }
}

export default MenuAdminProduction;