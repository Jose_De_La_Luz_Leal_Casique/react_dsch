import React, { Component } from 'react';
import {Link} from "react-router-dom";


export class MenuAdminRoot extends Component {
    render() {
        return (
            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Academia
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/academic" className="dropdown-item">
                                Registrar Información
                            </Link>
                            <Link to="/academic-trimester" className="dropdown-item">
                                Registros Trimestrales
                            </Link>
                            <Link to="/academic-totals" className="dropdown-item">
                                Registros Anuales
                            </Link>
                        </div>
                    </div>
                </li>
                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group ">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Becas
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/scholarship" className="dropdown-item">
                                Registrar Solicitud
                            </Link>
                            <Link to="/scholarship-year" className="dropdown-item">
                                Ver Solicitudes por Años
                            </Link>
                            <Link to="/scholarship-number" className="dropdown-item">
                                Ver Solicitudes por Profesor
                            </Link>
                        </div>
                    </div>
                </li>

                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Editorial
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/editorial" className="dropdown-item">
                                Registrar Solicitud
                            </Link>
                            <Link to="/editorial-list" className="dropdown-item">
                                Listar Solicitudes
                            </Link>
                            <Link to="/editorial-view" className="nav-link">
                                    Listar Solicitudes - Consejo
                            </Link>
                        </div>
                    </div>
                </li>

                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Concursos
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/concourse" className="dropdown-item">
                                Registrar Concuro
                            </Link>
                            <Link to="/concourse-list" className="dropdown-item">
                                Ver Concursos
                            </Link>
                        </div>
                    </div>
                </li>
                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Producción
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/production-file" className="dropdown-item">
                                Registrar Información
                            </Link>
                            <Link to="/production" className="dropdown-item">
                                Ver Producción Académica
                            </Link>
                        </div>
                    </div>
                </li>
            </ul>
        );
    }
}

export default MenuAdminRoot;