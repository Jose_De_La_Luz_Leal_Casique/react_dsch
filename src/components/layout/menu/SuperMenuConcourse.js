import React from "react"
import PropTypes from "prop-types";

import MenuAdminConcourseCurricular from "./MenuAdminConcourseCurricular";
import MenuAdminConcourseChair from "./MenuAdminConcourseChair";
import MenuAdminConcourseOpposition from "./MenuAdminConcourseOpposition";
import MenuAdminConcourseVisitor from "./MenuAdminConcourseVisitor";


export class SuperMenuConcourse extends  React.Component {

     static propTypes = {
        privileges: PropTypes.array
    };

    render() {
        return (
            <li className="submenu">
                <a style={{"cursor": "pointer"}}
                   title="Concursos - Facultad de Ciencias Sociales y Humanidades">
                    Concursos
                </a>
                <ul className="megamenu">
                    {
                        this.props.privileges.includes('authentication.admin_concourse_curricular')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            |
                        this.props.privileges.includes('authentication.view_concourse')
                            ?
                            <MenuAdminConcourseCurricular privileges={this.props.privileges} />
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('authentication.admin_concourse_chair')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminConcourseChair privileges={this.props.privileges} />
                            :
                            null

                    }

                    {
                        this.props.privileges.includes('authentication.admin_concourse_opposition')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminConcourseOpposition privileges={this.props.privileges} />
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('authentication.admin_concourse_visitor')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminConcourseVisitor privileges={this.props.privileges} />
                            :
                            null
                    }
                </ul>
            </li>
        )
    }

}

export default SuperMenuConcourse;