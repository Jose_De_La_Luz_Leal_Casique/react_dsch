import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminConcourseVisitor extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    render() {


        return (
            <ul>
                <h5><strong>Profesor Visitante</strong></h5>
                {
                    this.props.privileges.includes('authentication.admin_concourse_curricular')
                        |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/visitor">+ Registrar Concurso</Link>
                        </li>
                        :
                        null
                }

                <li>
                    <Link to="/visitor-list">+ Ver Concursos </Link>
                </li>
            </ul>
        )
    }
}

export default MenuAdminConcourseVisitor;