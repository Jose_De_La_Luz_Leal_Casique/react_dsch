import React from "react"
import PropTypes from "prop-types";

import MenuAdminAcademic from "./MenuAdminAcademic";
import MenuAdminAdjustments from "./MenuAdminAdjustments";
import MenuAdminProduction from "./MenuAdminProduction";
import MenuAdminScholarShip from "./MenuAdminScholarShip";



export class SuperMenuAcademic extends  React.Component {

     static propTypes = {
        privileges: PropTypes.array,
    };

    render() {
        return (
            <li className="submenu">
                <a style={{"cursor": "pointer"}}
                   title="Academia - Facultad de Ciencias Sociales y Humanidades">
                    Academia
                </a>
                <ul className="megamenu">
                    {
                        this.props.privileges.includes('authentication.admin_academic')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            |
                        this.props.privileges.includes('authentication.view_academic_humanities')
                            |
                        this.props.privileges.includes('authentication.view_academic_social_sciences')
                            |
                        this.props.privileges.includes('authentication.view_academic_institutional_studies')
                            |
                        this.props.privileges.includes('authentication.view_academic_general')
                            ?
                            <MenuAdminAcademic privileges={this.props.privileges} />
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('authentication.admin_production')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminProduction  />
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('authentication.admin_scholarship')
                        |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminScholarShip />
                            :
                            null
                    }

                     {
                        this.props.privileges.includes('authentication.admin_adjustments')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminAdjustments privileges={this.props.privileges}/>
                            :
                            null
                    }

                </ul>
            </li>
        )
    }

}

export default SuperMenuAcademic;