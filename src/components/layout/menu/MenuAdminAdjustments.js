import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminAdjustments extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    render() {

        return (
            <ul>
                <h5><strong>Adecuaciones</strong></h5>
                <li>
                    <Link to="/adjustments">+ Ver Adecuaciones</Link>
                </li>
            </ul>
        );
    }
}

export default MenuAdminAdjustments;