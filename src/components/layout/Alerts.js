import React, { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class Alerts extends Component {
    static propTypes = {
        error: PropTypes.object.isRequired,
        message: PropTypes.object.isRequired
    };

    componentDidUpdate(prevProps) {
        const {error, alert, message} = this.props;

        if (error !== prevProps.error){
            if (error.msg) alert.error(error.msg);
            if (error.msg.detail) alert.error(error.msg.detail);
            if (error.msg.password) alert.error(error.msg.password);
            if (error.msg.file) alert.error(error.msg.file);
            if (error.msg.file) alert.error(error.msg.file);
            if (error.msg.observations) alert.error(error.msg.file);
            if (error.msg.percentage) alert.error(error.msg.file);

        }

        if (message !==prevProps.message){
            if (message.msg.errorNumber) alert.error(message.msg.errorNumber);
            if (message.msg.NotFound) alert.error(message.msg.NotFound);
            if (message.msg.resetPassword) alert.info(message.msg.resetPassword);
            if (message.msg.PasswordNotMatch) alert.info(message.msg.PasswordNotMatch);
            if (message.msg.PasswordRestComplete) alert.success(message.msg.PasswordRestComplete);
            if (message.msg.userLoaded) alert.success(message.msg.userLoaded);
            if (message.msg.detail) alert.success(message.msg.detail);
            if (message.msg.detailFile) alert.error(message.msg.detailFile);
            if (message.msg.serverConnection) alert.error(message.msg.serverConnection);
            if (message.msg.registerStatistics) alert.success(message.msg.registerStatistics);
            if (message.msg.registerFile) alert.success(message.msg.registerFile);
        }

    }


    render() {
        return <Fragment />;
    }
}

const mapStateToProps = (state) => ({
    error: state.errors,
    message: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));
