import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { get_users_privileges_view } from '../../../actions/academic/getusers';
import { post_user_privilege } from '../../../actions/academic/createprivilegeuser';
import { delete_user_privilege } from '../../../actions/academic/deleteprivilegeuser';


export class Config extends Component {

    state = {
        number: '',
        privilege: 1,
    }

    static propTypes = {
        users: PropTypes.array,
        msg: PropTypes.string,
        departament: PropTypes.string,
        permissions: PropTypes.array.isRequired,
        get_users_privileges_view: PropTypes.func.isRequired,
        post_user_privilege: PropTypes.func.isRequired,
        delete_user_privilege: PropTypes.func.isRequired
    }

    getUser = () => {
        this.props.get_users_privileges_view();
    }

    componentDidMount() {
        if (
            this.props.permissions.includes('authentication.admin_statistics_academic')
            |
            this.props.permissions.includes('authentication.admin_root')
        ){
            this.getUser();
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.post_user_privilege(this.state.number, this.state.privilege);
        this.getUser();
    }

    onDelete = (pk) => {
        this.props.delete_user_privilege(pk);
        this.getUser();
    }

    render() {

        let list_users = null;
        let form_user = null;

        if (this.props.users.length > 0){
            list_users = (
                <section>
                    {this.props.users.map((user, index) => (
                        <div className="card card-footer alert alert-warning" key={index}>
                            <div className="col-12">
                                <strong>
                                    {user.name} {user.paternal_surname} {user.maternal_surname}
                                </strong>
                                <br></br>
                                <strong>
                                    Puede ver: {
                                        user.departament === 'view_academic_humanities' ?
                                            'Depto. Humanidades'
                                            :
                                            user.departament === 'view_academic_social_sciences' ?
                                            'Depto. Ciencias Sociales'
                                            :
                                            user.departament === 'view_academic_institutional_studies' ?
                                            'Depto. Estudios Institucionales'
                                            :
                                            null
                                }
                                </strong>
                            </div>
                            <div className="col-12 mt-2">
                                <button className="btn btn-warning float-right"
                                        onClick={this.onDelete.bind(this, user.id)}>
                                    Quitar Permiso
                                </button>
                            </div>
                        </div>
                        ))}
                </section>
            );
        }

        if (this.props.users.length >= 0 & this.props.users.length < 3){
            form_user = (
                <form className="card card-body mb-3 alert-primary" onSubmit={this.onSubmit}>
                    <div className="filter-section-dsch-multi-form b-filter p-2">
                        <div className="col-5">
                            <label>Número económico</label>
                            <input type="text" name="number" className="form-control"
                                   value={this.state.number} onChange={this.onChange}
                            />
                        </div>

                        <div className="col-7">
                            <label>Departamento</label>
                            <select className="form-control" name="privilege"
                            value={this.state.privilege} onChange={this.onChange} >
                                <option key={1} value={1}>Humanidades</option>
                                <option key={2} value={2}>Ciencias Sociales</option>
                                <option key={3} value={3}>Estudios insitucionales</option>
                            </select>
                        </div>
                    </div>

                    <div className="col-12 mt-2">
                        <button type="submit" className="btn btn-primary float-right">Guardar</button>
                    </div>
                </form>
            )
        }

        return (
            <div className="modal fade" id="ModalConfig" tabIndex="-1"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                                <strong>Usuarios Con Privilegios para Visualizar</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {form_user}
                            {list_users}
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

const mapStateToProps = (state) => ({
    users: state.academic.users,
    message: state.academic.msg
});

export default connect(
    mapStateToProps, {
        get_users_privileges_view, post_user_privilege, delete_user_privilege })(Config);
