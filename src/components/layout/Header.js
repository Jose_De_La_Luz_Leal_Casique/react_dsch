import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import NavBar from "./NavBar";
import { logout } from '../../actions/accounts/logout';


export class Header extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired,
        logout: PropTypes.func.isRequired,
        permissions: PropTypes.array.isRequired,
        user: PropTypes.object,
    };

    onLogout = (e) => {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        const { isAuthenticated, user } = this.props.auth;

        const authLinks = (
            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                <span className="navbar-text mr-3 text-white">
                    <strong></strong>
                </span>
                <li className="nav-item">

                    <div className="btn-group dropleft">
                        <button type="button" className="nav-link btn btn-info btn-sm text-light dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {user ? `Bienvenid@ ${user.name}` : ''}
                        </button>
                        <div className="dropdown-menu">
                            {
                                this.props.auth.permissions.includes('authentication.admin_exterior')
                                    ?
                                    null
                                    :
                                    <Link to="/profile" className="dropdown-item">
                                        Ver Perfil de Usuario
                                    </Link>
                            }
                            <Link to="/view-history-editorial" className="dropdown-item">
                                Ver Solicitudes a Editorial
                            </Link>
                            <Link to="/reports" className="dropdown-item">
                                Producción Académica
                            </Link>
                            {
                                this.props.permissions.includes(
                                    'authentication.view_academic_general')
                                    ?
                                    <Link to="/permissions" className="dropdown-item">
                                        Configuración de permisos
                                    </Link>
                                    :
                                    null
                            }
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href=""
                               onClick={this.onLogout}
                               style={{"cursor": "pointer"}}>
                                Cerrar Sesión
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        );

        return (
            <nav className="navbar navbar-expand-md navbar-light bg-dsch">
                <div className="collapse navbar-collapse justify-content-start">
                    <Link to="/" className="navbar-brand icon-navbar">
                        <span className="navbar-text text-white">
                            {
                                this.props.user !== null
                                    ?
                                    'UAM - DCSH'
                                    :
                                    'Universidad Autónoma Metropolitana - '
                                    +
                                    'División de Ciencias Sociales Y Humanidades'
                            }
                        </span>
                    </Link>
                </div>
                <button className="navbar-toggler icon-nav-dsch" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    {isAuthenticated ? <NavBar /> : null}
                    {isAuthenticated ? authLinks : null}
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    user: state.auth.user,
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, { logout })(Header);
