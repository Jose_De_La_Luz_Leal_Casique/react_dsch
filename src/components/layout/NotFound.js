import React, { Component } from 'react';



export class NotFound extends Component {

    render() {

        return (
            <section className="mt-5 text-center">
                <h1>Not Found</h1>
            </section>
        );
    }
}

export default NotFound;
