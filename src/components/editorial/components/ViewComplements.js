import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import Plagiarism from "./Plagiarism";
import Dictum from "./Dictum";



export class ViewComplements extends Component {

    static propTypes = {
        edit: PropTypes.object,
        pk: PropTypes.number

    };

    render() {
        if (this.props.edit !== null){
            return (
                <section  className="col-12 text-center card">
                    <div className="filter-section-dsch-multi-form">

                        <section className="col-12 filter-section-dsch-multi-form">
                            <section className="col-6 mt-3 alert alert-warning p-3 mx-1">
                                <h5 className="text-center">
                                    <strong>Software de Antiplagio</strong>
                                </h5>
                                {this.props.edit.plagiarism === null & this.props.edit.finalized === true
                                    ?
                                    <section className="card mt-3 p-3">
                                        <h5>CAMBIOS NO PERMITIDOS</h5>
                                    </section>
                                    :
                                    <Plagiarism
                                        plagiarism={this.props.edit.plagiarism}
                                        title={this.props.edit.title}
                                        pk={this.props.edit.id}
                                    />
                                }
                            </section>

                            <section className="col-6 mt-3 alert alert-warning p-3 mx-1">
                                <h5 className="text-center">
                                    <strong>Dictámenes (Evaluaciones)</strong>
                                </h5>
                                {this.props.edit.dictum === null & this.props.edit.finalized === true
                                    ?
                                    <section className="card mt-3 p-3">
                                        <h5>CAMBIOS NO PERMITIDOS</h5>
                                    </section>
                                    :
                                    <Dictum
                                        dictum={this.props.edit.dictum}
                                        pk={this.props.edit.id}
                                    />
                                }
                            </section>
                        </section>
                    </div>
                </section>
            );
        } else{
            return (
                <section className="text-center mt-5">
                    <h3>Loading...</h3>
                </section>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    edit: state.editorial.edit
});


export default connect(
    mapStateToProps, null)(ViewComplements);