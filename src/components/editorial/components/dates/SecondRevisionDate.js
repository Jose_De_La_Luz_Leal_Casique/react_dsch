import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {FormatDate, ConfigDateSave} from "../../../FormatDate";
import {update_date} from '../../../../actions/editorial/update_date';


export class SecondRevisionDate extends Component {

    state = {
        second_revision_date: new Date()
    }

    static propTypes = {
        second_revision_date: PropTypes.string,
        finalized: PropTypes.bool.isRequired,
        pk: PropTypes.number,
        update_date: PropTypes.func.isRequired,
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        const data = JSON.stringify({
            "second_revision_date": ConfigDateSave(this.state.second_revision_date)})
        this.props.update_date(data, this.props.pk);
    }

    render() {

        if (this.props.second_revision_date == null){
            if (this.props.finalized === true){
                return (
                    <div className="alert alert-info p-2 text-center">
                        <h5 className="pt-1">
                            NO HAY REGISTRO ALMACENADO - FECHA DE REVISIÓN DE SEGUNDAS
                        </h5>
                    </div>
                )
            }else {
                return (
                    <div className="filter-section-dsch-multi-form alert alert-warning p-1">
                        <h5 className="mt-3 col-5">Revisión de Segundas: </h5>
                        <input type="date" className="form-control col-4" name="second_revision_date"
                               onChange={this.onChange} value={this.state.second_revision_date}
                        />
                        <section className="col-1"></section>
                        <button className="btn btn-warning" onClick={this.onSubmit}>Guardar</button>
                    </div>
                );
            }
        } else {
           return (
               <div className="alert alert-success p-2 filter-section-dsch-multi-form">
                    <h5 className="col-5 pt-1">Revisión de Segundas:  </h5>
                    <h5 className="pt-1">
                        <strong>{FormatDate(this.props.second_revision_date)}</strong>
                    </h5>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, {update_date})(SecondRevisionDate)