import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {FormatDate, ConfigDateSave} from "../../../FormatDate";
import {update_date} from '../../../../actions/editorial/update_date';


export class PriceDate extends Component {

    state = {
        price_date: new Date()
    }

    static propTypes = {
        price_date: PropTypes.string,
        pk: PropTypes.number,
        finalized: PropTypes.bool.isRequired,
        update_date: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        const data = JSON.stringify({"price_date": ConfigDateSave(this.state.price_date)})
        this.props.update_date(data, this.props.pk);
    }

    render() {

        if (this.props.price_date == null){
            if (this.props.finalized === true){
                return (
                    <div className="alert alert-info p-2 text-center">
                        <h5 className="pt-1">
                            NO HAY REGISTRO ALMACENADO - FECHA DE SOLICITUD DE PRECIO
                        </h5>
                    </div>
                )
            }else {
                return (
                    <div className="filter-section-dsch-multi-form alert alert-warning p-1">
                        <h5 className="mt-3 col-5">Oficio de Solicitud de Precio: </h5>
                        <input type="date" className="form-control col-4" onChange={this.onChange}
                               name="price_date" value={this.state.price_date}/>
                        <section className="col-1"></section>
                        <button className="btn btn-warning" onClick={this.onSubmit}>Guardar</button>
                    </div>
                );
            }
        } else {
           return (
               <div className="alert alert-success p-2 filter-section-dsch-multi-form">
                    <h5 className="col-5 pt-1">Oficio de Solicitud de Precio:  </h5>
                    <h5 className="pt-1">
                        <strong>{FormatDate(this.props.price_date)}</strong>
                    </h5>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, {update_date})(PriceDate)