import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalViewRequestBook from "../../generals/components/ModalViewRequestBook";
import Year from "../../academic/components/Year";

import {get_list_request_editorial} from '../../../actions/editorial/get_list_request_editorial';
import {get_request_editorial} from '../../../actions/editorial/get_request_editorial';
import {FormatDate} from '../../FormatDate';

export class EditorialEditView extends Component {

    state = {
        temp_request: 0,
        year: new Date().getFullYear(),
        status: 0
    }

    static propTypes = {
        get_list_request_editorial: PropTypes.func.isRequired,
        get_request_editorial: PropTypes.func.isRequired,
        editorial: PropTypes.object.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSendRequest = (pk) => {
       if (pk !== this.state.temp_request){
           this.props.get_request_editorial(pk);
           this.setState({temp_request: pk});
       }
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.get_list_request_editorial(this.state.year, this.state.status);
    }

    render() {
        let section_view = null;

        if (this.props.editorial.load === true){
            if (this.props.editorial.request.length > 0){
                section_view = (
                    <section className="modal-footer">
                        {this.props.editorial.request.map((book) => (
                            <div className="col-4 m-auto p-1" key={book.id}>
                                <section className="card card-body alert-warning">
                                    <div className="col-12">
                                        <h6>
                                            <strong>Título de Obra: </strong>
                                            {
                                                book.title.length > 28 ?
                                                    book.title.substr(0, 28)+' ...'
                                                    :
                                                    book.title
                                            }
                                        </h6>
                                        <h6>
                                            <strong>Autor/Colaborador: </strong>
                                            {book.name}
                                        </h6>
                                        <h6>
                                            <strong>Fecha de Recepción: </strong>
                                            {FormatDate(book.date_created)}
                                        </h6>
                                        <h6>
                                            <strong>Última Actualización: </strong>
                                            {FormatDate(book.date_updated)}
                                        </h6>
                                        <div className="col-12">
                                            <button className="btn-dsch float-right"
                                                    data-toggle="modal"
                                                    data-target="#ModalRequestBook"
                                                    onClick={this.onSendRequest.bind(this, book.id)}>
                                                {
                                                    book.finalized === true ?
                                                        'FINALIZADO'
                                                        :
                                                        'EN PROCESO'
                                                } - VER DETALLE
                                            </button>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        ))}
                        <ModalViewRequestBook

                        />
                    </section>
                )

            } else {
                section_view = (
                    <div className="text-center mt-5 section-dsch-form pb-5">
                        <h4 className="pt-5 pb-5">
                            <strong>
                                NO HAY SOLICITUDES REALIZADAS
                            </strong>
                        </h4>
                    </div>
                );
            }

        } else {
            section_view =  (
                <div className="text-center mt-5">
                    <h4 className="pt-5 pb-5">Loading...</h4>
                </div>
            );
        }

        return (
            <div>
                <section className="alert alert-danger mt-2 text-center">
                    <h4>PARA PODER VER SOLICITUDES SELECIONAR AÑO</h4>
                    <form className="col-4 mx-auto filter-section-dsch-multi-form"
                          onSubmit={this.onSubmit}>
                        <Year
                            select_year={this.state.year}
                            onChange={this.onChange}
                        />
                        <div className="col-5">
                            <select className="form-control" value={this.state.status} name="status"
                                    onChange={this.onChange}>
                                <option key={0} value={0}>Pendientes</option>
                                <option key={1} value={1}>Finalizados</option>
                            </select>
                        </div>

                        <button className="btn btn-danger ml-3">Visualizar</button>
                    </form>
                </section>
                {section_view}
            </div>
        )

    }
}

const mapStateToProps = (state) => ({
    editorial: state.editorial,
});


export default connect(
    mapStateToProps, {
        get_list_request_editorial, get_request_editorial})(EditorialEditView);