import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import Meeting from "./Meeting";
import Letter from "./Letter";

import {FormatDate} from "../../FormatDate";



export class ViewAdvice extends Component {

    state = {
        observations: ''
    }

    static propTypes = {
        edit: PropTypes.object,
        pk: PropTypes.number,

    };


    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    render() {
        if (this.props.edit !== null){
            return (
                <section  className="col-12 text-center card ">
                    <div className="filter-section-dsch-multi-form">
                            <section className="col-6 mt-3 alert alert-warning p-3 mx-1">
                                <h5 className="text-center">
                                    <strong>Reunión de Consejo Ed.</strong>
                                </h5>
                                {this.props.edit.meeting === null & this.props.edit.finalized === true
                                    ?
                                    <section className="card mt-3 p-3">
                                        <h5>CAMBIOS NO PERMITIDOS</h5>
                                    </section>
                                    :
                                    <Meeting
                                        meeting={this.props.edit.meeting}
                                        pk={this.props.edit.id}
                                        is_active={this.props.edit.finalized}
                                    />
                                }
                            </section>

                            <section className="col-6 mt-3 alert alert-warning p-3 mx-1">
                                <h5 className="text-center">
                                    <strong>Resultados de Consejo Ed.</strong>
                                </h5>
                                {this.props.edit.letter === null & this.props.edit.finalized === true
                                    ?
                                    <section className="card mt-3 p-3">
                                        <h5>CAMBIOS NO PERMITIDOS</h5>
                                    </section>
                                    :
                                    <Letter
                                        letter={this.props.edit.letter}
                                        pk={this.props.edit.id}

                                    />
                                }
                            </section>
                    </div>
                </section>
            );
        } else{
            return (
                <section className="text-center mt-5">
                    <h3>Loading...</h3>
                </section>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    edit: state.editorial.edit
});


export default connect(
    mapStateToProps, null)(ViewAdvice);