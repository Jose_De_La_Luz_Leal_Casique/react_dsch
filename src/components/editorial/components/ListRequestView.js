import React, { Component } from 'react';
import { connect } from "react-redux";
import PropType from "prop-types";

import {FormatDate} from '../../FormatDate';
import {Link} from "react-router-dom";


export class ListRequestView extends Component {

    static propTypes = {
        editorial: PropType.object.isRequired
    }

    render() {

        if (this.props.editorial.load === true){
            return (
                <table className="table table-striped mt-3">
                    <thead className="text-center">
                        <tr>
                            <th width={200}>Obra</th>
                            <th width={150}>Autor/Colaborador</th>
                            <th width={300}>Email de Contacto</th>
                            <th width={190}>Fecha de Recepción</th>
                            <th width={190}>Última Actualización</th>
                            <th width={200}>Observaciones</th>
                            <th>Detalle</th>
                        </tr>
                    </thead>
                    <tbody className="text-center table-scroll-a">
                    {this.props.editorial.request.map((obj) => (
                        <tr key={obj.id}>
                            <td width={200}>{obj.title}</td>
                            <td width={150}>{obj.author}</td>
                            <td width={300}>{obj.email}</td>
                            <td width={190}>
                                {FormatDate(obj.date_created)}
                            </td>
                            <td width={190}>
                                {obj.date_updated === null ?
                                'NO HAY ACTUALIZACIÓN': FormatDate(obj.date_updated )}
                            </td>
                            <td width={200}>
                                {obj.observations === '' ? 'NO HAY OBSERVACIONES': obj.observations }
                            </td>
                            <td>
                                <Link to={{
                                        pathname: '/editorial-edit',
                                        state: {id: obj.id}
                                    }} >VER
                                </Link>
                            </td>
                        </tr>
                        )
                    )}
                    </tbody>
                </table>
            );
        } else{
            return (
                <section className="text-center mt-5">
                    <h3>NO HAY COINCIDENCIAS</h3>
                </section>
            )
        }

    }
}

const  mapStateToProps = (state) => ({
    editorial: state.editorial
});


export default connect(
    mapStateToProps, null)(ListRequestView);