import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import ModalDates from "./components/ModalDates";
import ViewAdvice from "./components/ViewAdvice";
import ViewComplements from "./components/ViewComplements";

import {get_request_editorial} from "../../actions/editorial/get_request_editorial";
import {finalized_request_book} from '../../actions/editorial/finalized_request_book';
import {update_request_book} from '../../actions/editorial/update_request_book';
import {FormatDate} from "../FormatDate";



export class EditorialEdit extends Component {

    state = {
        pk: this.props.location.state.id,
        observations: '',
        view_advice: false,
        view_complements: true,
    }

    static propTypes = {
        get_request_editorial: PropTypes.func.isRequired,
        finalized_request_book: PropTypes.func.isRequired,
        update_request_book: PropTypes.func.isRequired,
        edit: PropTypes.object

    };

    componentDidMount() {
        this.props.get_request_editorial(this.state.pk);
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onFinalizedRequestBook = (e) => {
        if (window.confirm('Desea Terminar Este Proceso')) {
            this.props.finalized_request_book(this.props.edit.id);
        }
    }

    onUpdateBook = (e) => {
        this.props.update_request_book(this.state.observations, this.state.pk);
        this.setState({observations: ''});
    }

    ViewComplements = (e) => {
        this.setState({view_complements: true, view_advice: false});
    }

    ViewAdvice = (e) => {
        this.setState({view_complements: false, view_advice: true});
    }

    render() {
        if (this.props.edit !== null){
            return (
                <section className="col-12 text-center card mt-2 pb-3">
                    <h4 className="text-left mt-3">
                        <strong>SOLICITUD: </strong> {this.props.edit.title}
                        {this.props.edit.finalized === true ? null
                            :
                            <button className="btn btn-danger float-right"
                                    onClick={this.onFinalizedRequestBook}>
                                Finalizar Proceso
                            </button>
                        }
                        <button className="btn btn-primary float-right mr-2" data-toggle="modal"
                                data-target="#modalDates">
                            Registro de Fechas
                        </button>
                    </h4>

                    <h6 className="text-left pl-5 margin-subtitle">
                        Última Actualización {FormatDate(this.props.edit.date_updated)}
                    </h6>

                    <div className="filter-section-dsch-multi-form">
                        <section className="col-3">
                            <div className="dsch-det-card alert-dark text-left">
                                <label>
                                    <strong>Autor: </strong>{this.props.edit.name}
                                </label>
                            </div>

                            <div className="dsch-det-card alert-dark text-left">
                                <label>
                                    <strong>Fecha de Solicitud: </strong><br></br>
                                    {FormatDate(this.props.edit.date_created)}
                                </label>
                            </div>

                            <div className="dsch-det-card alert-dark text-left">
                                <label>
                                    <strong>Email de Contacto: </strong>{this.props.edit.email}
                                </label>
                            </div>

                            <div className="dsch-det-card alert-dark text-left">
                                <label>
                                    <strong>Estado General(Producción): </strong>
                                    {this.props.edit.observations}
                                </label>
                            </div>

                            <div className="form-group col-12 m-auto pt-1">
                                <h5>Observaciones</h5>
                                <textarea name="observations" className="form-control" required rows="3"
                                          value={this.state.observations} onChange={this.onChange}>
                                </textarea>
                            </div>

                            <div className="dsch-det-card text-center pt-3 pr-3">
                                {this.props.edit.finalized === true ? null
                                    :
                                    <button className="btn btn-dsch" onClick={this.onUpdateBook}>
                                            Actualizar Proceso
                                        </button>
                                }

                            </div>
                        </section>

                        <section className="col-9 filter-section-dsch-multi-form">
                            <section className="col-2">
                                {
                                    this.state.view_advice === true
                                        ?
                                        <button className="btn btn-primary btn-block"
                                                onClick={this.ViewComplements}>
                                            Generales
                                        </button>
                                        :
                                        <button className="btn btn-primary btn-block"
                                                onClick={this.ViewAdvice}>
                                            Consejo
                                        </button>
                                }
                            </section>
                            <section className="col-10">
                                {
                                    this.state.view_advice === true
                                        ?
                                        <ViewAdvice
                                            pk={this.state.pk}
                                        />
                                        :
                                        <ViewComplements />
                                }
                            </section>
                        </section>

                    </div>
                    <ModalDates
                        edit={this.props.edit}
                    />
                </section>
            );
        } else{
            return (
                <section className="text-center mt-5">
                    <h3>Loading...</h3>
                </section>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    edit: state.editorial.edit
});


export default connect(
    mapStateToProps, {
        get_request_editorial, finalized_request_book, update_request_book})(EditorialEdit);