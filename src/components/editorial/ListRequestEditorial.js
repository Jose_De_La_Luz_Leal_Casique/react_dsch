import React, { Component } from 'react';
import Year from "../academic/components/Year";
import { connect } from "react-redux";
import PropType from "prop-types";

import ListRequestView from './components/ListRequestView';
import {get_list_request_editorial} from '../../actions/editorial/get_list_request_editorial';


const initial_date = new Date().getFullYear();


export class ListRequestEditorial extends Component {

    state = {
        year: initial_date.toString(),
        status: 0
    }

    static propTypes = {
        get_list_request_editorial: PropType.func.isRequired
    }


    componentDidMount() {
        this.get_list();
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit = (e) => {
        this.get_list();
    }

    get_list (){
        this.props.get_list_request_editorial(this.state.year, this.state.status);
    }

    render() {
        return (
            <section>
                <div className="col-12 text-center">
                    <section className="text-center mt-3">
                        <h3>Selecionar Año Y Estado de Solicitud</h3>
                    </section>
                    <section className="py-2 m-auto filter-section-dsch-multi-form col-10">
                        <div className="col-5">
                            <Year
                                select_year={this.state.year.toString()}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="col-5">
                            <select className="form-control" value={this.state.status} name="status"
                                    onChange={this.onChange}>
                                <option key={0} value={0}>Pendientes</option>
                                <option key={1} value={1}>Finalizados</option>
                            </select>
                        </div>
                        <div className="col-2">
                            <button className="btn btn-dsch" onClick={this.onSubmit}>
                                Buscar
                            </button>
                        </div>
                    </section>
                </div>
                <ListRequestView />
            </section>
        );
    }
}

const  mapStateToProps = (state) => ({});


export default connect(
    mapStateToProps, {get_list_request_editorial})(ListRequestEditorial);