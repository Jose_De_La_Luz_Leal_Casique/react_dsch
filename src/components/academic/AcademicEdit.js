import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import PropTypes from 'prop-types';

import { get_academic_register } from '../../actions/academic/get_academic_register';
import { update_academic_register } from '../../actions/academic/update_academic_register';


export class AcademicEdit extends Component {

    state = {
        number_hour: 0,
        number_course: 0,
        number_student: 0,
    }

    static propTypes = {
        get_academic_register: PropTypes.func.isRequired,
        update_academic_register: PropTypes.func.isRequired,
        edit: PropTypes.object
    };

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
        this.props.edit.register[e.target.name] = e.target.value;
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.props.edit.register.number_course < 0) {
            alert('No se Permiten Valores Negativos');
            return;
        }
        if (this.props.edit.register.number_hour < 0) {
            alert('No se Permiten Valores Negativos');
            return;
        }
        if (this.props.edit.register.number_student < 0) {
            alert('No se Permiten Valores Negativos');
            return;
        }

        this.props.update_academic_register(
            this.props.location.state.id,
            parseFloat(this.props.edit.register.number_course),
            parseFloat(this.props.edit.register.number_hour),
            parseFloat(this.props.edit.register.number_student),
        )
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.props.get_academic_register(params.id);
    }

    render() {

        if (this.props.edit.inProcess === true){

            return (
                <Fragment>
                    <section className="filter-section-dsch-multi-form mt-5">
                        <div className="card card-body mt-5 col-9 m-auto" >
                            <table className="table table-striped mt-2">
                                <thead className="text-center">
                                    <tr>
                                        <th>No. Económico</th>
                                        <th>Plan de estudio</th>
                                        <th>Departamento</th>
                                        <th>Año</th>
                                        <th>Trimestre</th>
                                        <th width="220">Nombre de Curso</th>
                                        <th>Detalle</th>
                                    </tr>
                                </thead>
                                <tbody className="text-center">
                                    <tr key={this.props.edit.id}>
                                        <td>{this.props.edit.register.user}</td>
                                        <td>{this.props.edit.register.study_plan}</td>
                                        <td>{this.props.edit.register.departament}</td>
                                        <td>{this.props.edit.register.year}</td>
                                        <td>{this.props.edit.register.trimester}</td>
                                        <td>{this.props.edit.register.course}</td>
                                        <td>{this.props.edit.register.classroom === true ?
                                            'Dentro de Aula': 'Fuera de Aula'}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <section className="m-auto ">
                                <h4 className="mt-4">Estadísticas a Modificar</h4>
                            </section>

                            <form onSubmit={this.onSubmit}>
                                <section className="filter-section-dsch-multi-form mt-3">
                                    <div className="form-group col-4">
                                        <label>Número de Cursos</label>
                                        <input type="number" className="form-control" required
                                               onChange={this.onChange} name="number_course"
                                               value={this.props.edit.register.number_course}/>
                                    </div>

                                    <div className="form-group col-4">
                                        <label>Número de Horas</label>
                                        <input type="number" className="form-control" required
                                               onChange={this.onChange} name="number_hour"
                                               value={this.props.edit.register.number_hour}/>
                                    </div>

                                    <div className="form-group col-4">
                                        <label>Número de Alumnos</label>
                                        <input type="number" className="form-control" required
                                               onChange={this.onChange} name="number_student"
                                               value={this.props.edit.register.number_student}/>
                                    </div>
                                </section>

                                <section className="filter-section-dsch-multi-form mt-3">
                                    <div className="form-group col-12">
                                        <button className="btn-dsch float-right">
                                            Guardar
                                        </button>
                                    </div>

                                </section>
                            </form>
                        </div>

                    </section>
                </Fragment>
            )
        } else if (this.props.edit.finalized === true && this.props.edit.inProcess === true) {
            return <Redirect to="/" />;
        }   else {
            return <h3>Loading...</h3>

        }
    }
}

const mapStateToProps = (state) => ({
    edit: state.edit
});

export default connect(
    mapStateToProps, { get_academic_register, update_academic_register })(AcademicEdit);

