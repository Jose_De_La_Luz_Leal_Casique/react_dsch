import React, { Component } from 'react';

import Form from "./components/Form";
import FormFile from "./components/FormFile";


export class Academic extends Component{

    render() {
        return (
            <div className="filter-section-dsch-multi-form" >
                <Form />
                <FormFile />
            </div>
        )
    }
}

export default Academic;
