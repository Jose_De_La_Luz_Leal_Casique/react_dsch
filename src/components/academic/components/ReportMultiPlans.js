import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import Departaments from "./Departaments";
import { get_academic_multi_statistics } from '../../../actions/academic/academicmultiplan';


let initial_year = new Date().getFullYear();


export class ReportMultiPlans extends Component {

    state = {
        departament: 'Humanidades',
        depart: false,
        doctorate: false,
        master: false,
        initial: initial_year,
        final: initial_year

    }

    static propTypes = {
        user: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
        get_academic_multi_statistics: PropTypes.func.isRequired,
        years: PropTypes.object.isRequired
    }

    componentDidMount() {
        if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            this.setState({ departament: 'Ciencias Sociales' });

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            this.setState({ departament: 'Estudios Institucionales' });

        } else if (this.props.permissions.includes('authentication.view_academic_humanities')){
            this.setState({ departament: 'Humanidades' });
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onChecked = (e) => {
        this.setState({ [e.target.name]: e.target.checked });
    }

    onSubmit = (e) => {
        e.preventDefault();
        let plans = '';

        if (this.state.doctorate === true){
            plans += 'Doctorado-';
        }

        if (this.state.depart === true){
            plans += 'Departamentos-';
        }

        if (this.state.master === true){
            plans += 'Maestría-';
        }

        this.props.get_academic_multi_statistics(
            plans, this.state.departament, this.state.initial, this.state.final)
    };

    render() {

        let inputDepartament = null;

        if (this.props.permissions.includes('authentication.view_academic_humanities')){
            inputDepartament = <input type="text" value="Humanidades" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            inputDepartament = <input type="text"  value="Ciencias Sociales" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            inputDepartament = <input type="text" value="Estudios Institucionales" className="form-control" disabled/>
        }

        return (
            <div className="col-12 mx-auto text-center alert alert-primary">
                <h5><strong>Reportes de carga académica anual por plan de estudio</strong></h5>
                <hr></hr>
                <form className="mt-3" onSubmit={this.onSubmit}>

                    <div className="mt-3 b-filter">
                        <strong>Planes de Estudio</strong>
                        <table className="col-10 m-auto">
                            <tbody>
                                <tr>
                                    <td className="text-left">
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" name="depart"
                                                   value={this.state.depart} onChange={this.onChecked}/>
                                            <label className="form-check-label" htmlFor="depart">
                                                Departamentos
                                            </label>
                                        </div>
                                    </td>
                                    <td className="text-left">
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" name="doctorate"
                                                   value={this.state.doctorate} onChange={this.onChecked}/>
                                            <label className="form-check-label" htmlFor="doctorate">
                                                Doctorado
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="text-left">
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" name="master"
                                                   value={this.state.master} onChange={this.onChecked}/>
                                            <label className="form-check-label" htmlFor="master">
                                                Maestría
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                    <div className="mt-3">
                        <strong>Departamento</strong>
                        {
                            this.props.permissions.includes('authentication.admin_academic') |
                            this.props.permissions.includes('authentication.admin_root') |
                            this.props.permissions.includes('authentication.view_academic_general') ?
                                <Departaments
                                    select_departament={this.state.departament}
                                    onChange={this.onChange}
                                />
                                : inputDepartament
                        }
                    </div>

                    <div className="mt-3 b-filter filter-section-dsch-multi-form">
                        <div className="col-6 p-2">
                            <strong>Año inicial</strong>
                            <select className="form-control" name="initial"
                                    value={this.state.initial} onChange={this.onChange}>
                                {this.props.years.years.map((year) => (
                                    <option key={year.id}>{year.year}</option>
                                    ))};
                            </select>
                        </div>

                        <div className="col-6 p-2">
                            <strong>Año final</strong>
                            <select className="form-control" name="final"
                                    value={this.state.final} onChange={this.onChange}>
                                {this.props.years.years.map((year) => (
                                    <option key={year.id}>{year.year}</option>
                                    ))};
                            </select>
                        </div>
                    </div>


                    <div className="form-group mt-4 ml-3">
                        <button className="btn btn-primary" data-toggle="modal" data-target="#ModalReportTotals">
                            Ver reporte
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    years: state.years,
    permissions: state.auth.permissions,
});

export default connect(
    mapStateToProps, { get_academic_multi_statistics})(ReportMultiPlans);
