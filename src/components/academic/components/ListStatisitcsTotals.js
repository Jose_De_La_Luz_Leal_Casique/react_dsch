import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { get_academic_statistics_totals } from '../../../actions/academic/academicstatisticstotals';

export class ListStatistics extends Component {

    static propTypes = {
        academic: PropTypes.array,
        load: PropTypes.bool,
        get_academic_statistics_totals: PropTypes.func.isRequired,
    };

    componentDidMount() {
        let year = new Date().getFullYear();
        this.props.get_academic_statistics_totals('Departamentos', 'Humanidades', year);
    }

    render() {
        if (this.props.load === true && this.props.academic) {
            return (
                <Fragment>
                    <table className="table table-striped mt-5">
                        <thead className="text-center">
                            <tr>
                                <th>Número Económico</th>
                                <th width={180}>Profesor(a)</th>
                                <th>cursos en aula</th>
                                <th>horas en aula</th>
                                <th>alumnos en aula</th>
                                <th>cursos fuera de aula</th>
                                <th>horas fuera de aula</th>
                                <th>alumnos fuera de aula</th>
                            </tr>
                        </thead>
                        <tbody className="text-center">
                            {this.props.academic.map((lead) => (
                            <tr key={lead.id}>
                                <td>{lead.user}</td>
                                <td>{lead.name}</td>
                                <td>{lead.course_in_classroom.toFixed(2)}</td>
                                <td>{lead.hour_in_classroom.toFixed(2)}</td>
                                <td>{lead.student_in_classroom.toFixed(2)}</td>
                                <td>{lead.course_out_classroom.toFixed(2)}</td>
                                <td>{lead.hour_out_classroom.toFixed(2)}</td>
                                <td>{lead.student_out_classroom.toFixed(2)}</td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </Fragment>
            );
        } else{

            if (this.props.load === true){
                return <h3 className="mt-5 text-center">NO HAY COINICIDENCIAS</h3>
            } else{
                return <h3 className="mt-5 text-center">Loading ...</h3>
            }

        }
    }
}

const mapStateToProps = (state) => ({
  academic: state.academic.totals,
  load: state.academic.load
});

export default connect(
    mapStateToProps, { get_academic_statistics_totals })(ListStatistics);