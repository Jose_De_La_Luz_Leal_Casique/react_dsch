import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { get_academic_statistics_totals } from '../../../actions/academic/academicstatisticstotals';

import Departaments from "./Departaments";
import Plans from './Plans';
import ListStatisitcsTotals from "./ListStatisitcsTotals";
import Year from './Year';


let initial_year = new Date().getFullYear();


export class SectionFilterTotals extends Component {

    state = {
        departament: 'Humanidades',
        study_plan: 'Departamentos',
        year: initial_year.toString(),
    }

    static propTypes = {
        academic: PropTypes.array,
        get_academic_statistics_totals: PropTypes.func.isRequired,
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_academic_statistics_totals(
            this.state.study_plan, this.state.departament, this.state.year);
    };


    render() {

        const { departament, study_plan, year } = this.state;

        return (
            <Fragment>
                <form className="filter-section-dsch" onSubmit={this.onSubmit}>

                    <Plans
                        select_plan={study_plan}
                        onChange={this.onChange}
                    />

                    <Departaments
                        select_departament={departament}
                        onChange={this.onChange}
                    />

                    <Year
                        select_year={year}
                        onChange={this.onChange}
                    />

                    <button className="btn-dsch mt-2">Mostar</button>
                </form>

                <ListStatisitcsTotals
                    academic={this.props.academic}
                />
            </Fragment>
            );
    }
}

const mapStateToProps = (state) => ({
    study: state.study,
    academic: state.academic.statistics,
});

export default connect(
    mapStateToProps, { get_academic_statistics_totals })(SectionFilterTotals);
