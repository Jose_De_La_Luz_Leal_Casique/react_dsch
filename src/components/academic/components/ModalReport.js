import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";

const date = new Date();
const date_now = date.getDate().toString()+'-'+(date.getMonth()+1).toString()+'-'+date.getFullYear();

export class ModalReport extends Component {

    static propTypes = {
        academic: PropTypes.array.isRequired,
        load: PropTypes.bool,
    };

    generatorPDF = () => {
        let totalPagesExp = '{total_pages_count_string}'
        let pdf = new jsPDF({
             orientation: 'l',
             unit: 'mm',
             format: 'letter',
        });
        pdf.autoTable({
            html: '#reportTotals',
            styles: { fontSize: 12, haling: 'center' },
            didDrawPage: function (data) {
                // titulo
                pdf.setFontSize(18)
                pdf.setTextColor(40)
                pdf.text('Universidad Autónoma Metropolitana Unidad Cuajimalpa - Reporte de Carga Académica',
                    data.settings.margin.left, 10)

                // pie de página
                let str = 'Página ' + pdf.internal.getNumberOfPages()
                if (typeof pdf.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }
                pdf.setFontSize(10)
                let pageSize = pdf.internal.pageSize
                let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                pdf.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 20 },
        })
        if (typeof pdf.putTotalPages === 'function') {
            pdf.putTotalPages(totalPagesExp)
        }
        pdf.save('Reporte-Carga-Académica@'+date_now);
    }


    render() {
        return (
            <div className="modal fade" id="ModalReportTotals" tabIndex="-1"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl">
                    <div className="modal-content">
                        <div className="modal-body" >
                            <div className="filter-section-dsch-multi-form">
                                <div className="col-8">
                                    <h3>Reporte de carga académica anual</h3>
                                </div>
                                <div className="col-4">
                                    {this.props.academic.length > 0 ?
                                        <button className="btn-dsch float-right"
                                                onClick={this.generatorPDF}> Guardar PDF </button>
                                        :
                                        null
                                    }
                                </div>
                            </div>

                            <div id="info">
                                {this.props.load === true ?
                                    this.props.academic.length > 0 ?
                                        <table className="table table-striped mt-5" id="reportTotals">
                                            <thead className="text-center">
                                                <tr>
                                                    <th>Número Económico</th>
                                                    <th width={180}>Profesor(a)</th>
                                                    <th>año</th>
                                                    <th>cursos en aula</th>
                                                    <th>horas en aula</th>
                                                    <th>alumnos en aula</th>
                                                    <th>cursos fuera de aula</th>
                                                    <th>horas fuera de aula</th>
                                                    <th>alumnos fuera de aula</th>
                                                </tr>
                                            </thead>
                                            <tbody className="text-center">
                                                {this.props.academic.map((lead, index) => (
                                                    <tr key={index}>
                                                        <td>{lead.user}</td>
                                                        <td>{lead.name}</td>
                                                        <td>{lead.year}</td>
                                                        <td>{lead.course_in_classroom.toFixed(2)}</td>
                                                        <td>{lead.hour_in_classroom.toFixed(2)}</td>
                                                        <td>{lead.student_in_classroom.toFixed(2)}</td>
                                                        <td>{lead.course_out_classroom.toFixed(2)}</td>
                                                        <td>{lead.hour_out_classroom.toFixed(2)}</td>
                                                        <td>{lead.student_out_classroom.toFixed(2)}</td>
                                                    </tr>
                                                ))}
                                                <tr key={this.props.academic.length + 1}>
                                                    <td><strong>Totales</strong></td>
                                                    <td>{}</td>
                                                    <td>{}</td>
                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.course_in_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>
                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.hour_in_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>

                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.student_in_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>

                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.course_out_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>

                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.hour_out_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>

                                                    <td>
                                                        {this.props.academic.reduce(
                                                            (sum, value) => (
                                                                sum + value.student_out_classroom),
                                                            0).toFixed(2)
                                                        }
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        :
                                        <h3 className="mt-5 text-center pb-5">No Hay Coincidencias</h3>
                                    :
                                    <h3 className="mt-5 text-center">Loading ...</h3>
                                }
                            </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
  academic: state.academic.totals,
  load: state.academic.load
});

export default connect(mapStateToProps, null)(ModalReport);