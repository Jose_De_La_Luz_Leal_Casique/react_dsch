import React, { Component } from 'react';
import PropTypes from 'prop-types';


export class Trimester extends Component {

    static propTypes = {
        select_trimester: PropTypes.string
    }

    render() {

        return (
            <select className="form-control" name="trimester"
                    value={this.props.select_trimester} onChange={this.props.onChange}>
                <option>Invierno</option>
                <option>Primavera</option>
                <option>Otoño</option>
            </select>
        );
    }
}

export default Trimester;
