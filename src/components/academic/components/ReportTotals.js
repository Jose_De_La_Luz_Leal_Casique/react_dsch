import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import Plans from "./Plans";
import Departaments from "./Departaments";
import Year from "./Year";

import { get_academic_statistics_totals } from '../../../actions/academic/academicstatisticstotals';


let initial_year = new Date().getFullYear();


export class Report extends Component {

    state = {
        departament: 'Humanidades',
        study_plan: 'Departamentos',
        year: initial_year.toString(),
    }

    static propTypes = {
        get_academic_statistics_totals: PropTypes.func.isRequired,
        permissions: PropTypes.array.isRequired,
    }

    componentDidMount() {
        if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            this.setState({ departament: 'Ciencias Sociales' });

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            this.setState({ departament: 'Estudios Institucionales' });

        } else if (this.props.permissions.includes('authentication.view_academic_humanities')){
            this.setState({ departament: 'Humanidades' });
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_academic_statistics_totals(
            this.state.study_plan, this.state.departament, this.state.year);
    };


    render() {

        let inputDepartament = null;

        if (this.props.permissions.includes('authentication.view_academic_humanities')){
            inputDepartament = <input type="text" value="Humanidades" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            inputDepartament = <input type="text"  value="Ciencias Sociales" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            inputDepartament = <input type="text" value="Estudios Institucionales" className="form-control" disabled/>
        }

        return (
            <div className="col-12 mx-auto text-center alert alert-primary">
                <h5><strong>Reportes de carga académica anual por departamento</strong></h5>
                <hr></hr>

                <form className="mt-3" onSubmit={this.onSubmit}>
                    <div className="mt-3">
                        <strong>Plan de Estudio</strong>
                        <Plans
                            select_plan={this.state.study_plan}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="mt-3">
                        <strong>Departamento</strong>
                        {
                            this.props.permissions.includes('authentication.admin_academic')
                            |
                            this.props.permissions.includes('authentication.admin_roots')
                            |
                            this.props.permissions.includes('authentication.view_academic_general')
                                ?
                                <Departaments
                                    select_departament={this.state.departament}
                                    onChange={this.onChange}
                                />
                                : inputDepartament
                        }
                    </div>

                    <div className="filter-section-dsch-multi-form mt-3">
                        <div className="col-6">
                            <strong>Año</strong>
                            <Year
                                select_year={this.state.year}
                                onChange={this.onChange}
                            />
                        </div>

                        <div className="form-group mt-4 ml-3 col-6">
                            <button className="btn btn-primary" data-toggle="modal" data-target="#ModalReportTotals">
                                Ver reporte
                            </button>
                        </div>
                        </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions,
});

export default connect(mapStateToProps, {get_academic_statistics_totals})(Report);
