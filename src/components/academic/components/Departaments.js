import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { loadDepartament } from '../../../actions/academic/departaments';


export class Departaments extends Component {

    static propTypes = {
        departament: PropTypes.object.isRequired,
        loadDepartament: PropTypes.func.isRequired,
        select_departament: PropTypes.string
    }

    componentDidMount() {
        if (this.props.departament.departaments === null){
            this.props.loadDepartament();
        }
    }

    render() {

        if (this.props.departament.load === true){

            return (
                <select className="form-control" name="departament"
                        value={this.props.select_departament} onChange={this.props.onChange}>

                    {this.props.departament.departaments.map((departament) => (
                        <option key={departament.id} >{departament.name}</option>
                    ))};

                </select>
            );
        } else{
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    departament: state.departament,
});

export default connect(
    mapStateToProps, { loadDepartament })(Departaments);
