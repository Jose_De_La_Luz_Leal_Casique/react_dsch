import React, { Component } from 'react';
import {connect} from "react-redux";
import PropType from "prop-types";

import ComplementConcourse from "./components/ComplementConcourse";

import {FormatDate} from "../FormatDate";
import {SERVER} from "../../actions/server";
import { get_retrieve_concourse } from '../../actions/concourse/retrieveconcourse';
import { set_finalized_concourse } from '../../actions/concourse/finalizedconcourse';


export class ConcourseEdit extends  Component {

    state = {
        id: 0,
        edit: false,
        file: null,
        type_concourse: 0,
    }

    static propTypes = {
        concourse: PropType.object,
        get_retrieve_concourse: PropType.func.isRequired,
        set_finalized_concourse: PropType.func.isRequired
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.props.get_retrieve_concourse(params.id);
        this.setState({ id: params.id, type_concourse: params.type_concourse });
    }

    onFinalizedConcourse = (e) => {
        this.props.set_finalized_concourse(this.state.id);
    }

    render() {

        if (this.props.concourse !== null) {

            return (
                <section className="filter-section-dsch-multi-form pt-3">
                    <section className="col-4">
                        <div className="card p-4">
                            <h4 className="text-center"><strong>
                                Detalle: {
                                this.state.type_concourse === 1
                                    ? 'Educación Curricular'
                                    :
                                this.state.type_concourse === 2
                                    ? 'Cátedra'
                                    :
                                this.state.type_concourse === 3
                                    ? 'Oposición'
                                    :
                                this.state.type_concourse === 4
                                    ? 'Profesor Visitante'
                                    :
                                    null
                            }
                            </strong></h4>
                            <h5 className="py-2 text-center mt-1">
                                <strong>Convocatoria: </strong>{this.props.concourse.code}
                            </h5>
                            <div className="card card-body">
                                <h6>
                                    <strong>Descripción: </strong>{this.props.concourse.description}
                                </h6>

                                {
                                    this.state.type_concourse === 4
                                        ?
                                        null
                                        :
                                        <h6>
                                            <strong>Fecha de Solicitud: </strong>
                                            {FormatDate(this.props.concourse.date_created)}
                                        </h6>
                                }

                                {
                                    this.state.type_concourse === 4
                                        ?
                                        null
                                        :
                                        <h6>
                                            <strong>Envío de Convocatoria: </strong>
                                            {FormatDate(this.props.concourse.date_send)}
                                        </h6>
                                }
                                <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                                   href={`${SERVER}${this.props.concourse.file}`}>
                                    <strong>Ver Convocatoria: </strong>
                                </a>
                            </div>
                            <div className="card card-body mt-3">
                                <div className="text-center">
                                    {
                                        this.props.concourse.is_active === true ?
                                            <button className="btn btn-danger"
                                                    onClick={this.onFinalizedConcourse}>
                                                Finalizar Convocatoria
                                            </button>
                                            :
                                            <button className="btn btn-primary"
                                                    onClick={this.onFinalizedConcourse}>
                                                Editar Convocatoria
                                            </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </section>

                    <section className="col-8">
                        <ComplementConcourse
                            concourse={this.props.concourse} type_concourse={this.state.type_concourse}
                        />
                    </section>
                </section>
            )
        } else {
            return (
                <section className="mt-5">
                    <div className="text-center pt-5">
                        <h1 className="m-auto"><strong>Loading...</strong></h1>
                    </div>
                </section>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    concourse: state.concourse.edit
});

export default connect(
    mapStateToProps,
    {get_retrieve_concourse, set_finalized_concourse})(ConcourseEdit);