import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { create_concourse } from '../../actions/concourse/createconcourse';
import { ConfigDateSave } from '../FormatDate';


export class Concourse extends  Component {

    state = {
        code: '',
        description: '',
        announcement: null,
        date_created: '',
        date_send: ''
    }

    static propTypes = {
        create_concourse: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onFile = (e) => {
        this.setState({announcement: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('code', this.state.code);
        data.append('description', this.state.description);
        data.append('announcement', this.state.announcement, this.state.announcement.name);
        data.append('date_created', ConfigDateSave(this.state.date_created));
        data.append('date_send', ConfigDateSave(this.state.date_send));
        this.props.create_concourse(data);
        this.setState({ code: '', description: '', date_created: '', date_send: ''});
    }

    render() {
        return (
            <div className="card card-body col-9 mx-auto mt-5">
                <form onSubmit={this.onSubmit}>

                    <section className="filter-section-dsch-multi-form">
                        <div className="col-4 p-1">
                            <strong className="ml-2">Nueva Convocatoria:</strong>
                            <input type="text" minLength={15} name="code" required
                                   value={this.state.code} onChange={this.onChange}
                                   className="form-control"/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-2">Descripción:</strong>
                            <textarea className="form-control" name="description" rows={2}
                                   required value={this.state.description}
                                   onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-2">Convocatoria:</strong>
                            <input type="file" className="form-control" name="announcement"
                                   required value={this.announcement} onChange={this.onFile}
                                   accept="application/pdf" />
                        </div>
                    </section>

                    <section className="filter-section-dsch-multi-form mt-5">
                        <div className="col-4 p-1">
                            <strong className="ml-2">Fecha de Solicitud:</strong>
                            <input type="date" className="form-control" name="date_created"
                                   required value={this.state.date_created}
                                   onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-2">Fecha de envio :</strong>
                            <input type="date" className="form-control" name="date_send"
                                   required value={this.state.date_send}
                                   onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1 text-center">
                            <button className="btn-dsch mt-4">Guardar Convocatoria</button>
                        </div>
                    </section>

                </form>
            </div>
        )
    }
}

const mapStateProps = (state) => ({

});

export default connect(mapStateProps, { create_concourse })(Concourse);