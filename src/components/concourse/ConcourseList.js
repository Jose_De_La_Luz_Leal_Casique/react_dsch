import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import PropType from "prop-types";

import ListConcourse from "./components/ListConcourse";
import Year from "../academic/components/Year";
import { get_list_concourse } from "../../actions/concourse/listconcourse";


export class ConcourseList extends  Component {

    state = {
        year: new Date().getFullYear().toString(),
        status: 1
    }

    static propTypes = {
        get_list_concourse: PropType.func.isRequired,
        type_concourse: PropType.number.isRequired,
    }

    componentDidMount() {
        this.props.get_list_concourse(this.state.year, this.state.status, this.props.type_concourse);
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_list_concourse(this.state.year, this.state.status, this.props.type_concourse);
    }

    render() {
        return (
            <Fragment>
                <section className="col-12 mx-auto alert alert-warning mt-3">
                    <section className="text-center mt-3">
                        <h4>
                            <strong>
                                {
                                    this.props.type_concourse === 1
                                        ? 'Listado de Concursos de Educación Curricular'
                                        :
                                    this.props.type_concourse === 2
                                        ? 'Listado de Concursos de Cátedra'
                                        :
                                    this.props.type_concourse === 3
                                        ? 'Listado de Concursos de Oposición'
                                        :
                                    this.props.type_concourse === 4
                                        ? 'Listado de Profesores Visitantes'
                                        :
                                        null
                                } (Seleccionar año y estado)
                            </strong>
                        </h4>
                    </section>
                    <form className="col-7 mx-auto" onSubmit={this.onSubmit}>
                        <section className="py-1 m-auto filter-section-dsch-multi-form">
                            <div className="col-5">
                                <Year
                                    select_year={this.state.year.toString()}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="col-5">
                                <select className="form-control" value={this.state.status} name="status"
                                        onChange={this.onChange}>
                                    <option key={0} value={1}>Pendientes</option>
                                    <option key={1} value={0}>Finalizados</option>
                                </select>
                            </div>
                            <div className="col-2">
                                <button className="btn btn-dsch" onClick={this.onSubmit}>
                                    Buscar
                                </button>
                            </div>
                        </section>
                    </form>
                </section>

                <section className="col-12 mt-3 mx-auto">
                    <ListConcourse type_concourse={this.props.type_concourse}/>
                </section>
            </Fragment>
        )
    }
}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { get_list_concourse })(ConcourseList);