import React,{ Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { SERVER } from "../../../actions/server";
import { create_aspirant } from '../../../actions/concourse/createaspirant';
import { set_delete_aspirant } from '../../../actions/concourse/deleteaspirant';
import { add_file_aspirant } from '../../../actions/concourse/addfileaspirant';
import { add_winner } from '../../../actions/concourse/addwinner';


export class Aspirants extends Component {

    state = {
        name: '',
        request: null,
        editFile: false,
        editAnnexed: false,
        file: null,
        annexed: null,
        winner: null
    }

    static propTypes = {
        aspirants: PropType.array,
        pk: PropType.number.isRequired,
        is_active: PropType.bool.isRequired,
        create_aspirant: PropType.func.isRequired,
        set_delete_aspirant: PropType.func.isRequired,
        add_file_aspirant: PropType.func.isRequired,
        add_winner: PropType.func.isRequired,

    }

    componentDidMount() {
        if (this.props.aspirants !== null){
            let exists = this.props.aspirants.find(aspirant => aspirant.winner === true);

            if (typeof(exists) != 'undefined'){
                this.setState({winner: exists});
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onChangeFile = (e) => {
        this.setState({request: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onEditFile = (e) => this.setState({editFile: true});
    onEditAnnexed = (e) => this.setState({editAnnexed: true})


    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('name', this.state.name);
        data.append('request', this.state.request, this.state.request.name);
        this.props.create_aspirant(data, this.props.pk);
        this.setState({name: ''});
    }

    onDeleteAspirant = (pk) => {
        if (window.confirm('¿Eliminar Este Registro?')){
            this.props.set_delete_aspirant(pk);
        }
    }

    onFileUpload = (e) => {
        this.setState({file: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onAnnexedUpload = (e) => {
        this.setState({annexed: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onFile = (pk) => {
        if (this.state.file !== null){
            const data = new FormData();
            data.append('file', this.state.file, this.state.file.name);
            this.props.add_file_aspirant(data, pk, 1);
        } else {
            alert('Agregar Archivo de Grado Académico')
        }
    }

    onAnnexed = (pk) => {
         if (this.state.annexed !== null){
            const data = new FormData();
            data.append('annexed', this.state.annexed, this.state.annexed.name);
            this.props.add_file_aspirant(data, pk, 2);
        } else {
            alert('Agregar Archivo Adicional')
        }
    }

    setWinnerConcourse = (aspirant) => {
        this.props.add_winner(aspirant.id);
        this.setState({winner: aspirant});
    }

    onWinner = (aspirant) => {
        if (this.state.winner !== null){
            if (window.confirm('Ya Existe Un Ganador. ¿Quiere Modificarlo?')){
                this.setWinnerConcourse(aspirant);
            }
        } else {
            this.setWinnerConcourse(aspirant);
        }

    }

    render() {

        let listAspirants = null;

        if (this.props.aspirants !== null){
            listAspirants = (
                this.props.aspirants.map((aspirant, index) => (
                    <div className={
                        `col-12 card mt-1 p-2 ${this.state.winner !== null ?
                            this.state.winner.id === aspirant.id ? 'bg-winner' : ''
                            :
                            ''}`
                    } tabIndex={index} key={aspirant.id}>

                        <div className="filter-section-dsch-multi-form">
                            <h6 className="mt-2 ml-2 col-3"><strong>{aspirant.name}</strong></h6>

                            <div className="col-2 text-center m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch"
                                   href={`${SERVER}${aspirant.request}`}>
                                    Solicitud
                                </a>
                            </div>

                            <div className="col-3 text-center m-auto">
                                {
                                    aspirant.file === null ?
                                        this.props.is_active === true ?
                                            this.state.editFile === true ?
                                                <div>
                                                    <input type="file" className="form-control"
                                                           name="file" value={this.file}
                                                           onChange={this.onFileUpload}
                                                           accept="application/pdf"
                                                    />
                                                    <button className="btn btn-primary"
                                                        onClick={this.onFile.bind(this, aspirant.id)}>
                                                        Guardar
                                                    </button>
                                                </div>
                                                :
                                                <button className="btn btn-warning"
                                                        onClick={this.onEditFile}>
                                                    Agregar
                                                </button>
                                           :
                                           'NO APLICA'
                                        :
                                        <a target="_blank" rel="noopener noreferrer"
                                           className="btn btn-dsch"
                                           href={`${SERVER}${aspirant.file}`}>
                                            Nivel Acad.
                                        </a>
                                }
                            </div>

                            <div className="col-3 text-center m-auto">
                                {
                                   aspirant.annexed === null ?
                                       this.props.is_active === true ?
                                           this.state.editAnnexed === true ?
                                                <div>
                                                    <input type="file" className="form-control"
                                                           name="annexed" value={this.annexed}
                                                           onChange={this.onAnnexedUpload}
                                                           accept="application/pdf"
                                                    />
                                                    <button className="btn btn-primary"
                                                        onClick={this.onAnnexed.bind(this, aspirant.id)}>
                                                        Guardar
                                                    </button>
                                                </div>
                                                :
                                                <button className="btn btn-warning"
                                                        onClick={this.onEditAnnexed}>
                                                    Agregar
                                                </button>
                                           :
                                           'NO APLICA'
                                        :
                                        <a target="_blank" rel="noopener noreferrer"
                                           className="btn btn-dsch"
                                           href={`${SERVER}${aspirant.annexed}`}>
                                            Anexo
                                        </a>
                                }
                            </div>

                            <div className="col-1 m-auto">

                                {
                                    this.props.is_active === true ?
                                        <section className="btn-group dropleft">
                                            <button className="btn btn-danger dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">

                                            </button>
                                            <div className="dropdown-menu">
                                                <button className="dropdown-item" onClick={
                                                    this.onDeleteAspirant.bind(this, aspirant.id)}>
                                                    Eliminar
                                                </button>
                                                <button className="dropdown-item btn-warning" onClick={
                                                    this.onWinner.bind(this, aspirant)}>
                                                    Ganador
                                                </button>
                                            </div>
                                        </section>
                                        :
                                        null
                                }
                            </div>
                        </div>
                    </div>
                ))

            );
        } else {
            listAspirants = (
                <h4></h4>
            );
        }

        return (
            <section className="card card-body bg-white mt-3">
                <div className='filter-section-dsch-multi-form mb-2'>
                    <div className="col-8 pt-2">
                        <div className="filter-section-dsch-multi-form">
                            {
                                this.state.winner === null ?
                                    <h4>Listado de Aspirantes</h4>
                                    :
                                    <h4 className="alert-danger bg-white"><strong>Ganador: </strong>
                                        <strong>{this.state.winner.name}</strong></h4>
                            }
                        </div>
                    </div>
                    <div className="col-4">
                        {this.props.is_active === true ?
                            <button className="btn btn-dsch float-right" data-toggle="modal"
                                data-target="#ModalRegisterAspirant">
                                Agregar Aspirante
                            </button>
                            : null
                        }
                    </div>
                </div>

                <section className="card card-footer">
                    <div className="filter-section-dsch-multi-form">

                        <div className="col-3 text-center m-auto">
                            <strong>ASPIRANTE</strong>
                        </div>
                        <div className="col-2 text-center m-auto">
                            <strong>SOLICITUD</strong>
                        </div>
                        <div className="col-3 text-center m-auto">
                            <strong>GRADO ACAD.</strong>
                        </div>
                        <div className="col-3 text-center m-auto">
                            <strong>ANEXO</strong>
                        </div>
                        <div className="col-1 text-center m-auto">
                        </div>
                    </div>
                    {listAspirants}
                </section>

                <div className="modal fade" id="ModalRegisterAspirant" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header alert-danger text-dark">
                                <h5 className="modal-title" id="exampleModalLongTitle">
                                    Registro de Aspirante
                                </h5>
                                <button type="button" className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form className="col-12 m-auto" onSubmit={this.onSubmit}>
                                    <div className="filter-section-dsch-multi-form">
                                        <h6 className="mt-3 col-3" ><strong>Nombre:</strong></h6>
                                        <input type="text" className="form-control" name="name"
                                           value={this.state.name} onChange={this.onChange} required
                                        />
                                    </div>

                                    <div className="filter-section-dsch-multi-form">
                                        <h6 className="mt-3 col-3"><strong>Solicitud:</strong></h6>
                                        <input type="file" className="form-control" name="request"
                                           value={this.request} onChange={this.onChangeFile}
                                               required accept="application/pdf"
                                        />
                                    </div>

                                    <div className="filter-section-dsch-multi-form mt-4">
                                        <div className="col-8"></div>
                                        <button className="btn btn-dsch float-right col-4">
                                            Guardar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, {
        create_aspirant, set_delete_aspirant, add_file_aspirant, add_winner})(Aspirants);