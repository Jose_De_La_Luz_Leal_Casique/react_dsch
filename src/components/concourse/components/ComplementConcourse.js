import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import Initials from "./Initials";
import Complements from "./Complements";
import Aspirants from "./Aspirants";
import Additional from "./Additional";
import Teacher from "./Teacher";


export class ComplementConcourse extends  Component {

    state = {
        concourse: true,
        complement: false,
        aspirants: false,
        additional: false,
        teacher: false,
    }

    static propTypes = {
        concourse: PropType.object,
        departament: PropType.string,
        type_concourse: PropType.number
    }


    onChaneOption = (e) => {
        let concourse = false;
        let complement = false;
        let aspirants = false;
        let additional = false;
        let teacher = false;

        switch (e.target.name) {

            case 'concourse':
                concourse = true;
                break;

            case 'complement':
                complement = true;
                break;

            case 'aspirants':
                aspirants = true;
                break;

            case 'additional':
                additional = true;
                break;

            case 'teacher':
                teacher = true;
                break;

            default:
                break;
        }

        this.setState(
            {
                concourse: concourse,
                complement: complement,
                aspirants: aspirants,
                additional: additional,
                teacher: teacher,
            }
        );
    }

    render() {

        let optionView = null;

        if (this.state.concourse === true){
            optionView = <Initials
                concourse={this.props.concourse}
                user_view={this.state.user_view}
                type_concourse={this.props.type_concourse}
            />

        } else if (this.state.complement === true){
            optionView = <Complements
                concourse={this.props.concourse} type_concourse={this.props.type_concourse}
            />

        } else if (this.state.aspirants === true) {
            optionView = <Aspirants
                aspirants={this.props.concourse.aspirants}
                pk={this.props.concourse.id}
                is_active={this.props.concourse.is_active}
            />

        } else if (this.state.additional === true){
            optionView = <Additional
                concourse={this.props.concourse} type_concourse={this.props.type_concourse}
            />
        } else if (this.state.teacher === true){
            optionView = <Teacher
                concourse={this.props.concourse} type_concourse={this.props.type_concourse}
            />
        }

        return (
            <section>
                <section className="m-1">
                    {
                        this.state.concourse === false ?
                            <button className="btn btn-primary mr-3" name="concourse"
                                    onClick={this.onChaneOption}>
                                Convocatoria
                            </button>
                            :
                            null
                    }
                    {
                        this.state.complement === false ?
                            <button className="btn btn-info mr-3" name="complement"
                                    onClick={this.onChaneOption}>
                                Complementos
                            </button>
                            :
                            null
                    }
                    {
                        this.state.aspirants === false & this.props.type_concourse !== 4 ?
                            <button className="btn btn-danger mr-3" name="aspirants"
                                    onClick={this.onChaneOption}>
                                Aspirantes
                            </button>
                            :
                            null
                    }

                    {
                        this.state.additional === false  & this.props.type_concourse === 3 ?
                            <button className="btn btn-secondary" name="additional"
                                    onClick={this.onChaneOption}>
                                Archivos adicionales
                            </button>
                            :
                            null
                    }

                    {
                        this.state.teacher === false  & this.props.type_concourse === 4 ?
                            <button className="btn btn-danger" name="teacher"
                                    onClick={this.onChaneOption}>
                                Profesor Visitante
                            </button>
                            :
                            null
                    }

                </section>
                <section className="mt-1">
                    { optionView }
                </section>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null)(ComplementConcourse);