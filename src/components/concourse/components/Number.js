import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { add_number } from '../../../actions/concourse/addnumber'


export class Number extends Component {

    state = {
        edit: false,
        number: '',
    }

    static propTypes = {
        concourse: PropType.object.isRequired,
        add_number: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({edit: false});

    onSendNumber = (e) => {
        if (this.props.concourse.is_active === true){
            if (this.state.number.trim() === ''){
                alert("Agregar Número Económico");
                this.setState({number: ''})
            }

            this.props.add_number(this.state.number, this.props.concourse.id)
            this.setState({edit: false, number: ''});

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }

        this.setState({edit: false, number: ''});
    }

    render() {
        let viewNumber = null;

        if (this.props.concourse.number === null | this.state.edit === true){

            if (this.props.concourse.is_active === true) {
                viewNumber = (
                    <div>
                        <input type="text" className="form-control" required name="number"
                               placeholder="Ingrese Número Económico" value={this.state.number}
                               onChange={this.onChange} maxLength={10} minLength={5}
                        />
                        <section className="text-center mt-2">
                            <button className="btn btn-primary" onClick={this.onSendNumber}>
                                Guardar
                            </button>
                            {
                                this.props.concourse.number !== null ?
                                    <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                        Cancelar
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </div>
                );
            } else {
                viewNumber = (
                    <section className="text-center">
                        <h2>NO APLICA</h2>
                    </section>
                );
            }
        } else {
            viewNumber = (
                <section className="ml-3 text-center">
                    <h6>{this.props.concourse.number}</h6>
                    <div className="text-center">
                        {
                            this.props.concourse.is_active === true ?
                                <button className="btn btn-warning" onClick={this.onEdit}>
                                    Editar
                                </button>
                                :
                                null
                        }
                    </div>
                </section>
            )
        }

        return (
            <section className="card col-4 p-2 m-1">
                <h5 className="text-center"><strong>Número Económico</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {viewNumber}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({

});

export default connect(mapStateToProps, { add_number })(Number);