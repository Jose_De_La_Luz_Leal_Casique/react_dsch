import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import Dictum from "./Dictum";
import Notification from "./Notification";
import StartNotification from "./StartNotification";
import Agreement from "./Agreement";
import SecondAspirants from "./SecondAspirants";
import Challenge from "./Challenge";
import FirstExtension from "./FirstExtension";
import SecondExtension from "./SecondExtension";


export class Complements extends  Component {

    static propTypes = {
        concourse: PropType.object,
        type_concourse: PropType.number.isRequired,
    }

    render() {
        return (
            <section>
                <section className="filter-section-dsch-multi-form">
                    <Dictum
                        dictum={this.props.concourse.dictum}
                        is_active={this.props.concourse.is_active}
                        pk={this.props.concourse.id}
                    />

                    {
                        this.props.type_concourse === 4
                            ? null
                            :
                            <Notification
                                notification={this.props.concourse.start_notification}
                                is_active={this.props.concourse.is_active}
                                pk={this.props.concourse.id}
                            />
                    }

                </section>
                {
                    this.props.type_concourse === 2
                        ?
                        <section className="filter-section-dsch-multi-form mt-3">
                            <Agreement
                                agreement={this.props.concourse.agreement}
                                is_active={this.props.concourse.is_active}
                                pk={this.props.concourse.id}
                            />
                        </section>
                        :
                        null
                }

                {
                    this.props.type_concourse === 3
                        ?
                        <section>
                            <section className="filter-section-dsch-multi-form mt-3">
                                <StartNotification
                                    notification={this.props.concourse.start_notification}
                                    is_active={this.props.concourse.is_active}
                                    pk={this.props.concourse.id}
                                />

                                <SecondAspirants
                                    notification={this.props.concourse.second_aspirants}
                                    is_active={this.props.concourse.is_active}
                                    pk={this.props.concourse.id}
                                />
                            </section>
                            <section className="filter-section-dsch-multi-form mt-3">
                                <Challenge
                                    notification={this.props.concourse.challenge}
                                    is_active={this.props.concourse.is_active}
                                    pk={this.props.concourse.id}
                                />
                            </section>
                        </section>
                        :
                        null
                }


                {
                    this.props.type_concourse === 3
                        ? null
                        :
                        <section className="filter-section-dsch-multi-form mt-3">
                            <FirstExtension
                                concourse={this.props.concourse}
                            />
                            <SecondExtension
                                concourse={this.props.concourse}
                            />
                        </section>
                }

            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null)(Complements);