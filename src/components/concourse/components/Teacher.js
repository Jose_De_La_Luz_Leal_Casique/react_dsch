import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import Departaments from "../../academic/components/Departaments";
import TeacherFiles from "./TeacherFiles";
import { add_teacher } from "../../../actions/concourse/add_teacher";


export class Teacher extends  Component {

    state = {
        number: '',
        name: '',
        paternal_surname: '',
        maternal_surname: '',
        mail_personal: '',
        mail_institutional: '',
        departament: 'Humanidades',
        file: null
    }

    static propTypes = {
        concourse: PropType.object,
        type_concourse: PropType.number.isRequired,
        add_teacher: PropType.func.isRequired
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value});
    onFileNotification = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('number', this.state.number);
        data.append('name', this.state.name);
        data.append('paternal_surname', this.state.paternal_surname);
        data.append('maternal_surname', this.state.maternal_surname);
        data.append('mail_personal', this.state.mail_personal);
        data.append('mail_institutional', this.state.mail_institutional);
        data.append('area', this.state.departament);

        try {
            data.append('file', this.state.file, this.state.file.name);
            this.props.add_teacher(data, this.props.concourse.id)
        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    this.props.add_teacher(data, this.props.concourse.id)
                }
            }
        }

        document.getElementById('formCreate').reset();
    }

    render() {

        const formNewUser = (
            <section className="col-12 card mt-3 pt-3 pb-5 px-4">
                <h5 className="text-center"><strong>Registro de Profesor Visitante</strong></h5>
                <form className="col-12 card mt-3 pt-2 alert-dark pb-3"
                      onSubmit={this.onSubmit} id="formCreate">
                    <section className="filter-section-dsch-multi-form mt-2">
                        <div className="col-4 p-1">
                            <strong className="ml-1">Nombre(s):</strong>
                            <input type="text" className="form-control" name="name"
                                      required value={this.state.name} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-1">Apellido Paterno:</strong>
                            <input type="text" className="form-control" name="paternal_surname"
                                   required value={this.state.paternal_surname}
                                   onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-1">Apellido Materno:</strong>
                            <input type="text" className="form-control" name="maternal_surname"
                                   required value={this.state.maternal_surname}
                                   onChange={this.onChange}/>
                        </div>

                    </section>


                    <section className="filter-section-dsch-multi-form mt-4">
                        <div className="col-4 p-1">
                            <strong className="ml-1">Número de Control:</strong>
                            <input type="text" className="form-control" name="number"
                                      required value={this.state.number} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-1">Departamento:</strong>
                            <Departaments
                                select_departament={this.state.departament}
                                onChange={this.onChange}
                            />
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-1">Email Personal:</strong>
                            <input type="text" className="form-control" name="mail_personal"
                                   required value={this.state.mail_personal}
                                   onChange={this.onChange}/>
                        </div>

                    </section>

                    <section className="filter-section-dsch-multi-form mt-4">

                        <div className="col-4 p-1">
                            <strong className="ml-1">Email Institucional:</strong>
                            <input type="text" className="form-control" name="mail_institutional"
                                   value={this.state.mail_institutional} onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-1">Solicitud:</strong>
                            <input type="file" className="form-control" name="file"
                                   value={this.file} onChange={this.onFileNotification}
                                   accept="application/pdf"
                            />
                        </div>

                        <div className="col-4 p-1 my-auto">
                            <button className="btn btn-dark float-right mt-4">Guardar Registro</button>
                        </div>

                    </section>

                </form>
            </section>
        );

        const ViewUser = (object) => (
            <section className="col-12 card mt-3 pt-3 pb-5 px-4">
                <h5 className="text-center"><strong>Profesor Visitante</strong></h5>
                <section className="filter-section-dsch-multi-form alert-warning mt-2 py-3">
                    <div className="col-4">
                        <h6>
                            <strong>
                                Nombre:
                            </strong>
                            {' '+object.user.name}
                            {' '+object.user.paternal_surname}
                            {' '+object.user.maternal_surname}
                        </h6>
                    </div>

                    <div className="col-4">
                        <h6>
                            <strong>
                                Número Económico:
                            </strong>
                            {' '+object.user.user}
                        </h6>
                    </div>

                    <div className="col-4">
                        <h6>
                            <strong>
                                Departamento:
                            </strong>
                            {' '+object.user.area}
                        </h6>
                    </div>
                </section>
                <section className="filter-section-dsch-multi-form alert-warning mt-2 py-3">
                    <div className="col-6">
                        <h6>
                            <strong>
                                Email Personal:
                            </strong>
                            {' '+object.user.mail_personal}
                        </h6>
                    </div>

                    <div className="col-6">
                        <h6>
                            <strong>
                                Email Institucional:
                            </strong>
                            {
                                object.user.mail_personal === object.user.mail_institutional
                                    ? ' PENDIENTE'
                                    :
                                    ' '+object.user.mail_institutional
                            }
                        </h6>
                    </div>
                </section>
                <hr></hr>
                <section>
                    <TeacherFiles teacher={this.props.concourse.teacher}/>
                </section>
            </section>
        );

        return (
            <section>
                <section className="filter-section-dsch-multi-form">
                    {
                        this.props.concourse.teacher === null
                            ?
                            formNewUser
                            :
                            ViewUser(this.props.concourse.teacher)
                    }
                </section>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_teacher })(Teacher);