import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { add_file_extend } from '../../../actions/concourse/addfileextend'
import {SERVER} from "../../../actions/server";


export class FileExtend extends Component {

    state = {
        edit: false,
        file_aspirants: null
    }

    static propTypes = {
        concourse: PropType.object,
        add_file_extend: PropType.func.isRequired,
        type_concourse: PropType.number.isRequired,
    }

    onEdit = (e) => this.setState({ edit: true });
    onFileExtend = (e) => this.setState({file_aspirants: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('file_aspirants', this.state.file_aspirants, this.state.file_aspirants.name);
        this.props.add_file_extend(data, this.props.concourse.id);
        this.setState({edit: false});
    }

    render() {

        let fileExtend = null;

        if (this.props.concourse !== null) {

            if (this.props.concourse.file_aspirants === null | this.state.edit === true) {

                if (this.props.concourse.is_active === true) {
                    fileExtend = (
                        <div className="mt-3 text-center">
                            <form onSubmit={this.onSubmit}>
                                <input type="file" className="form-control" name="file_aspirants"
                                       required value={this.file_aspirants} onChange={this.onFileExtend}
                                       accept="application/pdf"
                                />
                                <button className="btn btn-primary mt-2">Guardar</button>
                            </form>
                        </div>
                    );
                } else {
                    fileExtend = (
                        <section className="text-center">
                            <h2>NO APLICA</h2>
                        </section>
                    );
                }
            } else {
                fileExtend = (
                    <div className="filter-section-dsch-multi-form mt-3">
                        <section className="m-auto">
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.concourse.file_aspirants}`}>
                                <strong>Ver Archivo </strong>
                            </a>
                        </section>
                        <br></br>
                        {
                            this.props.concourse.is_active === true ?
                                <button className="btn btn-warning m-auto" onClick={this.onEdit}>
                                    Editar
                                </button>
                                :
                                null
                        }
                    </div>
                );
            }
        }

        return (
            <section className="card col-4 p-2 m-1">
                <h5 className="text-center"><strong>Lista de Aspirantes</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {fileExtend}
                </div>
            </section>
        )
    }
}


const mapStateToProps = (state) => ({
});

export default connect(mapStateToProps, {add_file_extend})(FileExtend);
