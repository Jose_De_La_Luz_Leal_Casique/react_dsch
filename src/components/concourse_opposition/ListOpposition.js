import React, { Component } from 'react';
import { connect } from "react-redux";

import ConcourseList from "../concourse/ConcourseList";


export class ListOpposition extends  Component {

    render() {
        return (
            <ConcourseList type_concourse={3}/>
        )
    }
}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null )(ListOpposition);