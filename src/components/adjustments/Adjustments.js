import React, { Component, Fragment } from 'react';
import PlansAdjustments from "./components/PlansAdjustments";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ListAdjustments from "./components/ListAdjustments";
import FormAdjustment from "./components/FormAdjustment";
import {
    get_list_adjustments_for_plan } from '../../actions/adjustments/get_list_adjustments_for_plan';


export class Adjustments extends Component {

    state = {
        academic_curricula: 1
    }

    static propTypes = {
        get_list_adjustments_for_plan: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_list_adjustments_for_plan(this.state.academic_curricula);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
        this.props.get_list_adjustments_for_plan(e.target.value);
    };

    render() {
        return (
            <Fragment>
                <section className="col-12 mx-auto alert alert-dark mt-3" >
                    <section className="filter-section-dsch-multi-form ">
                        <div className="col-4 p-2">
                        <div className="col-10 mx-auto text-center">
                            <h5><strong>Selecionar Plan de Estudio</strong></h5>
                        </div>
                        <div className="col-11 mx-auto">
                            <PlansAdjustments
                                onChange={this.onChange}
                                select_plan={this.state.academic_curricula.toString()}
                            />
                        </div>
                    </div>
                    <div className="col-4"></div>
                    <div className="col-4">
                        <button className="btn-dsch float-right mt-4"
                                data-toggle="modal" data-target="#AddAdjustment">
                            Crear plan de estudios
                        </button>
                    </div>
                    </section>
                </section>
                <section className="col-11 mx-auto mt-4" >
                    <ListAdjustments />
                </section>
                <FormAdjustment />
            </Fragment>
        )
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { get_list_adjustments_for_plan })(Adjustments);