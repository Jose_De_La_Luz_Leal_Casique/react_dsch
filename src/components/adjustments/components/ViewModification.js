import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import EditModification from "./EditModification";
import { FormatDate } from "../../FormatDate"
import { SERVER } from "../../../actions/server";



export class ViewModification extends Component {

    state = {
        edit: false,
    }

    static propTypes = {
        obj: PropTypes.object,
    }

    onEnableEdit = (e) => this.setState({edit: this.state.edit ? false : true});
    onEditFalse = (e) => this.setState({edit: false});


    render() {
        let detailView = null;

        if (this.props.obj != null){
            detailView = (
                <div className="col-11 m-auto">
                    <h6><strong>Sesión: </strong>
                        {
                        this.props.obj.code === null
                            ? 'NO REGISTRADO' : this.props.obj.code
                        }
                    </h6>

                    <h6><strong>Fecha de Creación: </strong>
                        {
                        this.props.obj.date === null
                            ? 'NO REGISTRADO' : FormatDate(this.props.obj.date)
                        }
                    </h6>

                    <h6><strong>Descripción: </strong>
                        {
                        this.props.obj.description === null
                            ? 'NO REGISTRADO' : this.props.obj.description
                        }
                    </h6>

                    {
                        this.props.obj.file !== null
                            ?
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.obj.file}`}>
                                <strong>Ver Probatorio: </strong>
                            </a>
                            :
                            <h6><strong>Probatorio: </strong> PENDIENTE </h6>
                    }
                    <button onClick={this.onEnableEdit} className="btn btn-primary float-right">
                        Editar
                    </button>

                </div>
            )
        }

        return (
            <div className="modal fade" id="viewModification" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                <div className="modal-md modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title " id="exampleModalLongTitle">
                                Adecuaciones
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={this.onEditFalse}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-dark">
                            {
                                this.state.edit
                                    ?
                                    <EditModification
                                        obj={this.props.obj}
                                        edit={this.state.edit}
                                        onEnableEdit={this.onEnableEdit}
                                    />
                                    :
                                    detailView
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, null  )(ViewModification);