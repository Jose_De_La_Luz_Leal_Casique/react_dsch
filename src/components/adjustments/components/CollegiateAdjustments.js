import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { load_collegiate_adjustments } from '../../../actions/adjustments/load_collegiate_adjustments';


export class CollegiateAdjustments extends Component {

    static propTypes = {
        adjustments: PropTypes.object.isRequired,
        load_collegiate_adjustments: PropTypes.func.isRequired,
        select_collegiate: PropTypes.string
    }

    componentDidMount() {
        if (!this.props.adjustments.load_c){
            this.props.load_collegiate_adjustments();
        }
    }

    render() {

        if (this.props.adjustments.load_c){
            return (
                <select className="form-control" name="collegiate"
                        value={this.props.select_collegiate} onChange={this.props.onChange}>

                    {this.props.adjustments.collegiate.map((plan, index) => (
                        <option value={plan.id} key={index}>{plan.name}</option>
                    ))};

                </select>
            );
        } else{
            return <h3 className="text-center">Loading...</h3>
        }

    }
}

const mapStateToProps = (state) => ({
    adjustments: state.adjustments
});

export default connect(mapStateToProps, { load_collegiate_adjustments })(CollegiateAdjustments);
