import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { load_plans_adjustments } from '../../../actions/adjustments/load_plans_adjustments'


export class PlansAdjustments extends Component {

    static propTypes = {
        adjustments: PropTypes.object.isRequired,
        load_plans_adjustments: PropTypes.func.isRequired,
        select_plan: PropTypes.string
    }

    componentDidMount() {
        if (!this.props.adjustments.load){
            this.props.load_plans_adjustments();
        }
    }

    render() {

        if (this.props.adjustments.load){
            return (
                <select className="form-control" name="academic_curricula"
                        value={this.props.select_plan} onChange={this.props.onChange}>

                    {this.props.adjustments.plans.map((plan, index) => (
                        <option value={plan.id} key={index}>{plan.name}</option>
                    ))};

                </select>
            );
        } else{
            return <h3 className="text-center">Loading...</h3>
        }

    }
}

const mapStateToProps = (state) => ({
    adjustments: state.adjustments
});

export default connect(mapStateToProps, { load_plans_adjustments })(PlansAdjustments);
