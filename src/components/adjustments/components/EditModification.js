import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { ConfigDateSave, FormatDate } from "../../FormatDate";
import { edit_modification } from "../../../actions/adjustments/edit_modification";


export class FormModification extends Component {

    state = {
        collegiate: 1,
        file: null,
        code: '',
        date: '',
        description: ''
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_modification: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            code: this.props.obj.code,
            description: this.props.obj.description,
        });
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('code', this.state.code);
        data.append('description', this.state.description);

        if (this.state.date.trim() !== ''){
            data.append('date', ConfigDateSave(this.state.date));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_modification(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_modification(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="mx-auto col-11" onSubmit={this.onSubmit} id="form">

                <div className="mt-1">
                    <h6><strong>Número de Sesión:</strong></h6>
                    <input type="text" className="form-control text-center col-12"
                           name="code" value={this.state.code}
                           onChange={this.onChange}/>
                </div>

                <div className="mt-3">
                    <h6><strong>Descripción:</strong></h6>
                    <textarea className="form-control text-center col-12" rows={3}
                           name="description" value={this.state.description}
                           onChange={this.onChange} required />
                </div>

                <div className="mt-3">
                    <h6>
                        <strong>Fecha de Creación: </strong> (Actual-

                        {
                            this.props.obj.date !== null
                                ? FormatDate(this.props.obj.date) : null
                        }
                        )

                    </h6>
                    <input type="date" className="form-control text-center col-12"
                           name="date" value={this.state.date}
                           onChange={this.onChange}/>
                </div>

                <div className="mt-3">
                    <h6><strong>Seleccionar Archivo:</strong></h6>
                    <input type="file" className="form-control text-center col-12"
                           name="file" value={this.file} accept="application/pdf"
                           onChange={this.onChangeFile}/>
                </div>

                <div className="col-12 mx-auto mt-4 pt-2">
                    <button className="btn btn-primary float-right">
                        Guardar
                    </button>
                    <button onClick={this.props.onEnableEdit}
                            className="btn btn-danger float-right mr-3" >
                        Cancelar
                    </button>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_modification } )(FormModification);