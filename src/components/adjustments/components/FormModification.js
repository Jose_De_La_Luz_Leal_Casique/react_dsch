import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { ConfigDateSave } from '../../FormatDate';
import { add_modification } from "../../../actions/adjustments/add_modification";
import CollegiateAdjustments from "./CollegiateAdjustments";


export class FormModification extends Component {

    state = {
        collegiate: 1,
        file: null,
        code: '',
        date: '',
        description: ''
    }

    static propTypes = {
        add_modification: PropTypes.func.isRequired,
        pk: PropTypes.number
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('code', this.state.code);
        data.append('description', this.state.description);
        data.append('collegiate', this.state.collegiate);

        if (this.state.date !== ''){
            data.append('date', ConfigDateSave(this.state.date));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_modification(this.props.pk, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_modification(this.props.pk, data);
                }
            }
        }

        this.setState({
            collegiate: 1,
            file: null,
            code: '',
            date: '',
            description: ''
        });
        document.getElementById('form').reset();
    }

    render() {
        return (
            <div className="modal fade" id="AddModification" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Registro de Adecuaciones
                            </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="mx-auto col-11" onSubmit={this.onSubmit} id="form">

                                <div className="mt-1">
                                    <h6><strong>Número de Sesión:</strong></h6>
                                    <input type="text" className="form-control text-center col-12"
                                           name="code" value={this.state.code}
                                           onChange={this.onChange}/>
                                </div>

                                <div className="mt-3">
                                    <h6><strong>Descripción:</strong></h6>
                                    <textarea className="form-control text-center col-12" rows={2}
                                           name="description" value={this.state.description}
                                           onChange={this.onChange} required />
                                </div>

                                <div className="mt-3">
                                    <h6><strong>Fecha de Propuesta:</strong></h6>
                                    <input type="date" className="form-control text-center col-12"
                                           name="date" value={this.state.date}
                                           onChange={this.onChange}/>
                                </div>

                                <div className="mt-3">
                                    <h6><strong>Órgano Colegiado:</strong></h6>
                                    <CollegiateAdjustments
                                        select_collegiate={this.state.collegiate.toString()}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="mt-3">
                                    <h6><strong>Seleccionar Archivo:</strong></h6>
                                    <input type="file" className="form-control text-center col-12"
                                           name="file" value={this.file} accept="application/pdf"
                                           onChange={this.onChangeFile}/>
                                </div>

                                <div className="mt-3">
                                    <button className="btn btn-primary float-right">
                                        Guardar Adecuación
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_modification } )(FormModification);
