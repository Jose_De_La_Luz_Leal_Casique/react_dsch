import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FormatDate } from '../../FormatDate';
import {Link} from "react-router-dom";


export class ListAdjustments extends Component {

    static propTypes = {
        adjustments: PropTypes.object.isRequired,
    }

    render() {

        if (this.props.adjustments.list.length > 0){
            return (
                <section className="modal-footer">
                    {this.props.adjustments.list.map((plan, index) => (
                        <div className="col-6 mx-auto" tabIndex={index} key={plan.id}>
                            <div className="card card-body px-2 py-3 alert alert-warning">
                                <h6>
                                    <strong>Plan de Estudio:</strong> {plan.academic_curricula}
                                </h6>
                                <h6>
                                    <strong>Número de Sesión:</strong> {plan.code}
                                </h6>
                                <h6>
                                    <strong>Fecha de Propuesta:</strong> {FormatDate(plan.date)}
                                </h6>
                                <h6>
                                    <strong>Descripción:</strong> {plan.description}
                                </h6>
                                <div className="col-12">
                                    <Link to={{
                                        pathname: '/adjustments-edit',
                                        state: {id: plan.id}
                                    }} >
                                        <button className="btn-dsch float-right">
                                            Ver Detalle
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    ))}
                </section>
            );
        } else{
            return (
                <div className="alert col-8 mx-auto alert alert-danger mt-5 pb-5">
                    <h2 className="text-center mt-5"><strong>NO HAY COINCIDENCIAS</strong></h2>
                </div>
            );
        }

    }
}

const mapStateToProps = (state) => ({
    adjustments: state.adjustments
});

export default connect(mapStateToProps, null )(ListAdjustments);
