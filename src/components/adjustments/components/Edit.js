import React, { Component, Fragment } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import FormModification from "./FormModification";
import ListModification from "./ListModification";
import { get_retrieve_adjustments } from "../../../actions/adjustments/get_retrieve_adjustments";
import { FormatDate } from "../../FormatDate";
import {SERVER} from "../../../actions/server";


export class Edit extends Component {

    state = {
        pk: 0,
        view: 1,
    }

    static propTypes = {
        get_retrieve_adjustments: PropTypes.func.isRequired,
        adjustments: PropTypes.object.isRequired
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({ pk: params.id });
        this.props.get_retrieve_adjustments(params.id);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

    };

    render() {

        if (this.props.adjustments.view !== null){
            return (
                <Fragment>
                    <section className="alert alert-warning mt-3 col-12 mx-auto">
                        <section className="filter-section-dsch-multi-form">
                            <div className="col-4">
                                <h6>
                                    <strong>PLAN DE ESTUDIO: </strong>
                                    {this.props.adjustments.view.academic_curricula}
                                </h6>
                                <h6>
                                    <strong>ÓRGANO COLEGIADO: </strong>
                                    Sesión <b> {this.props.adjustments.view.code}</b>
                                </h6>
                                <h6>
                                    <strong>FECHA DE PROPUESTA: </strong>
                                    {FormatDate(this.props.adjustments.view.date)}
                                </h6>
                            </div>
                            <div className="col-4 text-justify">
                                <h6>
                                    <strong>BREVE DESCRIPCIÓN: </strong>
                                    {this.props.adjustments.view.description}
                                </h6>
                                {
                        this.props.adjustments.view.file !== null
                            ?
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.adjustments.view.file}`}>
                                <strong>Ver Probatorio: </strong>
                            </a>
                            :
                            <h6><strong>Probatorio: </strong> PENDIENTE </h6>
                    }
                            </div>
                            <div className="col-1"></div>
                            <div className="col-3">
                                <div className="col-10 bg-warning alert text-center float-right">
                                    <button className="btn-dsch"
                                        data-toggle="modal" data-target="#AddModification">
                                        Agregar Adecuación
                                    </button>
                                </div>
                            </div>
                        </section>
                    </section>
                    <section className=" col-12 mx-auto filter-section-dsch-multi-form">
                        <ListModification
                            modifications={this.props.adjustments.modifications}
                        />

                    </section>
                    <FormModification
                        pk={this.props.adjustments.view.id}
                    />
                </Fragment>
            );
        } else {
            return (
                <section className="text-center mt-5 pt-5">
                    <h1 className="mt-5 pt-5"><strong>LOADING...</strong></h1>
                </section>
            )
        }
    }

}

const mapStateToProps = (state) => ({
    adjustments: state.adjustments
});

export default connect(mapStateToProps, { get_retrieve_adjustments } )(Edit);