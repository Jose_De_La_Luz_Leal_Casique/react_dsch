import React, { Component, Fragment} from 'react';
import { connect } from "react-redux";
import PropType from 'prop-types';

import { SERVER } from '../../actions/server';

import FormDictumFile from './components/FormDictumFile';
import FormNotificationFile from "./components/FormNotificationFile";
import FormResolutionFile from "./components/FormResolutionFile";
import FormReplyFile from "./components/FormReplyFile";
import FormBisFile from "./components/FormBisFile";
import FormReceiptBisFile from "./components/FormReceiptBisFile";
import FormReceiptDictum from "./components/FormReceiptDictum";
import Finalized from "./components/Finalized";

import { FormatDate } from '../FormatDate';
import { get_scholarship } from '../../actions/scholarship/get_scholarship';
import { get_list_additional_files } from '../../actions/scholarship/get_list_additional_files';
import {Redirect} from "react-router-dom";


export class ScholarshipEdit extends Component {

    state = {
        id: 0,
    }

    static propTypes = {
        get_scholarship: PropType.func.isRequired,
        get_list_additional_files: PropType.func.isRequired,
        register: PropType.object,
        inProcess: PropType.bool,
        delete: PropType.bool
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({ id: params.id });
        this.props.get_scholarship(params.id);
        this.props.get_list_additional_files(params.id);
    }

    render() {

        if (this.props.inProcess === true){
            return (
                <Fragment>
                    <section className="filter-section-dsch-multi-form mt-3">
                        <section className="col-4 card ">
                            <h4 className="text-center mt-2">
                                <strong>SOLICITUD: </strong> {this.props.register.number_request}
                            </h4>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Usuario: </strong>{this.props.register.full_name}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Número Económico: </strong>{this.props.register.user}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Fecha de Solicitud: </strong>
                                    {FormatDate(this.props.register.date_request)}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Fecha de Envió: </strong>
                                    {this.props.register.date_send_request != null ?
                                        FormatDate(this.props.register.date_send_request):
                                        'Pendiente'}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Fecha de Publicación del dictamen:</strong><br></br>
                                    {this.props.register.date_get_request != null ?
                                        FormatDate(this.props.register.date_get_request):
                                        'Pendiente'}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Estado de Solicitud: </strong>
                                    {this.props.register.finalized === false ?
                                        'Pendiente':
                                        'Finalizado'}
                                </label>
                            </div>
                            <div className="dsch-det-card alert-dark">
                                <label>
                                    <strong>Archivo de Solicitud: </strong>
                                    <a target="_blank" rel="noopener noreferrer"
                                       href={`${SERVER}${this.props.register.file_request}`}>
                                        Ver Archivo
                                    </a>
                                </label>
                            </div>

                            <Finalized
                                id={this.state.id}
                            />
                        </section>

                        <section className="col-8">
                            <section className="filter-section-dsch-multi-form">
                                <FormDictumFile />
                                <FormReceiptDictum />
                            </section>

                            <section className="filter-section-dsch-multi-form">
                                <FormNotificationFile />
                                <FormResolutionFile />
                            </section>

                            <section className="filter-section-dsch-multi-form">
                                <FormBisFile />
                                <FormReceiptBisFile />
                            </section>
                        </section>
                    </section>
                </Fragment>
            );
        } else if(this.props.delete === true){
            return <Redirect to="/" />;

        } else {
            return <h3>Loading...</h3>
        }


    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register,
    inProcess: state.complete.inProcess,
    delete: state.complete.delete
});

export default connect(
    mapStateToProps, {get_scholarship, get_list_additional_files})(ScholarshipEdit);