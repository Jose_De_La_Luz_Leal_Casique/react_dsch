import React, { Component} from 'react';
import { connect } from "react-redux";
import PropType from 'prop-types';

import {register_finalized_scholarship} from '../../../actions/scholarship/register_finalized_scholarship';
import {deleted_scholarship} from '../../../actions/scholarship/delete_scholarship';
import ModalScholarshipSuccess from "./ModalScholarshipSuccess";
import FilesExtra from "./FilesExtra";

export class ScholarshipEdit extends Component {

    static propTypes = {
        register_finalized_scholarship: PropType.func.isRequired,
        deleted_scholarship: PropType.func.isRequired,
        register: PropType.object,
        id: PropType.number,
    }


    onFinalized = (e) => {
        this.props.register_finalized_scholarship(this.props.id);
    }

    onDeleted = (e) => {
        if (window.confirm('Desea Eliminar esta Solicitud')){
            this.props.deleted_scholarship(this.props.id);
        }
    }

    render() {

        let buttonStatus = null;

        if (this.props.register.finalized === false & this.props.register.file_receipt_dictum !== null) {

            buttonStatus = (
                <button className="btn btn-file-academic" onClick={this.onFinalized}>
                    Finalizar Solicitud
                </button>
            );
            /*return (
                <button className="btn btn-file-academic m-auto" onClick={this.onFinalized}>
                    Finalizar Proceso
                </button>

            );*/
        } else if (this.props.register.finalized === true) {
            buttonStatus = (
                <button className="btn btn-primary m-auto" onClick={this.onFinalized}>
                    Editar Solicitud
                </button>
            );
            /*return (
                <button className="btn btn-primary m-auto" onClick={this.onFinalized}>
                    Editar Proceso
                </button>
            );*/
        }
        return (
                <section className="form-group text-center mt-2 filter-section-dsch-multi-form">
                    <button className="btn btn-danger m-auto" onClick={this.onDeleted}>
                        Eliminar Solicitud
                    </button>
                    {buttonStatus}
                    <div className="dropdown m-auto">
                        <a className="btn btn-warning dropdown-toggle" href="#" role="button"
                           id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            Más
                        </a>

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <button className="dropdown-item" data-toggle="modal"
                                    data-target="#FilesExtra">
                                Archivos Adicionales
                            </button>
                            <button className="dropdown-item" data-toggle="modal"
                                    data-target="#SuccessModal">
                                Guardar Resultados
                            </button>
                        </div>
                    </div>

                    <ModalScholarshipSuccess />
                    <FilesExtra />
                </section>
            )
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register,
    inProcess: state.complete.inProcess
});

export default connect(
    mapStateToProps, {register_finalized_scholarship, deleted_scholarship})(ScholarshipEdit);