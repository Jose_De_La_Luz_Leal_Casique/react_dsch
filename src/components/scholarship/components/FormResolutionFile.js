import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";
import {register_resolution} from '../../../actions/scholarship/register_resolution';


class FormResolutionFile extends Component{

    state = {
        file_resolution: null,
        number_resolution: '',
        edit: false
    }

    static propTypes = {
        register: PropType.object,
        register_resolution: PropType.func.isRequired
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFile = (e) => {
        this.setState({file_resolution: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_resolution', this.state.number_resolution);
        data.append('file_resolution', this.state.file_resolution, this.state.file_resolution.name);
        this.props.register_resolution(data, this.props.register.id);
        this.setState({edit: false});
    }

    onEditFormFile = (e) => {
        this.setState({
            edit: this.state.edit === false ? true: false
        })
    }

    render() {


        const cancelButton = (
            <button className="btn btn-danger m-auto col-4" onClick={this.onEditFormFile}>
                Cancelar
            </button>
        );

        const editButton = (
            <button className="btn btn-primary btn-edit-files col-3 m-auto" onClick={this.onEditFormFile}>
                Editar
            </button>
        );

        const addButton = (
            <button className="btn btn-warning btn-edit-files col-3 m-auto">
                Agregar
            </button>
        );

        const formResolutionRequired = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Notificación de Dictamen</label>
                    <input type="text" className="form-control" name="number_resolution"
                           required onChange={this.onChange} value={this.state.number_resolution}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Notificación de Dictamen</label>
                    <input type="file" className="form-control" name="file_resolution"
                           required onChange={this.onLoadFile} accept="application/pdf"
                           value={this.file_resolution}/>
                </div>
                <div className="form-group text-center">
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );


        const formResolution = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Notificación de Dictamen</label>
                    <input type="text" className="form-control" name="number_resolution"
                           onChange={this.onChange} value={this.state.number_resolution}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Notificación de Dictamen</label>
                    <input type="file" className="form-control" name="file_resolution"
                           required onChange={this.onLoadFile} accept="application/pdf"
                           value={this.file_resolution}/>
                </div>
                <div className="form-group text-center">
                    {this.state.edit === true ? cancelButton: null}
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        if (this.props.register.file_resolution === null) {
            if (this.props.register.finalized === true){
                return (
                    <div className="col-6 card-form-file">
                        <div className="message-depend">
                            <h5 className="text-center">
                                NO APLICA - NOTIFICACIÓN DE DICTAMEN
                            </h5>
                        </div>
                    </div>
                );
            } else {
                return (
                    <section className="card col-6 card-form-file">
                        {formResolutionRequired}
                    </section>
                );
            }
        } else if (this.state.edit === true){
            return (
                <section className="card col-6 card-form-file">
                    {formResolution}
                </section>
            );
        } else {
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body">
                        <h6 className="text-center">
                            <strong>NOTIFICACIÓN DE DICTAMEN </strong>
                            {this.props.register.number_resolution}
                        </h6>

                        <section className="filter-section-dsch-multi-form">
                            <section className="alert alert-dark text-center col-5 m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.register.file_resolution}`}>
                                    Ver Archivo
                                </a>
                            </section>
                            {this.props.register.finalized === false ? editButton : null}
                            {/*this.props.register.finalized === false ? addButton : null*/}
                        </section>
                    </section>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, {register_resolution})(FormResolutionFile);