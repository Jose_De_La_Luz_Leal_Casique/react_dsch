import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from "prop-types";



export class OptionScholarship extends Component {

    static propTypes = {
        promotion: PropType.bool,
        stimulus: PropType.bool,
        trajectory: PropType.bool,
        permanence: PropType.bool,
        years: PropType.number,
        onChecked: PropType.func,
        onChange: PropType.func
    }

    render() {
        return (
            <section className="card card-body">
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" name="promotion"
                    value={this.props.promotion} onChange={this.props.onChecked}/>
                    <label className="form-check-label" htmlFor="promotion">Promoción</label>
                </div>

                <div className="form-check">
                    <input type="checkbox" className="form-check-input" name="stimulus"
                    value={this.props.stimulus} onChange={this.props.onChecked}/>
                    <label className="form-check-label" htmlFor="stimulus">
                        Estímulo a la Docencia e Investigación
                    </label>
                </div>

                <div className="form-check">
                    <input type="checkbox" className="form-check-input" name="trajectory"
                    value={this.props.trajectory} onChange={this.props.onChecked}/>
                    <label className="form-check-label" htmlFor="trajectory">
                        Estímulo a la Trayectoria Académica Sobresaliente
                    </label>
                </div>

                <section className="filter-section-dsch-multi-form">
                    <div className="form-check col-8">
                        <input type="checkbox" className="form-check-input" name="permanence"
                        value={this.props.permanence} onChange={this.props.onChecked}/>
                        <label className="form-check-label" htmlFor="permanence">
                            Beca de Apoyo a la Permanecia
                        </label>
                    </div>
                    <div className="col-3">
                        <select className="form-control" value={this.props.years} name="years"
                                onChange={this.props.onChange}>
                            <option key={1} value={1}>1 año</option>
                            <option key={2} value={2}>2 años</option>
                            <option key={3} value={3}>3 años</option>
                            <option key={4} value={4}>4 años</option>
                            <option key={5} value={5}>5 años</option>
                        </select>
                    </div>
                </section>

            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    options: state.scholarship.options
});

export default connect(
    mapStateToProps, null )(OptionScholarship);
