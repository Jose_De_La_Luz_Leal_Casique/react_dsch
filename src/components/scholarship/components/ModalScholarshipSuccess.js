import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from 'prop-types';

import {set_result_scholarship} from '../../../actions/scholarship/set_result_scholarship';


class ModalScholarshipSuccess extends Component {

    static propTypes = {
        register: PropType.object,
        set_result_scholarship: PropType.func.isRequired
    }

    onSetResult = (option, result) => {
        this.props.set_result_scholarship(this.props.register.id, option, result);
    }

    onSendPromotion = (e) => {
        let op = 0;
        if (e.target.name === 'YES-PRO' ? op = 1: op = 2);
        this.onSetResult(1, op);
    }

    onSendStimulus = (e) => {
        let op = 0;
        if (e.target.name === 'YES-S' ? op = 1: op = 2);
        this.onSetResult(2, op);
    }

    onSendTrajectory = (e) => {
        let op = 0;
        if (e.target.name === 'YES-T' ? op = 1: op = 2);
        this.onSetResult(3, op);
    }

    onSendPermanence = (e) => {
        let op = 0;
        if (e.target.name === 'YES-PER' ? op = 1: op = 2);
        this.onSetResult(4, op);
    }

    render() {


        let questionPromotion = null;
        let questionStimulus = null;
        let questionTrajectory = null;
        let questionPermanence = null;

        let result_yes = (
            <div className="alert alert-success result-optional">
                <b>SI SE ENTREGO BECA</b>
            </div>
        );
        let result_not = (
            <div className="alert alert-danger result-optional">
                <b>NO SE ENTREGÓ BECA</b>
            </div>
        );

        if (this.props.register.promotion === true) {
            questionPromotion = (
                <div className="question-optional">
                    <label className="text-question">
                        ¿Se obtuvo la promoción?
                    </label>
                    {this.props.register.result_promotion === 0 ?
                    <section className="col-8 m-auto section-button">
                        <button className="btn btn-success col-4" name='YES-PRO'
                                onClick={this.onSendPromotion}>
                            Si
                        </button>
                        <a className="col-2"></a>
                        <button className="btn btn-warning col-4" name='NOT-PRO'
                                onClick={this.onSendPromotion}>
                            No
                        </button>
                    </section>
                    : this.props.register.result_promotion === 1 ?
                    result_yes: result_not}
                </div>
            );
        }

        if (this.props.register.stimulus === true) {
            questionStimulus = (
                <div className="question-optional">
                    <label className="text-question">
                        ¿Se obtuvo el Estímulo a la  Docencia e Investigación?
                    </label>
                    {this.props.register.result_stimulus === 0 ?
                    <section className="col-8 m-auto section-button">
                        <button className="btn btn-success col-4" name='YES-S'
                                onClick={this.onSendStimulus}>
                            Si
                        </button>
                        <a className="col-2"></a>
                        <button className="btn btn-warning col-4" name='NOT-S'
                                onClick={this.onSendStimulus}>
                            No
                        </button>
                    </section>
                    : this.props.register.result_stimulus === 1 ?
                    result_yes: result_not}
                </div>
            );
        }

        if (this.props.register.permanence === true) {
            questionPermanence = (
                <div className="question-optional">
                    <label className="text-question">
                        ¿Se obtuvo el Apoyo a la Permanencia por {this.props.register.years} Años?
                    </label>
                    {this.props.register.result_permanence === 0 ?
                    <section className="col-8 m-auto section-button">
                        <button className="btn btn-success col-4" name='YES-PER'
                                onClick={this.onSendPermanence}>
                            Si
                        </button>
                        <a className="col-2"></a>
                        <button className="btn btn-warning col-4" name='NOT-PER'
                                onClick={this.onSendPermanence}>
                            No
                        </button>
                    </section>
                    : this.props.register.result_permanence === 1 ?
                    result_yes: result_not}
                </div>
            );
        }

        if (this.props.register.trajectory === true) {
            questionTrajectory = (
                <div className="question-optional">
                    <label className="text-question">
                        ¿Se obtuvo el Estímulo a la Trayectoria Académica?
                    </label>
                    {this.props.register.result_trajectory === 0 ?
                    <section className="col-8 m-auto section-button">
                        <button className="btn btn-success col-4" name='YES-T'
                                onClick={this.onSendTrajectory}>
                            Si
                        </button>
                        <a className="col-2"></a>
                        <button className="btn btn-warning col-4" name='NOT-T'
                                onClick={this.onSendTrajectory}>
                            No
                        </button>
                    </section>
                    : this.props.register.result_trajectory === 1 ?
                    result_yes: result_not}
                </div>
            );
        }

        return (
            <div className="modal fade md-modal" id="SuccessModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="py-1 m-auto col-11" id="exampleModalLabel">
                                <strong>Resultados de Solicitud</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {questionPromotion}
                            {questionPermanence}
                            {questionTrajectory}
                            {questionStimulus}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register
});


export default connect(
    mapStateToProps, {set_result_scholarship})(ModalScholarshipSuccess);