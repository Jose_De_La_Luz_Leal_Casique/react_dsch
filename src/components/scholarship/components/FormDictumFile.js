import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";
import {ConfigDateSave} from "../../FormatDate";

import { register_dictum } from '../../../actions/scholarship/register_dictum';


class FormDictumFile extends Component{

    state = {
        file_dictum: null,
        number_dictum: '',
        date_get_request: '',
        edit: false
    }

    static propTypes = {
        register: PropType.object,
        register_dictum: PropType.func.isRequired
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFileDictum = (e) => {
        this.setState({file_dictum: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_dictum', this.state.number_dictum);
        if (this.state.date_get_request !== '') {
            data.append('date_get_request', ConfigDateSave(this.state.date_get_request));
        }
        data.append('file_dictum', this.state.file_dictum, this.state.file_dictum.name);
        this.props.register_dictum(data, this.props.register.id);

        this.setState({ edit: false });
    }

    onEditFormFile = (e) => {
        this.setState({
            edit: this.state.edit === false ? true: false
        })
    }

    render() {

        const cancelButton = (
            <button className="btn btn-danger m-auto col-4" onClick={this.onEditFormFile}>
                Cancelar
            </button>
        );

        const editButton = (
            <button className="btn btn-primary btn-edit-files col-3 m-auto" onClick={this.onEditFormFile}>
                Editar
            </button>
        );

        const addButton = (
            <button className="btn btn-warning btn-edit-files col-3 m-auto">
                Agregar
            </button>
        );

        const formDictumRequired = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Dictamen</label>
                    <input type="text" className="form-control" name="number_dictum" required
                           onChange={this.onChange} value={this.state.number_dictum}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Dictamen</label>
                    <input type="file" className="form-control" name="file_dictum" required
                           onChange={this.onLoadFileDictum} value={this.file_dictum}
                           accept="application/pdf"/>
                </div>
                <div className="form-group">
                    <label>Fecha de Respuesta</label>
                    <input type="date" className="form-control" name="date_get_request" required
                           onChange={this.onChange} value={this.state.date_get_request}/>
                </div>
                <div className="form-group text-center ">
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        const formDictum = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Dictamen</label>
                    <input type="text" className="form-control" name="number_dictum"
                           onChange={this.onChange} value={this.state.number_dictum}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Dictamen</label>
                    <input type="file" className="form-control" name="file_dictum" required
                           onChange={this.onLoadFileDictum} value={this.file_dictum}
                           accept="application/pdf"/>
                </div>
                <div className="form-group">
                    <label>Fecha de Respuesta</label>
                    <input type="date" className="form-control" name="date_get_request"
                           onChange={this.onChange} value={this.state.date_get_request}/>
                </div>
                <div className="form-group text-center ">
                    {this.state.edit === true ? cancelButton: null}
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        if (this.props.register.file_dictum === null){
            return (
                <div className="card col-6 card-form-file">
                    {formDictumRequired}
                </div>
            );
        } else  if (this.state.edit === true){
            return (
                <div className="card col-6 card-form-file">
                    {formDictum}
                </div>
            );
        } else{
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body col-12">
                        <h6 className="text-center">
                            <strong>DICTAMEN </strong>{this.props.register.number_dictum}
                        </h6>

                        <section className="filter-section-dsch-multi-form">
                            <section className="alert alert-dark text-center col-5 m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.register.file_dictum}`}>
                                    Ver Archivo
                                </a>
                            </section>
                            {this.props.register.finalized === false ? editButton : null}
                            {/*this.props.register.finalized === false ? addButton : null*/}
                        </section>
                    </section>
                </div>
            );
        }
    }

}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, { register_dictum })(FormDictumFile);