import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FormatDate } from '../../FormatDate';

export class ListScholarshipYear extends Component {

    static propTypes = {
        scholarship: PropTypes.object,
    };

    render() {
        if (this.props.scholarship.load === true & this.props.scholarship.scholarship.length > 0) {
            return (
                <Fragment>
                    <table className="table table-striped mt-3">
                        <thead className="text-center">
                            <tr>
                                <th width={200}>Nombre del solicitante</th>
                                <th width={210}>Fecha de solicitud</th>
                                <th width={150}>No. de solicitud</th>
                                <th width={210}>Fecha de envío</th>
                                <th width={230}>Publicación de dictamen</th>
                                <th width={100}>Año</th>
                                <th width={100}>Estado</th>
                                <th>Acciones </th>
                            </tr>
                        </thead>
                        <tbody className="text-center table-scroll-presupuestal">
                            {this.props.scholarship.scholarship.map((scholar) => (
                            <tr key={scholar.id}>
                                <td width={200}>{scholar.full_name}</td>
                                <td width={210}>
                                    {
                                        FormatDate(scholar.date_request).substring(
                                            0, FormatDate(scholar.date_get_request).length - 8)
                                    }
                                </td>
                                <td width={150}>{scholar.number_request}</td>
                                <td width={210}>
                                    {
                                        FormatDate(scholar.date_send_request).substring(
                                            0, FormatDate(scholar.date_get_request).length - 8)
                                    }
                                </td>
                                <td width={220}>
                                    {scholar.date_get_request != null ?
                                        FormatDate(scholar.date_get_request).substring(
                                            0, FormatDate(scholar.date_get_request).length - 8): 'Pendiente'}
                                </td>
                                <td width={100}>{scholar.year}</td>
                                <td width={100}>
                                    {scholar.finalized === false ?
                                        'Pendiente':
                                        'Finalizado'}
                                </td>
                                <td width={80}>
                                    <Link to={{
                                        pathname: '/scholarship-edit',
                                        state: {id: scholar.id}
                                    }} >Ver</Link>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </Fragment>
            );
        } else{
            return <h3 className="mt-5 text-center">NO HAY COINICIDENCIAS</h3>
        }
    }
}

const mapStateToProps = (state) => ({
    scholarship: state.scholarship
});

export default connect(
    mapStateToProps, {}  )(ListScholarshipYear);