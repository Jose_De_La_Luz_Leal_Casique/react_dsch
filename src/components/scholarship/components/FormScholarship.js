import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import OptionScholarship from "./OptionScholarship";
import { register_scholarship } from '../../../actions/scholarship/register_scholarship';
import { ConfigDateSave } from '../../FormatDate';
import { createMessage } from '../../../actions/messages';


export class FormScholarship extends Component {

    state = {
        user: '',
        number_request: '',
        date_request: '',
        date_send_request: '',
        file_request: null,
        promotion: false,
        stimulus: false,
        trajectory: false,
        permanence: false,
        years: 1
    }

    static propTypes = {
        register_scholarship: PropTypes.func.isRequired,
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFile = (e) => {
        this.setState({file_request: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onChecked = (e) => {
        if (e.target.name === 'promotion') {
            this.setState({promotion: e.target.checked});

        } else if (e.target.name === 'stimulus') {
            this.setState({stimulus: e.target.checked});

        } else if (e.target.name === 'trajectory'){
            this.setState({trajectory: e.target.checked});

        } else if (e.target.name === 'permanence') {
            this.setState({permanence: e.target.checked});
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        if (this.state.permanence === true | this.state.stimulus === true |
            this.state.trajectory === true | this.state.promotion === true){
            const data = new FormData();
            data.append('user', this.state.user);
            //data.append('request', this.state.request);
            data.append('promotion', this.state.promotion);
            data.append('stimulus', this.state.stimulus);
            data.append('trajectory', this.state.trajectory);
            data.append('permanence', this.state.permanence);
            data.append('years', this.state.years);
            data.append('date_request', ConfigDateSave(this.state.date_request));
            data.append('number_request', this.state.number_request);
            data.append('date_send_request', ConfigDateSave(this.state.date_send_request));
            data.append('file_request', this.state.file_request, this.state.file_request.name);
            this.props.register_scholarship(data);
        } else{
            this.props.createMessage({
                PasswordNotMatch: 'Selecionar Recurso a Solicitar en Beca' });
        }
    }

    render() {
        return (
            <form className="mt-3" onSubmit={this.onSubmit} encType="multipart/form-data">
                <section className="filter-section-dsch-multi-form mt-3 section-dsch-form">
                    <div className="form-group col-3">
                        <label>Número Económico</label>
                        <input type="number" className="form-control" name="user" required
                               onChange={this.onChange} value={this.state.user}/>
                    </div>
                    <div className="form-group col-3">
                        <label>Fecha de Solicitud</label>
                        <input type="date" className="form-control" name="date_request" required
                               onChange={this.onChange} value={this.state.date_request}/>
                    </div>

                    <div className="form-group col-3">
                        <label>Número de Solicitud</label>
                        <input type="text" className="form-control" name="number_request" required
                               onChange={this.onChange} value={this.state.number_request}/>
                    </div>

                    <div className="form-group col-3">
                        <label>Fecha de Envío a Dictaminadora</label>
                        <input type="date" className="form-control" name="date_send_request" required
                               onChange={this.onChange} value={this.state.date_send_request}/>
                    </div>

                </section>

                <section className="filter-section-dsch-multi-form mt-5 section-dsch-form">
                    <div className="form-group col-7">
                        <label>¿Qué Solicita?</label>
                        {/*<OptionRequest
                            select_option={this.state.request}
                            onChange={this.onChange}
                            number_characters={this.state.number_characters}
                        />*/}
                        <OptionScholarship
                            stimulus={this.state.stimulus}
                            permanence={this.state.permanence}
                            promotion={this.state.promotion}
                            trajectory={this.state.trajectory}
                            years={parseInt(this.state.years)}
                            onChange={this.onChange}
                            onChecked={this.onChecked}
                        />
                    </div>

                    <div className="form-group col-4">
                        <label>Archivo de Solicitud</label>
                        <input type="file" className="form-control" name="file_request" required
                               onChange={this.onLoadFile} value={this.file_request}
                               accept="application/pdf"/>
                    </div>

                    <div className="form-group col-1">
                        <label className="m-3"></label>
                        <button className="btn btn-file-academic m-auto">Guardar</button>
                    </div>
                </section>
            </form>
        );
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { register_scholarship, createMessage })(FormScholarship);

