import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalViewRequestBook from "./components/ModalViewRequestBook";

import { get_list_request_editorial_personal} from '../../actions/generals/get_list_request_editorial_personal';
import {get_request_editorial} from '../../actions/editorial/get_request_editorial';
import {FormatDate} from '../FormatDate';

export class ViewListRequestBook extends Component {

    state = {
        temp_request: 0
    }

    static propTypes = {
        get_list_request_editorial_personal: PropTypes.func.isRequired,
        get_request_editorial: PropTypes.func.isRequired,
        editorial: PropTypes.object.isRequired
    };

    onSendRequest = (pk) => {
       if (pk !== this.state.temp_request){
           this.props.get_request_editorial(pk);
           this.setState({temp_request: pk});
       }
    }

    componentDidMount() {
        if (this.props.editorial.request === null){
            this.props.get_list_request_editorial_personal();
        }
    }

    render() {
        if (this.props.editorial.load === true){
            if (this.props.editorial.request.length > 0){

                return (
                    <section className="modal-footer">
                        {this.props.editorial.request.map((book) => (
                            <div className="col-4 m-auto p-1" key={book.id}>
                                <section className="card card-body alert-warning">
                                    <div className="col-12">
                                        <h6>
                                            <strong>Título de Obra: </strong>
                                            {
                                                book.title.length > 28 ?
                                                    book.title.substr(0, 28)+' ...'
                                                    :
                                                    book.title
                                            }
                                        </h6>
                                        <h6>
                                            <strong>Autor/Colaborador: </strong>
                                            {book.name}
                                        </h6>
                                        <h6>
                                            <strong>Última Actualización: </strong>
                                            {FormatDate(book.date_updated)}
                                        </h6>
                                        <div className="col-12">
                                            <button className="btn-dsch float-right"
                                                    data-toggle="modal"
                                                    data-target="#ModalRequestBook"
                                                    onClick={this.onSendRequest.bind(this, book.id)}>
                                                {
                                                    book.finalized === true ?
                                                        'FINALIZADO'
                                                        :
                                                        'EN PROCESO'
                                                } - VER DETALLE
                                            </button>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        ))}
                        <ModalViewRequestBook />
                    </section>
                )

            } else {
                return (
                    <div className="text-center mt-5 section-dsch-form pb-5">
                        <h4 className="pt-5 pb-5">
                            <strong>
                                NO HAY SOLICITUDES REALIZADAS
                            </strong>
                        </h4>
                    </div>
                );
            }

        } else {
            return (
                <div className="text-center mt-5">
                    <h4 className="pt-5 pb-5">Loading...</h4>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    editorial: state.editorialpersonal
});


export default connect(
    mapStateToProps, {
        get_list_request_editorial_personal, get_request_editorial})(ViewListRequestBook);