import React, { Component } from "react";
import PropTypes from "prop-types";
import jsPDF from "jspdf";
import html2PDF from 'jspdf-html2canvas';

import "./css/style.css";

import FilePDF from "./FilePDF";


export class ModalViewReport extends Component {

    static propTypes = {
        obj: PropTypes.object,
    }

    onCreateFilePDF = () => {
        let doc = new jsPDF({ orientation: 'p', unit: 'px', format: 'letter' });
        doc.setProperties({
            title: 'Reporte de Investigación',
        });
        let viewFile = document.getElementById('formatReport');
        let totalPagesExp = '{total_pages_count_string}'

        doc.html(viewFile, {
            callback: function (doc) {

                let str = 'Página ' + doc.internal.getNumberOfPages()
                if (typeof doc.putTotalPages === 'function') str = str + ' de ' + totalPagesExp;
                doc.setFontSize(10)
                let pageSize = doc.internal.pageSize
                let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, pageSize.getWidth()-100, pageHeight - 20)
                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp)
                }
                 window.open(doc.output('bloburl'));
                },
            x: 10,
            y: 10,
        });
    }

    onCreateFilePDF1 = () => {
        const pages = document.getElementById('formatReport');
        // eslint-disable-next-line no-undef
        html2PDF(pages , {
            jsPDF: {
                format: 'letter',
                callback: function (pdf) {
                    let totalPagesExp = '{total_pages_count_string}'
                    let str = 'Página ' + pdf.internal.getNumberOfPages()
                    if (typeof pdf.putTotalPages === 'function') str = str + ' de ' + totalPagesExp;
                    pdf.setFontSize(10)
                    let pageSize = pdf.internal.pageSize
                    let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    pdf.text(str, pageSize.getWidth()-60, pageHeight - 10)
                    if (typeof pdf.putTotalPages === 'function') {
                        pdf.putTotalPages(totalPagesExp)
                    }
                }
            },
            imageType: 'image/jpeg',
            output: './pdf/generate.pdf'
        });
    }

    onCreateFilePDF2 = () => {
        let pdf = new jsPDF({ orientation: 'p', unit: 'px', format: 'letter' });
        pdf.html(document.getElementById('formatReport'), {
            html2canvas: {
                windowWidth: 200,
                windowHeight: 500,
            },
            callback: function (pdf) {
                pdf.save("test.pdf");
            },
        });
    }

    onCreateFilePDF3 = () => {
        let year = '2021';
        let author = 'JOse';

        let totalPagesExp = '{total_pages_count_string}'
        let pdf = new jsPDF('p', 'px', 'letter');

        let str = 'Página ' + pdf.internal.getNumberOfPages()
        if (typeof pdf.putTotalPages === 'function') str = str + ' de ' + totalPagesExp;
        pdf.setFontSize(10)
        let pageSize = pdf.internal.pageSize
        let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        pdf.text(str, pageSize.getWidth()-60, pageHeight - 10)
        if (typeof pdf.putTotalPages === 'function') {
            pdf.putTotalPages(totalPagesExp)
        }
        pdf.save(`Reporte-${author}-${year}`);
    }

    render() {
        return (
            <div className="modal fade" id="viewReportInvestigation" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div className="modal-dialog" role="document">
                   <div className="modal-content">
                       {
                           this.props.obj !== null
                               ?
                               this.props.obj.title !== ""
                                   ?
                                   <div className="content-pdf">
                                       <button onClick={this.onCreateFilePDF}>Guardar</button>
                                       <br></br>
                                       <FilePDF />
                                   </div>
                                   :
                                   <div
                                       className="text-center mt-3 col-11 mx-auto alert alert-danger pt-3">
                                       <h4>
                                           <strong>
                                               NO HAY VISTA PREVIA.
                                               PARA TENER UNA VISUALIZACIÓN ES NECESARIO UN TÍTULO
                                           </strong>
                                       </h4>
                                   </div>
                               :
                               <div className="text-center mt-3 col-11 mx-auto alert alert-danger pt-3">
                                   <h4><strong>NO HAY VISTA PREVIA</strong></h4>
                               </div>
                       }
                   </div>
               </div>
            </div>
        );
    }

}

export default ModalViewReport;