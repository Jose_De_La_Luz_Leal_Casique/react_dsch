import React from "react";
import logo from "../../../media/logo2.png";
import "./css/style.css"

export class FilePDF extends React.Component {

    render() {
        return (
            <div className="mx-1 bg-file-pdf " id="formatReport">

                <div className="text-center">
                    <p className="text-title-pdf">
                        UAM-CUAJIMALPA
                    </p>
                    <p className="text-title-pdf">
                        <strong>DIVISIÓN DE CIENCIAS SOCIALES Y HUMANIDADES</strong>
                    </p>
                    <p className="text-title-pdf">
                        <strong>DEPARTAMENTO DE ESTUDIOS INSTITUCIONALES </strong>
                    </p>
                    <p className="text-title-pdf">
                        <strong>PROYECTO DE INVESTIGACIÓN</strong>
                    </p>
                </div>

                <div className="text-center col-12 mx-auto px-2 mt-4">
                    <div className="filter-section-dsch-multi-form">
                        <div className="text-right text-subtitle-option-pdf doc-section-width">
                            <p><strong>I.</strong></p>
                        </div>
                        <div className="text-justify text-subtitle-option-pdf doc-div-width">
                            <p><strong>Hoja Resumen</strong></p>
                        </div>
                    </div>

                    <div className="div-doc">
                        <p className="text-right text-subtitle-option-pdf doc-section-width">
                            Título del proyecto
                        </p>
                        <p className="text-justify text-subtitle-option-pdf doc-div-width">
                            Prácticas comerciales internacionales.
                            Política económica y desarrollo económico de México
                        </p>
                    </div>

                    <div className="filter-section-dsch-multi-form  div-doc">
                        <div className="text-right text-subtitle-option-pdf doc-section-width">
                            <p>Participantes</p>
                        </div>
                        <div className="text-justify text-subtitle-option-pdf doc-div-width">
                            <p>
                                Dra. Aureola Quiñonez Salcido (Responsable del Proyecto)
                                Departamento de Estudios Institucionales (DESIN) UAM-C
                                Dra. Esther Morales Franco
                                DESIN UAM-C
                                Dra. Mariana Moranchel
                                DESIN UAM-C
                                Dra. Sandra Carrillo Andrés
                                Departamento de
                                Administración UAM-A
                                Cuerpo Académico: Derecho, Administración e Instituciones
                            </p>
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form div-doc">
                        <div className="text-right text-subtitle-option-pdf doc-section-width">
                            <p>Pregunta de investigación</p>
                        </div>
                        <div className="text-justify text-subtitle-option-pdf doc-div-width">
                            <p>
                                ¿Cómo ha incidido la política económica comercial en el desarrollo
                                económico de México?
                            </p>
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form div-doc">
                        <div className="text-right text-subtitle-option-pdf doc-section-width">
                            <p>Hipótesis</p>
                        </div>
                        <div className="text-justify text-subtitle-option-pdf doc-div-width">
                            <p>
                                La política comercial de competencia económica
                                internacional ha permitido el mayor ingreso de productos al
                                mercado mexicano, beneficiando a los consumidores, sin
                                embargo, las prácticas desleales de comercio internacional,
                                han tenido un impacto negativo en diversos sectores
                                económicos o ramas de la industria nacional, como el
                                agrícola, la industria siderúrgica y química, propiciando
                                reducciones en los niveles de producción y de empleo, por lo
                                que no han favorecido el desarrollo económico del país.
                            </p>
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form div-doc">
                        <div className="text-right text-subtitle-option-pdf doc-section-width">
                            <p>Objetivo</p>
                        </div>
                        <div className="text-justify text-subtitle-option-pdf doc-div-width">
                            <p>
                                Evaluar la incidencia de la política comercial de México con
                                el resto del mundo en el desarrollo económico del país,
                                mediante el análisis del comportamiento de las prácticas
                                comerciales y la competencia económica de México, las
                                nuevas reglas establecidas en el T-MEC y el análisis de
                                casos presentados en las instituciones nacionales e
                                internacionales, para analizar el comportamiento de diversos
                                sectores económicos.
                            </p>
                        </div>
                    </div>

                    <div className="div-doc">
                        <label className="text-right text-subtitle-option-pdf doc-section-width">
                            Duración
                        </label>
                        <label className="text-justify text-subtitle-option-pdf doc-div-width">
                            3 años (2021-2024)
                        </label>
                    </div>
                </div>
            </div>
        )
    }

}

export default FilePDF;
