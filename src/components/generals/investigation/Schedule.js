import React, { Component } from "react";
import PropTypes from "prop-types";


export class Schedule extends Component {

    state = {
        range_date: [],
        active: false,

    }

    static propTypes = {
        onChangeView: PropTypes.func.isRequired,
        onChecked: PropTypes.func.isRequired,
        onAddProductoForSchedule: PropTypes.func.isRequired,
        onDeleteProductForSchedule: PropTypes.func.isRequired,
        initial: PropTypes.string,
        fin: PropTypes.string,
        duration: PropTypes.number,
        date_initial: PropTypes.number,
        date_final: PropTypes.number,
        name_schedule: PropTypes.string,
        list_schedule: PropTypes.array,
    }

    componentDidMount() {
        console.log(this.props.date_initial, this.props.date_final);
        if (this.props.initial !== '' & this.props.fin !== '') {

            for (let i = this.props.date_initial; i <= this.props.date_final; i++) {
                this.state.range_date.push(i);
            }
            this.setState({active: true});
        }
    }

    render () {
        return (
            <section className="mt-2 p-1">
                <div className="col-12 filter-section-dsch-multi-form">
                    <div className="col-8">
                        <h4 className="text-right mb-4 col-12 pr-5">
                            <strong>Cronograma de trabajo</strong>
                        </h4>
                    </div>

                    <div className="col-4 mx-auto">
                        <button className="btn-dsch float-right"
                                onClick={this.props.onChangeView.bind(this, 1)}>
                            Regresar Pre-Solicitud
                        </button>
                    </div>
                </div>

                {
                    this.state.active
                        ?
                        <div className="pr-1 mt-2 col-12 card py-4 px-4">
                            <section className="filter-section-dsch-multi-form">
                                <div className="col-4 mx-auto">
                                    <form className="card alert-warning py-3 px-2"
                                          onSubmit={this.props.onAddProductoForSchedule}>
                                        <div className="group-dsch">
                                            <h6><strong>Producto:</strong></h6>
                                            <textarea name="name_schedule"  rows={3}
                                                      value={this.props.name_schedule}
                                                      className="form-control col-12 mx-auto"
                                                      onChange={this.props.onChange} required
                                            />
                                        </div>

                                        <section className="filter-section-dsch-multi-form">
                                            <div className="group-dsch col-6 mx-auto">
                                                <h6><strong>Inicio de actividad:</strong></h6>
                                                <select name="date_initial"
                                                        onChange={this.props.onChange}
                                                        className="form-control col-12 mx-auto"
                                                        value={this.props.date_initial}>
                                                    {
                                                        this.state.range_date.map(
                                                            (year, index) => (
                                                                <option key={index}>
                                                                    {year}
                                                                </option>
                                                            )
                                                        )
                                                    }
                                                </select>
                                            </div>

                                            <div className="group-dsch col-6 mx-auto">
                                                <h6><strong>Fin de actividad:</strong></h6>
                                                <select name="date_final"
                                                        onChange={this.props.onChange}
                                                        className="form-control col-12 mx-auto"
                                                        value={this.props.date_final}>
                                                    {
                                                        this.state.range_date.map(
                                                            (year, index) => (
                                                                <option key={index}>
                                                                    {year}
                                                                </option>
                                                            )
                                                        )
                                                    }
                                                </select>
                                            </div>
                                        </section>
                                        <section className="col-12">
                                            <button className="btn btn-warning float-right">
                                                Agregar
                                            </button>
                                        </section>
                                    </form>
                                </div>
                                <section className="col-8 card">
                                    <div className="col-12 mx-auto pt-3">
                                        <table className="table table-bordered ">
                                            <thead className="alert alert-warning">
                                                <tr>
                                                    <th width={300} className="text-center">
                                                        Producto
                                                    </th>

                                                    {
                                                        this.state.range_date.map((year, index) => (
                                                            <th key={index} className="text-center"
                                                                width=
                                                                    {
                                                                        375 / this.state.range_date.length
                                                                    }>
                                                                {year}
                                                            </th>

                                                        ))
                                                    }
                                                    <th width={60}></th>
                                                </tr>
                                            </thead>
                                            <tbody className="table-scroll-schedule"

                                            >
                                            {
                                                this.props.list_schedule.map((obj, index) => (
                                                    <tr>
                                                        <td width={300} className="text-center">
                                                            {obj.name}
                                                        </td>
                                                        {
                                                            this.state.range_date.map(
                                                                (year, index) => (
                                                                <td width=
                                                                    {
                                                                        375 / this.state.range_date.length
                                                                    } className={
                                                                        year >= obj.initial
                                                                        &
                                                                        year <= obj.final
                                                                        ? 'alert-dark' : 'bg-white'
                                                                }
                                                                >
                                                                </td>
                                                            ))
                                                        }
                                                        <td width={60}>
                                                            <button className="btn btn-danger"
                                                                    onClick={
                                                                        this.props.onDeleteProductForSchedule.bind(
                                                                            this, obj.name
                                                                        )
                                                                    }>
                                                                X
                                                            </button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </section>
                        </div>
                        :
                        <div className="col-11 mx-auto text-center alert alert-danger py-5 mt-5">
                            <h4>
                                <strong>
                                    Para habilitar cronograma es necesario definir fechas de inicio y
                                    finalización de proyecto.
                                </strong>
                            </h4>
                        </div>
                }

            </section>
        )
    }
}

export default Schedule;
