import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {FormatDate} from '../../FormatDate';
import {SERVER} from '../../../actions/server';


export class ViewRequestScholarship extends Component {

    static propTypes = {
        view_request: PropTypes.string.isRequired,
        scholarship: PropTypes.array,
    }

    render() {

        let content_modal = null;
        let file_additional = null;
        let view = null;

        if (this.props.scholarship !== null){
            this.props.scholarship.forEach(element => {
                if (element.number_request === this.props.view_request){
                    view = element;
                }
            });

            if (view !== null){
                file_additional = (
                    <section>
                        <div className="text-center">
                            <h5><strong>Archivo Adicionales</strong></h5>
                        </div>
                    </section>
                );

                content_modal = (
                    <section>
                        <div className="filter-section-dsch-multi-form">
                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Solicitud </strong><br></br>
                                    {view.file_dictum !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                           href={`${SERVER}${view.file_request}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>
                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Dictamen</strong><br></br>
                                    {view.file_dictum !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                           href={`${SERVER}${view.file_dictum}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>
                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Acuse de Dictamen</strong><br></br>
                                    {view.file_receipt_dictum !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                            href={`${SERVER}${view.file_receipt_dictum}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>
                        </div>

                        <div className="filter-section-dsch-multi-form">
                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Recurso Interpuesto</strong><br></br>
                                    {view.file_notification !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                            href={`${SERVER}${view.file_notification}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>

                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Notificación de Dictamen</strong><br></br>
                                    {view.file_resolution !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                           href={`${SERVER}${view.file_resolution}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }

                                </label>
                            </div>

                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Dictamen BIS</strong><br></br>
                                    {view.file_bis !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                            href={`${SERVER}${view.file_bis}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>
                        </div>

                        <div className="filter-section-dsch-multi-form">
                            <div className="card view-div-personal col-4">
                                <label className="col-12 m-auto view-label-personal text-center">
                                    <strong>Acuse de Dictamen BIS</strong><br></br>
                                    {view.file_receipt_bis !== null ?
                                        <a target="_blank" rel="noopener noreferrer"
                                            href={`${SERVER}${view.file_receipt_bis}`}>
                                            Clic Aquí
                                        </a>
                                        :
                                        <div className="alert-danger py-2 px-3 radius-view">
                                            Archivo No Agregado
                                        </div>
                                    }
                                </label>
                            </div>
                        </div>
                    </section>
                );

                return (
                    <section className="filter-section-dsch-multi-form">

                        <div className="col-6">
                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Número de Solicitud: </strong>{view.number_request}
                                </label>
                            </div>

                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Número de Económico: </strong>{view.user}
                                </label>
                            </div>

                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Fecha de Solicitud: </strong>
                                    {FormatDate(view.date_request)}
                                </label>
                            </div>

                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Fecha de Envió: </strong>
                                    {FormatDate(view.date_send_request)}
                                </label>
                            </div>

                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Fecha de Publicacion de Dictamen: </strong>
                                    {FormatDate(view.date_get_request)}
                                </label>
                            </div>

                            <div className="card view-div-personal ">
                                <label className="col-11 m-auto view-label-personal">
                                    <strong>Estado de Solicitud: </strong>
                                    {
                                        view.finalized === true ? 'Finalizada' : 'Pendiente'
                                    }
                                </label>
                            </div>
                        </div>

                        <section className="col-6">
                            {view.finalized === true ?
                                <section className="card card-body">
                                    <h5>Resultados de Solicitud</h5>
                                    <ul>
                                        {view.promotion === true ?
                                            <li>
                                                Promoción
                                                {view.result_promotion === 1 ?
                                                    <div className="text-success">Aprobada</div>
                                                    : view.result_promotion === 2 ?
                                                        <div className="text-danger">No Aprovado</div>
                                                        :
                                                        <div className="text-info">Pendiente</div>
                                                }
                                            </li> : null
                                        }
                                        <br></br>
                                        {view.stimulus === true ?
                                            <li>
                                                Estímulo a la Docencia e Investigación
                                                {view.result_stimulus === 1 ?
                                                    <div className="text-success">Aprobada</div>
                                                    : view.result_stimulus === 2 ?
                                                        <div className="text-danger">No Aprovado</div>
                                                        :
                                                        <div className="text-info">Pendiente</div>
                                                }
                                            </li> : null
                                        }
                                        <br></br>
                                        {view.trajectory === true ?
                                            <li>
                                                Estímulo a la Trayectoria Académica
                                                {view.result_trajectory === 1 ?
                                                    <div className="text-success">Aprobada</div>
                                                    : view.result_trajectory === 2 ?
                                                        <div className="text-danger">No Aprovado</div>
                                                        :
                                                        <div className="text-info">Pendiente</div>
                                                }
                                            </li>
                                            : null
                                        }
                                        <br></br>
                                        {view.permanence === true ?
                                            <li>
                                                Apoyo a la Permanencia por {view.years} Años
                                                {view.result_permanence === 1 ?
                                                    <div className="text-success">Aprobada</div>
                                                    : view.result_permanence === 2 ?
                                                        <div className="text-danger">No Aprovado</div>
                                                        :
                                                        <div className="text-info">Pendiente</div>
                                                }
                                            </li>
                                            :
                                            null
                                        }
                                    </ul>
                                </section>
                                :
                                null
                            }

                            <br></br>
                            <button type="button" className="btn btn-primary float-right"
                                    data-toggle="modal" data-target="#ModalFilesRequest">
                                Ver Archivos
                            </button>
                        </section>


                        <div className="modal fade" id="ModalFilesRequest" tabIndex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog modal-lg" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="text-center">
                                            Archivos de Solicitud
                                        </h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        {content_modal}
                                        <hr></hr>
                                        {file_additional}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                );
            } else {
                return <h1 className="text-center">Opción No Disponible</h1>
            }
        } else{
            return <h1 className="text-center">Elija una Opción</h1>
        }
    }
}

const mapStateToProps = (state) => ({
    scholarship: state.history.scholarship,
});

export default connect(
    mapStateToProps,  null)(ViewRequestScholarship);
