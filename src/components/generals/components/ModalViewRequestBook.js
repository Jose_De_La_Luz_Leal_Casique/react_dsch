import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from 'prop-types';

import {FormatDate} from "../../FormatDate";
import {SERVER} from "../../../actions/server";


class ModalViewRequestBook extends Component {

    static propTypes = {
        permissions: PropType.array.isRequired,
        book: PropType.object,
    }

    render() {
        let content = null;
        let plagiarism = null;
        let meeting = null;
        let letter = null;
        let dictum = null;

        if (this.props.book !== null){
            if (this.props.book.plagiarism !== null){
                plagiarism = (
                    <div className="px-3 py-2">
                        <h6 className="text-dark">
                            <strong>Procentaje de Plagio: </strong>
                            {this.props.book.plagiarism.percentage}%
                        </h6>
                        <h6 className="text-dark">
                            <strong>Archivo de Evidencia: </strong>
                            <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.book.plagiarism.file}`}>
                                    Ver Archivo
                                </a>
                        </h6>
                        <h6 className="text-dark">
                            <strong>Observaciones: </strong>
                            {this.props.book.plagiarism.observations}
                        </h6>
                    </div>
                );
            } else {
                plagiarism = (
                    <div className="my-3 text-center">
                        <strong>PENDIENTE</strong>
                    </div>
                );
            }

            if (this.props.book.meeting !== null){
                meeting = (
                    <div className="px-3 py-2">
                        <h6 className="text-dark">
                            <strong>Estado Actual: </strong>
                            {this.props.book.meeting.status === 0 ? 'PENDIENTE'
                                : this.props.book.meeting.status === 1 ? 'DICTAMEN'
                                    : this.props.book.meeting.status === 2 ? 'PREDICTAMEN':
                                        'RECHAZADO'
                            }
                        </h6>
                        <h6 className="text-dark">
                            <strong>Fecha de Reunión de Consejo Ed. </strong>
                            {FormatDate(this.props.book.meeting.date)}
                        </h6>
                        <h6 className="text-dark">
                            <strong>Observaciones: </strong>
                            {this.props.book.meeting.observations}
                        </h6>
                    </div>
                );
            } else {
                meeting = (
                    <div className="my-3 text-center">
                        <strong>PENDIENTE</strong>
                    </div>
                );
            }

            if (this.props.book.letter !== null){
                letter = (
                    <div className="px-3 py-2">
                        <h6 className="text-dark">
                            <strong>Fecha de Entrega de Resultados: </strong>
                            {FormatDate(this.props.book.letter.date)}
                        </h6>
                        <h6 className="text-dark">
                            <strong>Observaciones: </strong>
                            {this.props.book.letter.observations}
                        </h6>
                    </div>
                )
            } else {
                letter = (
                    <div className="my-3 text-center">
                        <strong>PENDIENTE</strong>
                    </div>
                );
            }

            if (this.props.book.dictum !== null){
                dictum = (
                    <div className="px-3 py-2">
                        <h6 className="text-dark">
                            <strong>Primer Evaluación: </strong>
                            {
                                this.props.book.dictum.evaluationOne !== null
                                    ?
                                    <a target="_blank" rel="noopener noreferrer"
                                       className="float-right pl-2"
                                       href={`${SERVER}${this.props.book.dictum.evaluationOne}`}>
                                        Ver Archivo
                                    </a>
                                    :
                                    'PENDIENTE'
                            }
                        </h6>

                        <h6 className="text-dark">
                            <strong>Segunda Evaluación: </strong>
                            {
                                this.props.book.dictum.evaluationTwo !== ""
                                    ?
                                    <a target="_blank" rel="noopener noreferrer"
                                       className="float-right pl-2"
                                       href={`${SERVER}${this.props.book.dictum.evaluationTwo}`}>
                                        Ver Archivo
                                    </a>
                                    :
                                    'PENDIENTE'
                            }
                        </h6>

                        <h6 className="text-dark">
                            <strong>Resultado: </strong>
                            {
                                this.props.book.dictum.result !== null
                                    ?
                                    this.props.book.dictum.result === 1
                                        ?
                                        'Publicable'
                                        :
                                    this.props.book.dictum.result === 2
                                        ?
                                        'Publicable condicionado a cambios y sugerencias del dictamen'
                                        :
                                    this.props.book.dictum.result === 3
                                        ?
                                        'No Publicable'
                                        :
                                    this.props.book.dictum.result === 4
                                        ?
                                        'Publicable con modificaciones sustantivas y sujeto a una nueva revisión'
                                        :
                                        null
                                    :
                                    'PENDIENTE'
                            }
                        </h6>

                    </div>
                )
            } else {
                dictum = (
                    <div className="my-3 text-center">
                        <strong>PENDIENTE</strong>
                    </div>
                );
            }


            content = (
                <div className="modal-content">
                    <div className="modal-header bg-dark text-white">
                        <h5 className="py-1 m-auto col-8 text-justify">
                            <strong>{this.props.book.title}</strong>
                        </h5>
                        <h5 className="col-1"></h5>
                        <h5 className="py-1 m-auto col-3">
                            <strong >{this.props.book.finalized === true ?
                                'FINALIZADO'
                                :
                                'EN PROCESO'}
                            </strong>
                        </h5>
                    </div>

                    <div className="modal-body">

                        <div className="col-12 filter-section-dsch-multi-form">
                            {
                                this.props.permissions.includes('authentication.admin_editorial')
                                |
                                this.props.permissions.includes('authentication.view_editorial_advice')
                                |
                                this.props.permissions.includes('authentication.admin_root')
                                    ?
                                    <section className="col-6 text-center">
                                        <h5 >
                                            <strong>Autor/Colaborador: </strong>
                                            {this.props.book.name}
                                        </h5>
                                        <h5 >
                                            <strong>Correo Electronico: </strong>
                                            {this.props.book.email}
                                        </h5>
                                    </section>
                                    :
                                    null
                                }

                            <section className="col-6 text-center">
                                <h5 >
                                    <strong>Última Actualización: </strong>
                                    {FormatDate(this.props.book.date_updated)}
                                </h5>
                                <h5 >
                                    <strong>Obeservaciones Generales: </strong>
                                    {this.props.book.observations === "" ?
                                        'SIN OBERVACIONES'
                                        :
                                        this.props.book.observations
                                    }
                                </h5>
                            </section>
                        </div>

                        <div className=" card-body col-12 filter-section-dsch-multi-form">
                            <section className="col-8 card p-3">
                                {
                                    this.props.permissions.includes('authentication.admin_editorial')
                                    |
                                    this.props.permissions.includes('authentication.view_editorial_advice')
                                    |
                                    this.props.permissions.includes('authentication.admin_root')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Recepción de Manuscrito: </strong>
                                            {FormatDate(this.props.book.date_created)}
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    this.props.permissions.includes('authentication.admin_editorial')
                                    |
                                    this.props.permissions.includes('authentication.view_editorial_advice')
                                    |
                                    this.props.permissions.includes('authentication.admin_root')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Entrega a Editorial: </strong>
                                            {this.props.book.delivery_date_to_editorial === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.delivery_date_to_editorial)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    !this.props.permissions.includes('authentication.view_editorial_advice')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Disponible para Correción de Estilo: </strong>
                                            {this.props.book.correction_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.correction_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    !this.props.permissions.includes('authentication.view_editorial_advice')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Revisión de Primeras: </strong>
                                            {this.props.book.first_review_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.first_review_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    !this.props.permissions.includes('authentication.view_editorial_advice')
                                        ?
                                        <h5>
                                            <strong>Fecha de Revisión de Segundas:  </strong>
                                            {this.props.book.second_revision_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.second_revision_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    !this.props.permissions.includes('authentication.view_editorial_advice')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Revisión de Terceras:  </strong>
                                            {this.props.book.third_review_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.third_review_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    !this.props.permissions.includes('authentication.view_editorial_advice')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Revisión de Galeras:  </strong>
                                            {this.props.book.galley_review_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.galley_review_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    this.props.permissions.includes('authentication.admin_root')
                                        |
                                    this.props.permissions.includes('authentication.admin_editorial')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Entrega Solicitud ISBN:  </strong>
                                            {this.props.book.isbn_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.isbn_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    this.props.permissions.includes('authentication.admin_root')
                                        |
                                    this.props.permissions.includes('authentication.admin_editorial')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Entrega:  </strong>
                                            {this.props.book.delivery_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.delivery_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                                {
                                    this.props.permissions.includes('authentication.admin_root')
                                        |
                                    this.props.permissions.includes('authentication.admin_editorial')
                                        ?
                                        <h5 >
                                            <strong>Fecha de Oficio de Solicitud de Precio:  </strong>
                                            {this.props.book.price_date === null ?
                                                'PENDIENTE'
                                                :
                                                FormatDate(this.props.book.price_date)
                                            }
                                        </h5>
                                        :
                                        null
                                }

                            </section>

                            <section className="col-4 card p-3">
                                {
                                    this.props.permissions.includes('authentication.admin_editorial')
                                        |
                                    this.props.permissions.includes('authentication.view_editorial_advice')
                                        |
                                    this.props.permissions.includes('authentication.admin_root')
                                        ?
                                        <div className="alert alert-warning text-center">
                                            <section className="nav-link option-editorial"
                                                     data-toggle="dropdown" aria-haspopup="true"
                                                     aria-expanded="false">
                                                Resultados de Plagio
                                            </section>
                                            <div className="dropdown-menu sub-menu-editorial">
                                                {plagiarism}
                                            </div>
                                        </div>
                                        :
                                        null
                                }

                                {
                                    this.props.permissions.includes('authentication.admin_editorial')
                                        |
                                    this.props.permissions.includes('authentication.view_editorial_advice')
                                        |
                                    this.props.permissions.includes('authentication.admin_root')
                                        ?
                                        <div className="alert alert-warning text-center">
                                            <section className="nav-link option-editorial"
                                                     data-toggle="dropdown" aria-haspopup="true"
                                                     aria-expanded="false">
                                                Reunón de Consejo editorial
                                            </section>
                                            <div className="dropdown-menu sub-menu-editorial">
                                                {meeting}
                                            </div>
                                        </div>
                                        :
                                        null
                                }

                                <div className="alert alert-warning text-center">
                                    <section className="nav-link option-editorial"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        Resultados de Consejo editorial
                                    </section>
                                    <div className="dropdown-menu sub-menu-editorial">
                                        {letter}
                                    </div>
                                </div>

                                <div className="alert alert-warning text-center">
                                    <section className="nav-link option-editorial"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        Dictámenes
                                    </section>
                                    <div className="dropdown-menu sub-menu-editorial">
                                        {dictum}
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                </div>
            );

        } else {
            content = (
                <div className="text-center mt-5 modal-content">
                    <h4 className="pt-5 pb-5">
                        <strong>
                            Loading...
                        </strong>
                    </h4>
                </div>
            )
        }

        return (
            <div className="modal fade md-modal" id="ModalRequestBook" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl" role="document">
                    {content}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    book: state.editorialpersonal.book,
    permissions: state.auth.permissions
});


export default connect(mapStateToProps, null)(ModalViewRequestBook);