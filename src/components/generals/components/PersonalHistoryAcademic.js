import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pdf from 'react-to-pdf';
import PropTypes from "prop-types";
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";

const date = new Date();
const date_now = date.getDate().toString()+'-'+(date.getMonth()+1).toString()+'-'+date.getFullYear();

const tableHistoryFilter = React.createRef();
const options = { orientation: 'landscape', unit: 'in',};


class PersonalHistoryAcademic extends Component {

    state = {
        pages: null
    }

    static propTypes = {
        history: PropTypes.array,
        viewYear: PropTypes.number.isRequired,
    };

    generatorPDF = () => {
        let year = this.props.viewYear;
        let author = this.props.history[0].full_name;

        let totalPagesExp = '{total_pages_count_string}'
        let pdf = new jsPDF({
             orientation: 'l',
             unit: 'mm',
             format: 'letter',
        });
        pdf.autoTable({
            html: '#reportTotals',
            styles: { fontSize: 12, haling: 'center' },
            didDrawPage: function (data) {
                // titulo
                pdf.setFontSize(18)
                pdf.setTextColor(40)
                pdf.text(`${author} - Historial Académico del ${year}`,
                    data.settings.margin.left, 10)

                // pie de página
                let str = 'Página ' + pdf.internal.getNumberOfPages()
                if (typeof pdf.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }
                pdf.setFontSize(10)
                let pageSize = pdf.internal.pageSize
                let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                pdf.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 20 },
        })
        if (typeof pdf.putTotalPages === 'function') {
            pdf.putTotalPages(totalPagesExp)
        }
        pdf.save(`Reporte-${author}-${year}`);
    }

    render() {

        let viewHistory = null;

        if (this.props.history !== null){
            viewHistory = (
                <section ref={tableHistoryFilter}>
                    <h5 className="py-1 m-auto text-center">
                        <strong>
                            {
                                `${this.props.history[0].full_name} - Historial Académico del ${
                                this.props.viewYear}`}
                        </strong>
                    </h5>
                    <table className="table table-striped mt-3" id="reportTotals">
                        <thead className="text-center">
                            <tr>
                                <th width={150}>No. Económico</th>
                                <th width={130}>P. De Estudio</th>
                                <th width={100}>Depto.</th>
                                <th width={100}>Trimestre</th>
                                <th >Curso</th>
                                <th width={80}>Cursos</th>
                                <th width={80}>Horas</th>
                                <th width={80}>Almnos</th>
                                <th width={90}>En Aula</th>

                            </tr>
                        </thead>
                        <tbody className="text-center">
                            {this.props.history.map((scholar) => (
                                <tr key={scholar.id}>
                                    <td>{scholar.user}</td>
                                    <td>{scholar.study_plan}</td>
                                    <td>{scholar.departament}</td>
                                    <td>{scholar.trimester }</td>

                                    <td>{scholar.course }</td>
                                    <td>{scholar.number_course }</td>
                                    <td>{scholar.number_hour }</td>
                                    <td>{scholar.number_student }</td>
                                    <td>
                                        { scholar.classroom === true ? 'SI' : 'NO' }
                                    </td>
                                </tr>
                            ))}
                            <tr key={0}>
                                <td><strong>Totales</strong></td>
                                <td>{}</td>
                                <td>{}</td>
                                <td>{}</td>
                                <td>{}</td>

                                <td>
                                    {this.props.history.reduce((sum, value) => (sum + value.number_course), 0)}
                                </td>

                                <td>
                                    {this.props.history.reduce((sum, value) => (sum + value.number_hour), 0)}
                                </td>

                                <td>
                                    {this.props.history.reduce(
                                            (sum, value) => (sum + value.number_student), 0)
                                    }
                                </td>

                                <td>{}</td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            );
        } else {
            <h3>NO HAY COINCIDENCIAS</h3>
        }

        return (
            <div className="modal fade " id="HistoryAcademic" tabIndex="-1"
                 role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            {
                                /*
                            <Pdf targetRef={tableHistoryFilter}
                                     filename={this.props.history !== null ?
                                         `Reporte-${
                                             this.props.history[0].full_name.split(" ").join("")}-${
                                             this.props.history[0].year}` : 'Reporte'
                                     }
                                     options={options} x={.2} y={.3} scale={.98}>
                                {({toPdf}) =>
                                    this.props.history !== null ?
                                        <button onClick={toPdf} className="btn-dsch float-right">
                                            Guardar en PDF
                                        </button>
                                        : null
                                }
                                </Pdf>
                            <button type="button" className="close col-1" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                                 */
                            }
                            {
                                this.props.history !== null
                                    ?
                                    <button className="btn-dsch float-right"
                                            onClick={this.generatorPDF}>
                                        Guardar PDF
                                    </button>
                                    :
                                    <section className="py-4 mx-auto">
                                        <h4><strong>NO HAY COINCIDENCIAS</strong></h4>
                                    </section>
                            }
                        </div>
                        <div className="modal-body">
                            {viewHistory}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    history: state.history.history
});


export default connect(
    mapStateToProps, null)(PersonalHistoryAcademic);
