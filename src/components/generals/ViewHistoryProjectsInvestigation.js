import React, { Component } from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { get_list_projects_investigation } from "../../actions/generals/get_list_projects_investigation";

import ListProjectInvestigation from "./investigation/ListProjectInvestigation";

export class ViewHistoryProjectsInvestigation extends Component {

    static propTypes = {
        investigation: PropTypes.object.isRequired,
        get_list_projects_investigation: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_list_projects_investigation();
    }


    render () {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form">
                    <div className="col-8 mx-auto mt-3 text-right">
                        <h4><strong>Mis Proyectos de Investigación</strong></h4>
                    </div>
                    <div className="col-4 mx-auto mt-3">
                        <Link to={
                            { pathname: '/pre-project', state:{pk: 0, edit: false} }
                        } >
                            <button className="btn-dsch float-right">
                                Nuevo Proyecto
                            </button>
                        </Link>
                    </div>
                </section>

                <ListProjectInvestigation option={1} />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    investigation: state.investigation
});

export default connect(
    mapStateToProps,  {get_list_projects_investigation} )(ViewHistoryProjectsInvestigation);