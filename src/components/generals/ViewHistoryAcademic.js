import React, { Component } from 'react';
import Pdf from 'react-to-pdf';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { get_history_academic_for_user } from '../../actions/generals/get_history_academic_for_user';

const ref = React.createRef();
const options = {
    orientation: 'landscape',
    unit: 'in',
};

export class ViewHistoryAcademic extends Component{

    static propTypes = {
        get_history_academic_for_user: PropTypes.func.isRequired,
        history: PropTypes.object
    };

    componentDidMount() {
        this.props.get_history_academic_for_user()
    }


    render() {
        if (this.props.history.load === true){
            return (
                <>
                    <Pdf targetRef={ref} filename={this.props.history.registers[0].user}
                                options={options} x={.2} y={.3} scale={.85}>
                        {({ toPdf }) => <button onClick={toPdf}
                                                className="btn-dsch float-right margin-btn-pdf">
                            Guardar en PDF
                        </button>}
                    </Pdf>
                    <div>
                        <table className="table table-striped" ref={ref}>
                            <thead className="text-center">
                                <tr>
                                    <th>Plan de Estudio</th>
                                    <th>Departamento</th>
                                    <th>Año</th>
                                    <th>Trimestre</th>
                                    <th>Cursos en Aula</th>
                                    <th>Horas en Aula</th>
                                    <th>Estudiantes en Aula</th>
                                    <th>Cursos Fuera de Aula</th>
                                    <th>Horas Fuera de Aula</th>
                                    <th>Estudiantes Fuera de Aula</th>
                                </tr>
                            </thead>
                            <tbody className="text-center table-pdf">
                            {this.props.history.registers.map((register) => (
                                <tr key={register.id}>
                                    <td>{register.study_plan}</td>
                                    <td>{register.departament}</td>
                                    <td>{register.year}</td>
                                    <td>{register.trimester}</td>
                                    <td>{register.course_in_classroom}</td>
                                    <td>{register.hour_in_classroom}</td>
                                    <td>{register.student_in_classroom}</td>
                                    <td>{register.course_out_classroom}</td>
                                    <td>{register.hour_out_classroom}</td>
                                    <td>{register.student_out_classroom}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                    </>
            )
        } else{
            return <div className="mt-5 text-center"><h3>Loading...</h3></div>
        }
    }
}
const mapStateToProps = (state) => ({
    history: state.history
});

export default connect(
    mapStateToProps, { get_history_academic_for_user })(ViewHistoryAcademic);
