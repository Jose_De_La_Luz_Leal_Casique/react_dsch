import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import EditSabbatical from "./forms/EditSabbatical";
import { FormatDate } from "../../../FormatDate";
import {SERVER} from "../../../../actions/server";


class DetailSabbatical extends Component {

    static propTypes = {
        sabbatical: PropTypes.object,
        auth: PropTypes.object.isRequired,
    }

    get_full_name = (obj) => {
        return obj.name + ' ' + obj.paternal_surname + ' ' + obj.maternal_surname
    }

    is_owner = (user) => {
        if (this.props.auth.isAuthenticated) {
            let user_session = this.props.auth.user;

            if (user_session.user === user.user){
                return true;

            }else{
                return false;
            }

        }
        return false;
    }

    render() {

        if (this.props.sabbatical !== null){
            return (
                <section className="py-3">
                    <div className="filter-section-dsch-multi-form">
                        <div className="col-9 filter-section-dsch-multi-form ">
                            <h5>
                                <strong>Solicitante: </strong>
                            </h5>
                            <div className="col-11 mx-auto">
                                <h5>
                                    {this.get_full_name(this.props.sabbatical.user)}
                                </h5>
                            </div>
                        </div>
                        <div className="col-3">
                            {
                                this.props.sabbatical.status === 'Periodo sabático en revisión'
                                    &&
                                this.is_owner(this.props.sabbatical.user)
                                    ?
                                    <button className="btn btn-dark float-right py-1"
                                            data-toggle="modal" data-target="#edit-Sabbatical">
                                        Editar Solicitud
                                    </button>
                                    : null

                            }
                        </div>
                    </div>
                    <div className="filter-section-dsch-multi-form pl-4">
                        <h6>
                            <strong>Estado de solicitud: </strong>
                            {this.props.sabbatical.status}
                        </h6>
                    </div>

                    <hr className="pt-0 mt-1"></hr>

                    <section className="card py-2 mb-0">
                        <div className="filter-section-dsch-multi-form">

                            <div className="col-8 py-2">
                                <div className="filter-section-dsch-multi-form alert alert-dark py-1 px-1 my-0 mb-2">
                                    <h6 className="col-5 pt-1">
                                        <strong>
                                            Número económico:
                                        </strong>
                                    </h6>
                                    <div className="col-7 card mx-auto px-0 pt-1 text-center">
                                        {this.props.sabbatical.user.user}
                                    </div>
                                </div>

                                <div className="filter-section-dsch-multi-form alert alert-dark py-1 px-1 my-0 mb-2">
                                    <h6 className="col-5 pt-1">
                                        <strong>
                                            Departamento:
                                        </strong>
                                    </h6>
                                    <div className="col-7 card mx-auto px-0 pt-1 text-center">
                                        {this.props.sabbatical.user.area}
                                    </div>
                                </div>

                                <div className="filter-section-dsch-multi-form alert alert-dark py-1 px-1 my-0 mb-2">
                                    <h6 className="col-5 pt-1">
                                        <strong>
                                            Fecha de inicio:
                                        </strong>
                                    </h6>
                                    <div className="col-7 card mx-auto px-0 pt-1 text-center">
                                        {FormatDate(this.props.sabbatical.date_initial)}
                                    </div>
                                </div>

                                <div className="filter-section-dsch-multi-form alert alert-dark py-1 px-1 my-0 mb-2">
                                    <h6 className="col-5 pt-1">
                                        <strong>
                                            Fecha de fin:
                                        </strong>
                                    </h6>
                                    <div className="col-7 card mx-auto px-0 pt-1 text-center">
                                        {FormatDate(this.props.sabbatical.date_final)}
                                    </div>
                                </div>

                                <div className="filter-section-dsch-multi-form alert alert-dark py-1 px-1 my-0 mb-2">
                                    <h6 className="col-5 pt-1">
                                        <strong>
                                            Duración:
                                        </strong>
                                    </h6>
                                    <div className="col-7 card mx-auto px-0 pt-1 text-center">
                                        {
                                            this.props.sabbatical.months > 1
                                                ?
                                                this.props.sabbatical.months  + ' meses'
                                                :
                                                'Un mes'
                                        }
                                    </div>
                                </div>
                            </div>

                            <div className="col-4 py-2 my-auto text-center">
                                <h5 className="mb-3">
                                    <strong>
                                        Archivos de solicitud
                                    </strong>
                                </h5>
                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch btn-block"
                                   href={`${SERVER}${this.props.sabbatical.file}`}>
                                    <strong>Solicitud de sabático</strong>
                                </a>

                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch btn-block"
                                   href={`${SERVER}${this.props.sabbatical.program}`}>
                                    <strong>Programa de actividades</strong>
                                </a>

                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch btn-block"
                                   href={`${SERVER}${this.props.sabbatical.constancy}`}>
                                    <strong>Constancia de antigüedad</strong>
                                </a>
                            </div>

                        </div>
                    </section>
                    <EditSabbatical sabbatical={this.props.sabbatical}/>

                </section>
            )
        }

        return (
            <section className="py-5 text-center">
                <h4>
                    <strong>
                        loading...
                    </strong>
                </h4>
            </section>
        )

    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical,
    auth: state.auth,
});

export default connect( mapStateToProps, null  )(DetailSabbatical);