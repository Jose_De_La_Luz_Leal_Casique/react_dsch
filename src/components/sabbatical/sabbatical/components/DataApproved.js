import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import DataAdditional from "./components/DataAdditional";


export class DataApproved extends Component {

    static propTypes = {
        onRedirectToListRequest: PropTypes.func.isRequired,
        sabbatical: PropTypes.object,
    }

    render() {

        if (this.props.sabbatical !== null){
            return (
                <section className="py-3">
                    <div className="filter-section-dsch-multi-form mb-2">
                        <h5>
                            <strong>+ {this.props.sabbatical.request.status}</strong>
                        </h5>
                    </div>
                    <section className="card">
                        {
                            this.props.sabbatical.request !== null
                                ?
                                <DataAdditional
                                    request={this.props.sabbatical.request}
                                    onRedirectToListRequest={this.props.onRedirectToListRequest}
                                />
                                :
                                null

                        }
                    </section>
                </section>
            )
        }
        return (
            <section className="py-5 text-center">
                <h4>
                    <strong>
                        loading...
                    </strong>
                </h4>
            </section>
        )
    }

}


const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical
});

export default connect( mapStateToProps, null  )(DataApproved);