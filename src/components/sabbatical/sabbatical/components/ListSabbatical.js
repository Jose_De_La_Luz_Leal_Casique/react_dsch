import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { FormatDate } from "../../../FormatDate";
import {Link} from "react-router-dom";


class ListSabbatical extends Component {

    static propTypes = {
        list: PropTypes.array,
        option: PropTypes.number.isRequired,
    }

    get_full_name = (obj) => {
        return obj.name + ' ' + obj.paternal_surname + ' ' + obj.maternal_surname
    }

    render() {
        if (this.props.list.length > 0){
            return (
                <table className="table table-striped mt-2">
                    <thead>
                    <tr>
                        <th width={250} className="text-center">Profesor Solicitante</th>
                        <th width={240} className="text-center">Fecha de Inicio</th>
                        <th width={240} className="text-center">Fecha de Fin</th>
                        <th width={150} className="text-center">Duración</th>
                        <th width={230} className="text-center">Estado de Solicitud</th>
                        <th width={113} className="text-center"></th>
                    </tr>
                    </thead>
                    <tbody className="table-scroll-a">
                    {
                        this.props.list.map((obj, index) => (
                            <tr key={index} className="alert alert-primary">
                                <td className="text-center" width={250}>
                                    { this.get_full_name(obj.user) }
                                </td>
                                <td className="text-center" width={240}>
                                    { FormatDate(obj.date_initial) }
                                </td>
                                <td className="text-center" width={240}>
                                    { FormatDate(obj.date_final) }
                                </td>
                                <td className="text-center" width={150}>
                                    { obj.months > 1 ? obj.months + ' meses' : 'Un mes' }
                                </td>
                                <td className="text-center" width={230}>
                                    { obj.status }
                                </td>
                                <td className="text-center" width={100}>
                                    <Link to={{
                                        pathname: '/view-sabbatical',
                                        state: {
                                            pk: obj.id,
                                            option: this.props.option,
                                        }
                                    }}>
                                        <button className="btn-dsch px-2">
                                            Detalle
                                        </button>
                                    </Link>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
            )
        }

        return (
            <section className="col-10 mx-auto alert alert-danger text-center py-3 mt-5">
                <h3>
                    <strong>
                        SIN SOLICITUDES A MOSTRAR
                    </strong>
                </h3>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    list: state.sabbatical.list
});

export default connect( mapStateToProps, null )(ListSabbatical);