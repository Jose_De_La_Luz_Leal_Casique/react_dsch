import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {SERVER} from "../../../../actions/server";
import { FormatDate } from "../../../FormatDate";
import { send_report_sabbatical } from "../../../../actions/sabbatical/send_report_sabbatical";
import { send_office_reception } from "../../../../actions/sabbatical/send_office_reception";


export class ReportSabbatical extends  Component{

    state = {
        file: null,
        zip: null,
        office: null,
    }

    static propTypes = {
        user: PropTypes.object,
        sabbatical: PropTypes.object,
        permissions: PropTypes.array,
        report: PropTypes.object,
        send_report_sabbatical: PropTypes.func.isRequired,
        send_office_reception: PropTypes.func.isRequired,
        onRedirectToListSabbatical: PropTypes.func.isRequired,
        onRedirectToListApproved: PropTypes.func.isRequired,
    }

    onChangeFile = (e) => this.setState({[e.target.name]: e.target.files[0]});

    onCheckAuthor = () => {
        return this.props.user.user === this.props.sabbatical.user.user;
    }

    onCheckCommission = () => {
        return this.props.permissions.includes("investigation.admin_evaluation_commission");
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData();
        data.append('file', this.state.file, this.state.file.name);
        data.append('zip', this.state.zip, this.state.zip.name);

        this.props.send_report_sabbatical(this.props.sabbatical.id, data);
        setTimeout(this.props.onRedirectToListSabbatical, 500);
    }

    onSendOfficeReception = (e) => {
        e.preventDefault()
        let data = new FormData();
        data.append('office', this.state.office, this.state.office.name);
        this.props.send_office_reception(this.props.sabbatical.id, data);
        setTimeout(this.props.onRedirectToListApproved, 500);
    }

    render() {
        if (this.props.report !== null){
            return (
                <section className="my-3 text-center">
                    <div className="col-12 py-2 my-auto text-center">
                        <div className="bg-dark py-2 mb-0 alert">
                            <h5 className="text-white">
                                <strong>Informe y probatorios de periodo sabático</strong>
                            </h5>
                            <div className="col-10 bg-dark py-0 alert mx-auto text-white">
                                <h6>
                                    <strong>Fecha de registro: </strong>
                                    {FormatDate(this.props.report.created)}
                                </h6>
                            </div>
                        </div>

                        <div className="filter-section-dsch-multi-form py-3 mt-0">
                            <div className="col-5 alert-dark py-2 alert mx-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch btn-block my-2"
                                   href={`${SERVER}${this.props.report.file}`}>
                                    <strong> Ver informe de sabático</strong>
                                </a>
                            </div>

                            <div className="col-5 alert-dark py-2 alert mx-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   className="btn btn-dsch btn-block my-2"
                                   href={`${SERVER}${this.props.report.zip}`}>
                                    <strong> Ver probatorios de sabático</strong>
                                </a>
                            </div>
                        </div>
                        {
                            this.props.report.office
                                ?
                                <div className="col-6 alert-dark py-2 alert mx-auto">
                                    <a target="_blank" rel="noopener noreferrer"
                                       className="btn btn-dsch btn-block my-2"
                                       href={`${SERVER}${this.props.report.office.office}`}>
                                        <strong> Ver oficio de recepción de informe</strong>
                                    </a>
                                </div>
                                :
                                this.onCheckCommission() ?
                                    <section className="filter-section-dsch-multi-form pt-2 pb-4">
                                        <form className="col-6 mx-auto alert alert-dark"
                                              onSubmit={this.onSendOfficeReception}>
                                            <h6 className="text-center pl-1">
                                                <strong>
                                                    Informe de periodo sabático
                                                </strong>
                                            </h6>
                                            <input type="file" className="form-control" name="office"
                                               value={this.office} onChange={this.onChangeFile}
                                               accept="application/pdf" required/>
                                            <section className="col-12 mb-2 mt-3">
                                                <button className="btn btn-dark">
                                                    Guardar informe
                                                </button>
                                            </section>
                                        </form>
                                    </section>
                                    :
                                    <section className="alert alert-danger my-4 py-4 text-center">
                                        <h5><strong>Informe derecepción pendiente</strong></h5>
                                    </section>
                        }
                    </div>
                </section>
            )
        } else{

            if (this.onCheckAuthor()){
                return (
                    <form className="text-center my-3" onSubmit={this.onSubmit}>
                        <h5>
                            <strong>Registro de Informe y probatorios de periodo sabático</strong>
                        </h5>

                        <hr></hr>

                        <section className="filter-section-dsch-multi-form pt-2 pb-4">
                            <div className="col-6">
                                <h6 className="text-left pl-1">
                                    <strong>
                                        Informe de periodo sabático
                                    </strong>
                                </h6>
                                <input type="file" className="form-control" name="file"
                                       value={this.file} onChange={this.onChangeFile}
                                       accept="application/pdf" required/>
                            </div>

                            <div className="col-6">
                                <h6 className="text-left pl-1">
                                    <strong>
                                        Probatorios de periodo sabático
                                    </strong>
                                </h6>
                                <input type="file" className="form-control" name="zip"
                                       value={this.zip} onChange={this.onChangeFile}
                                       required/>
                            </div>
                        </section>
                        <section className="col-12 mb-2">
                            <button className="btn btn-dark">Guardar informe</button>
                        </section>
                    </form>
                );
            }
            return (
                <section className="alert alert-danger my-4 py-4 text-center">
                    <h5><strong>INFORME Y PROBATORIOS PENDIENTES</strong></h5>
                </section>
            );
        }

    }

}


const mapStateToProps = (state) => ({
    user: state.auth.user,
    permissions: state.auth.permissions,
    sabbatical: state.sabbatical.sabbatical
});

export default connect(
    mapStateToProps, { send_report_sabbatical, send_office_reception })(ReportSabbatical
);