import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import SelectMonth from "./SelectMonth";
import { FormatDate, ConfigDateSave } from "../../../FormatDate";
import {
    create_request_sabbatical
} from "../../../../actions/sabbatical/create_request_sabbatical";


class FormSabbatical extends Component {

    state = {
        date_initial: '',
        date_final: '',
        months: 1,
        file: null,
        program: null,
        constancy: null,
    }

     static propTypes = {
        user: PropTypes.object,
        create_request_sabbatical: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({[e.target.name]: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.props.user.user);
        data.append('date_initial', ConfigDateSave(this.state.date_initial));
        data.append('date_final', ConfigDateSave(this.state.date_final));
        data.append('months', this.state.months);
        data.append('file', this.state.file, this.state.file.name);
        data.append('program', this.state.program, this.state.program.name);
        data.append('constancy', this.state.constancy, this.state.constancy.name);
        this.props.create_request_sabbatical(data);
        this.setState({ date_initial: '', date_final: '', months: 1 });
        document.getElementById('form-sabbatical').reset();
    }

    render() {
        return (
            <form className="col-12 mx-auto text-center" id="form-sabbatical"
                  onSubmit={this.onSubmit}>
                <h5 className="py-3">
                    <strong>
                        Formulario para solicitar un periodo sabático
                    </strong>
                </h5>
                <hr></hr>
                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Fecha de inicio de periodo sabático
                            </strong>
                        </h6>
                        <input type="date" className="form-control" name="date_initial" required
                               value={this.state.date_initial} onChange={this.onChange}/>
                        <small className="text-danger">
                            {
                                this.state.date_initial !== ''
                                    ?
                                    <strong>
                                        Fecha seleccionada: {
                                            FormatDate(ConfigDateSave(this.state.date_initial)) }
                                    </strong>
                                    :
                                    null
                            }
                        </small>
                    </div>

                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Fecha de fin de periodo sabático
                            </strong>
                        </h6>
                        <input type="date" className="form-control" name="date_final" required
                               value={this.state.date_final} onChange={this.onChange}/>
                        <small className="text-danger">
                            {
                                this.state.date_final !== ''
                                    ?
                                    <strong>
                                        Fecha seleccionada: {
                                            FormatDate(ConfigDateSave(this.state.date_final)) }
                                    </strong>
                                    :
                                    null
                            }
                        </small>
                    </div>

                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Duración de periodo sabático
                            </strong>
                        </h6>
                        <SelectMonth
                            select_month={parseInt(this.state.months)}
                            onChange={this.onChange}
                        />
                    </div>
                </section>

                <hr></hr>
                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Solicitud de periodo sabático
                            </strong>
                        </h6>
                        <input type="file" className="form-control" name="file"
                               value={this.file} onChange={this.onChangeFile}
                               accept="application/pdf" required/>
                    </div>

                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Programa de actividades
                            </strong>
                        </h6>
                        <input type="file" className="form-control" name="program"
                               value={this.program} onChange={this.onChangeFile}
                               accept="application/pdf" required/>
                    </div>

                    <div className="col-4">
                        <h6 className="text-left pl-1">
                            <strong>
                                Constancia de antigüedad
                            </strong>
                        </h6>
                        <input type="file" className="form-control" name="constancy"
                               value={this.constancy} onChange={this.onChangeFile}
                               accept="application/pdf" required/>
                    </div>
                </section>

                <section className="pb-4">
                    <div className="col-2 float-right mb-4">
                        <button className="btn-dsch px-2 ">Enviar Solicitud</button>
                    </div>
                </section>

            </form>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps, { create_request_sabbatical })(FormSabbatical);
