import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { FormatDate } from "../../../../FormatDate";


export class DetailInformationAdditional extends Component {

    static propTypes = {
        additional: PropTypes.object,
        date_final: PropTypes.string,
        note: PropTypes.string
    }


    render() {
        return (
            <div className="modal fade " tabIndex="-1" role="dialog" id="view-modification-data"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLabel">
                                <strong>Detalle de información de consejo divisional</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body py-2">
                            {
                                this.props.additional !== null
                                    ?
                                    <section>
                                        {
                                            this.props.date_final != null
                                                ?
                                                <div className="alert alert-danger">
                                                    <h6><strong>{this.props.note}.</strong></h6>
                                                    <h6>
                                                        Nueva fecha de finalización: {FormatDate(this.props.date_final)}
                                                    </h6>
                                                </div>
                                                :
                                                null
                                        }

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>No. de Registro de periodo sabático: </strong>
                                                {this.props.additional.register}
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>No. De Sesión en la que fue aprobado: </strong>
                                                {this.props.additional.session}
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>No. De Acuerdo con el que se aprobó: </strong>
                                                {this.props.additional.agreement}
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Fecha de aprobación: </strong>
                                                {FormatDate(this.props.additional.created)}
                                            </h6>
                                        </div>
                                    </section>
                                    :
                                    <section className="py-3">
                                        <h5><strong>loading...</strong></h5>
                                    </section>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null )(DetailInformationAdditional);