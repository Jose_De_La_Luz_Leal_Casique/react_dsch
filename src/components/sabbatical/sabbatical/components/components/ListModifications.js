import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {SERVER} from "../../../../../actions/server";

import ViewModification from "./ViewModification";
import DetailInformationAdditional from "./DetailInformationAdditional";
import FormDataCommissionToModification from "../forms/FormDataCommissionToModification";


export class ListModifications extends Component {

    state = {
        obj: null,
        pk_request: 0,
        additional: null,
        date_final: "",
        notes: "",
    }

    static propTypes = {
        request: PropTypes.array.isRequired,
        permissions: PropTypes.array.isRequired,
    }

    onChangeObjToView = (obj) => {
        this.setState({obj: obj});
    }

    onChangeAdditionalDataToView = (additional, date, notes) => {
        this.setState({
            additional: additional, date_final: date, notes: notes
        });
    }

    onChangeTuPkObj = (pk) => {
        this.setState({pk_request: pk})
    }

    render() {
        return (
            <section className="text-center mt-3">
                <section className="bg-light">
                    {
                        this.props.request.length > 0
                            ?
                            <table className="table table-striped mt-1">
                                <thead>
                                    <tr>
                                        <th width="250"> Tipo de Modificación </th>
                                        <th width="250"> Modificación de solicitud </th>
                                        <th width="150"> Agregó Carta </th>
                                        <th width="150"> Acciones </th>
                                    </tr>
                                </thead>
                                <tbody className="table-scroll-modifications-sabbatical">
                                {
                                    this.props.request.map((obj, index) => (
                                        <tr className="alert-primary" key={index}>
                                            <td width="250">{obj.status}</td>
                                            <td width="250">
                                                {
                                                    obj.file
                                                        ?
                                                        <a target="_blank"
                                                           rel="noopener noreferrer"
                                                           className="btn btn-dark"
                                                           href={
                                                               `${SERVER}${obj.file}`}>
                                                            <strong>Ver solicitud</strong>
                                                        </a>
                                                        :
                                                        'Sin modificación'
                                                }
                                            </td>
                                            <td width="150">
                                                {
                                                    obj.letter
                                                        ?
                                                        <a target="_blank"
                                                           rel="noopener noreferrer"
                                                           className="btn btn-dark"
                                                           href={
                                                               `${SERVER}${obj.letter}`}>
                                                            <strong>Ver carta</strong>
                                                        </a>
                                                        :
                                                        'Sin modificación'
                                                }
                                            </td>
                                            <td width="150">
                                                <div className="btn-group dropleft ">
                                                    <button type="button"
                                                            className="btn btn-dark dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        opciones
                                                    </button>
                                                    <div className="dropdown-menu">
                                                        <a className="dropdown-item"
                                                           style={{cursor: 'pointer'}}
                                                           data-toggle="modal"
                                                           data-target="#view-modification"
                                                           onClick={
                                                               this.onChangeObjToView.bind(
                                                                   this, obj)}>
                                                            Ver detalle de modificación
                                                        </a>
                                                        {
                                                            obj.additional === null
                                                                ?
                                                                this.props.permissions.includes(
                                                                    'investigation.admin_evaluation_commission'
                                                                )
                                                                    ?
                                                                        <a className="dropdown-item"
                                                                           style={{cursor: 'pointer'}}
                                                                           data-toggle="modal"
                                                                           data-target="#modal-data-commission"
                                                                            onClick={
                                                                                this.onChangeTuPkObj.bind(
                                                                                    this, obj.id)}>
                                                                            Agregar información de aceptación
                                                                        </a>
                                                                    :
                                                                    null
                                                                :
                                                                <a className="dropdown-item"
                                                                   style={{cursor: 'pointer'}}
                                                                   data-toggle="modal"
                                                                   data-target="#view-modification-data"
                                                                   onClick={
                                                                       this.onChangeAdditionalDataToView.bind(
                                                                           this,
                                                                           obj.additional,
                                                                           obj.date_final,
                                                                           obj.note
                                                                       )
                                                                   }>
                                                                    Ver información de aceptación
                                                                </a>
                                                        }

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                            :
                            <section className="my-4 py-4 alert alert-danger">
                                <h5>
                                    <strong>
                                        Sin modificaciones
                                    </strong>
                                </h5>
                            </section>
                    }
                </section>
                <ViewModification modification={this.state.obj}/>
                <DetailInformationAdditional
                    additional={this.state.additional}
                    date_final={this.state.date_final}
                    note={this.state.notes}
                />
                <FormDataCommissionToModification pk={this.state.pk_request}/>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    request: state.sabbatical.request,
    permissions: state.auth.permissions,
});

export default connect(mapStateToProps, null )(ListModifications);