import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import FormDataAdditional from "./FormDataAdditional";
import OptionsProcess from "./OptionsProcess";
import {FormatDate} from "../../../../FormatDate";
import {SERVER} from "../../../../../actions/server";
import {
    send_file_approval_sabbatical
} from "../../../../../actions/sabbatical/send_file_approval_sabbatical";


export class DataAdditional extends Component {

    state = {
        approval: null,
    }

    static propTypes = {
        onRedirectToListRequest: PropTypes.func.isRequired,
        send_file_approval_sabbatical: PropTypes.func.isRequired,
        permissions: PropTypes.array.isRequired,
        sabbatical: PropTypes.object,
        request: PropTypes.object,
    }

    onChangeFile = (e) => this.setState({approval: e.target.files[0]});

    onCheckPermissions = () => {
        return this.props.permissions.includes('investigation.admin_evaluation_commission');
    }

    onSubmitFile = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('approval', this.state.approval, this.state.approval.name);
        this.props.send_file_approval_sabbatical(this.props.sabbatical.id, data);
        this.props.onRedirectToListRequest();
    }

    render() {

        if (this.props.request.additional !== null){
            return (
                <section className="card px-2 py-3 text-center">
                    <h5 className="mt-2">
                        <strong>
                            Información Proporcionada por Oficina Técnica de Consejo Divisional
                        </strong>
                    </h5>
                    <table className="table mt-3">
                        <thead className="thead-dark">
                        <tr>
                            <th width={200}>No. de registro de proyecto</th>
                            <th width={200}>No. de sesión en la que fue aprobado</th>
                            <th width={200}>No. de acuerdo con el que se aprobó</th>
                            <th width={180}>Fecha de registro de profesor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr className="alert-warning">
                            <td>
                                {this.props.sabbatical.request.additional.register}
                            </td>
                            <td>
                                {this.props.sabbatical.request.additional.session}
                            </td>
                            <td>
                                {this.props.sabbatical.request.additional.agreement}
                            </td>
                            <td>
                                {FormatDate(this.props.sabbatical.request.additional.created)}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <section className="mt-2 col-12 my-1">
                        {
                            this.props.sabbatical.approval === null
                                ?
                                this.props.permissions.includes(
                                    'investigation.admin_evaluation_commission')
                                    ?
                                    <form className="col-12 alert mb-0 alert-warning mx-auto"
                                          onSubmit={this.onSubmitFile} id="add-file-approval">
                                        <h5>
                                            <strong>
                                                Registro de oficio de aprobación de proyecto
                                            </strong>
                                        </h5>
                                        <section
                                            className="filter-section-dsch-multi-form col-12 my-2">
                                            <div className="col-8 mx-auto ">
                                                <input type="file" className="form-control"
                                                       name="approval" required
                                                       value={this.approval}
                                                       onChange={this.onChangeFile}
                                                       accept="application/pdf"
                                                />
                                            </div>
                                            <div className="col-4 mx-auto pt-1">
                                                <button className="btn btn-warning">
                                                    Guardar Oficio
                                                </button>
                                            </div>
                                            <div className="col-1 mx-auto "></div>
                                        </section>
                                    </form>
                                    :
                                    <section className="py-4 ">
                                        <strong>
                                            Oficio de aprobación pendiente
                                        </strong>
                                    </section>
                                :
                                <div className="col-12 bg-dark mb-4">
                                    <a href={`${SERVER}${this.props.sabbatical.approval}`}
                                       className="btn btn-dark float-right"
                                       target={"_blank"} rel="noopener noreferrer" >
                                        Ver oficio de aprobación
                                    </a>
                                </div>
                        }
                    </section>
                </section>
            )
        } else {

            if (this.onCheckPermissions()){
                return (
                    <section className="py-5 text-center">
                        {
                            this.props.sabbatical.status === 'Periodo sabático en revisión'
                                ?
                                <OptionsProcess
                                    pk={this.props.sabbatical.id}
                                    onRedirectToListRequest={this.props.onRedirectToListRequest}
                                />
                                :
                                <FormDataAdditional
                                    pk={this.props.request.id}
                                    onRedirectToListRequest={this.props.onRedirectToListRequest}
                                />
                        }
                    </section>
                )
            } else {
                return (
                    <section className="py-5 text-center">
                        {
                            this.props.sabbatical.status === 'Periodo sabático rechazado'
                                ?
                                <div className="alert alert-danger col-11 mx-auto">
                                    <h3>
                                        <strong>
                                            Periodo sabático rechazado
                                        </strong>
                                    </h3>
                                </div>
                                :
                                <h4>
                                    <strong>
                                        Pendiente de respuesta
                                    </strong>
                                </h4>
                        }
                    </section>
                )
            }

        }
    }

}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions,
    sabbatical: state.sabbatical.sabbatical
});

export default connect( mapStateToProps, { send_file_approval_sabbatical } )(DataAdditional);