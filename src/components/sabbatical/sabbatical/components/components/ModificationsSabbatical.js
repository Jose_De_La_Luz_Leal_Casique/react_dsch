import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import ListModifications from "./ListModifications";
import FormDateInitial from "../forms/FormDateInitial";
import FormFileProgram from "../forms/FormFileProgram";
import {
    get_list_modifications_sabbatical
} from "../../../../../actions/sabbatical/get_list_modifications_sabbatical";


export class ModificationsSabbatical extends Component {

    static propTypes = {
        get_list_modifications_sabbatical: PropTypes.func.isRequired,
        sabbatical: PropTypes.object,
        user: PropTypes.object,
    }

    componentDidMount() {
        this.props.get_list_modifications_sabbatical(this.props.sabbatical.id);
    }

    onCheckUser = () => {
        return this.props.sabbatical.user.user === this.props.user.user;
    }

    onCheckStateProcess = () => {
        return (
            this.props.sabbatical.status === 'En revisión por modificación'
                ||
            this.props.sabbatical.status === 'Periodo sabático aceptado'
        )
    }

    render() {
        return (
            <section className="py-3 text-center">
                <div className="filter-section-dsch-multi-form text-right">
                    <h5 className="col-9 mt-2">
                        <strong>Listado de modificaciones en la solicitud</strong>
                    </h5>

                    {
                        this.onCheckUser() && this.onCheckStateProcess()
                            ?
                            <div className="btn-group dropleft col-3 ml-auto">
                                <button type="button"
                                        className="btn btn-dark dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    Modificaciones
                                </button>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" style={{cursor: 'pointer'}}
                                       data-toggle="modal" data-target="#modal-change-initial">
                                        Cambiar fecha de inicio
                                    </a>
                                    <a className="dropdown-item" style={{cursor: 'pointer'}}
                                       data-toggle="modal" data-target="#modal-change-program">
                                        Cambiar programa de actividades
                                    </a>
                                </div>
                            </div>
                            :
                            null
                    }

                </div>

                <ListModifications />

                <FormDateInitial
                    get_list_modifications_sabbatical={
                        this.props.get_list_modifications_sabbatical
                    }
                    pk={this.props.sabbatical.id}
                />

                <FormFileProgram />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical,
    user: state.auth.user
});

export default connect(
    mapStateToProps, { get_list_modifications_sabbatical })(ModificationsSabbatical);