import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {
    continue_request_sabbatical
} from "../../../../../actions/sabbatical/continue_request_sabbatical";
import {
    finalized_request_sabbatical
} from "../../../../../actions/sabbatical/finalized_request_sabbatical";


export class OptionsProcess extends Component {

    static propTypes = {
        finalized_request_sabbatical: PropTypes.func.isRequired,
        continue_request_sabbatical: PropTypes.func.isRequired,
        onRedirectToListRequest: PropTypes.func.isRequired,
        pk: PropTypes.number.isRequired,
    }

    onActionSelect = (option) => {
        if (option){
            this.props.continue_request_sabbatical(this.props.pk);
        } else {
            this.props.finalized_request_sabbatical(this.props.pk);
        }

        this.props.onRedirectToListRequest();
    }


    render() {

        return (
            <section className="text-center py-0">
                <div className="col-11 mx-auto alert alert-warning my-0">
                    <h5>
                        <strong>
                            Para continuar con el proceso de la solicitud,
                            es necesario definir el siguiente estado.
                        </strong>
                    </h5>

                    <div className="col-11 mx-auto filter-section-dsch-multi-form pt-2">
                        <div className="alert alert-dark col-5 mx-auto">
                            <h6>
                                <strong>Proyecto rechazado, clic aquí.</strong>
                            </h6>
                            <button className="btn btn-dark my-2"
                                    onClick={this.onActionSelect.bind(this, false)}>
                                Rechazar Proyecto
                            </button>
                        </div>

                        <div className="alert bg-form-presupuestal col-5 mx-auto">
                            <h6>
                                <strong>Proyecto aceptado, clic aquí.</strong>
                            </h6>
                            <button className="btn btn-light my-2"
                                    onClick={this.onActionSelect.bind(this, true)}>
                                Aceptar Proyecto
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }

}


const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, {continue_request_sabbatical, finalized_request_sabbatical}
)(OptionsProcess);