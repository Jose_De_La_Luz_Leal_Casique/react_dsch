import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { ConfigDateSave, FormatDate } from "../../../../FormatDate";
import SelectMonth from "../SelectMonth";
import {
    edit_request_sabbatical
} from "../../../../../actions/sabbatical/edit_request_sabbatical";


export class EditSabbatical extends Component {

    state = {
        date_initial: '',
        date_final: '',
        months: 1,
        file: null,
        program: null,
        constancy: null,
    }

    static propTypes = {
        sabbatical: PropTypes.object,
        user: PropTypes.object,
        edit_request_sabbatical: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.onResetState();
    }

    onResetState = () => {
        this.setState({
            date_initial: this.props.sabbatical.date_initial.toString().substr(0, 10),
            date_final: this.props.sabbatical.date_final.toString().substr(0, 10),
            months: this.props.sabbatical.months,
        });
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({[e.target.name]: e.target.files[0]});

    onSubmitEdit = (e) => {
        e.preventDefault();

        let data = new FormData()
        data.append('user', this.props.user.user);
        data.append('date_initial', ConfigDateSave(this.state.date_initial));
        data.append('date_final', ConfigDateSave(this.state.date_final));
        data.append('months', this.state.months);

        try {
            data.append('file', this.state.file, this.state.file.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        try {
            data.append('program', this.state.program, this.state.program.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        try {
            data.append('constancy', this.state.constancy, this.state.constancy.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        this.props.edit_request_sabbatical(this.props.sabbatical.id, data);
    }

    render() {
        return (
            <div className="modal fade" id="edit-Sabbatical" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                Formulario para edición de solicitud
                            </h5>
                            <button type="button" className="close"
                                    data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-warning">
                            <form className="col-12 mx-auto text-center" id="edit-sabbatical"
                                  onSubmit={this.onSubmitEdit}>
                                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                                    <div className="col-4">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Fecha de inicio
                                            </strong>
                                        </h6>
                                        <input type="date" className="form-control"
                                               name="date_initial" required
                                               value={this.state.date_initial}
                                               onChange={this.onChange}/>
                                        <small>
                                            {
                                                this.state.date_initial !== ''
                                                    ?
                                                    <strong>
                                                        {
                                                            FormatDate(
                                                            ConfigDateSave(
                                                                this.state.date_initial))
                                                        }
                                                    </strong>
                                                    :
                                                    null
                                            }
                                        </small>
                                    </div>

                                    <div className="col-4">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Fecha de finalización
                                            </strong>
                                        </h6>
                                        <input type="date" className="form-control"
                                               name="date_final" required
                                               value={this.state.date_final}
                                               onChange={this.onChange}/>
                                        <small>
                                            {
                                                this.state.date_final !== ''
                                                    ?
                                                    <strong>
                                                        {FormatDate(
                                                            ConfigDateSave(
                                                                this.state.date_final)) }
                                                    </strong>
                                                    :
                                                    null
                                            }
                                        </small>
                                    </div>

                                    <div className="col-4">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Meses de duración
                                            </strong>
                                        </h6>
                                        <SelectMonth
                                            select_month={parseInt(this.state.months)}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </section>

                                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                                    <div className="col-6">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Solicitud de sabático
                                            </strong>
                                        </h6>
                                        <input type="file" className="form-control" name="file"
                                               value={this.file} onChange={this.onChangeFile}
                                               accept="application/pdf"/>
                                    </div>

                                    <div className="col-6">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Programa de actividades
                                            </strong>
                                        </h6>
                                        <input type="file" className="form-control"
                                               name="program"
                                               value={this.program}
                                               onChange={this.onChangeFile}
                                               accept="application/pdf"/>
                                    </div>
                                </section>

                                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                                    <div className="col-6">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Constancia de antigüedad
                                            </strong>
                                        </h6>
                                        <input type="file" className="form-control"
                                               name="constancy"
                                               value={this.constancy}
                                               onChange={this.onChangeFile}
                                               accept="application/pdf"/>
                                    </div>

                                    <div className="col-6 mb-4 pt-3">
                                        <button className="btn btn-warning px-2 mt-2 float-right">
                                            Guardar cambios en solicitud
                                        </button>
                                    </div>
                                </section>


                            </form>
                        </div>
                        <div className="modal-footer bg-form-presupuestal" >
                            <button type="button" className="btn btn-light mr-auto"
                                    onClick={this.onResetState}>
                                Conservar información anterior
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps, { edit_request_sabbatical } )(EditSabbatical);