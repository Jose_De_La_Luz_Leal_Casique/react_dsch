import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { ConfigDateSave } from "../../../../FormatDate";
import { change_date_initial } from "../../../../../actions/sabbatical/change_date_initial";


export class FormDateInitial extends Component {

    state = {
        date_initial: '',
        date_final: '',
        file: null
    }

    static propTypes = {
        change_date_initial: PropTypes.func.isRequired,
        get_list_modifications_sabbatical: PropTypes.func.isRequired,
        pk: PropTypes.number.isRequired,
        sabbatical: PropTypes.object,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({[e.target.name]: e.target.files[0]});

    onResetForm = () => {
        document.getElementById('form-change-initial').reset();
        this.setState({date_initial: '', date_final: ''});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('date_initial', ConfigDateSave(this.state.date_initial))
        data.append('date_final', ConfigDateSave(this.state.date_final))
        data.append('file', this.state.file, this.state.file.name)
        this.props.change_date_initial(this.props.pk, data);
        this.onResetForm();
    }

    render() {
        return (
            <div className="modal fade" tabIndex="-1" role="dialog" id="modal-change-initial"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Edición de fecha de inicio
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={this.onSubmit} id="form-change-initial">
                            <div className="modal-body">
                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Fecha de inicio</strong></h5>
                                    <input type="date" className="form-control"
                                           name="date_initial" value={this.state.date_initial}
                                           onChange={this.onChange} required />
                                </div>

                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Fecha de fin</strong></h5>
                                    <input type="date" className="form-control"
                                           name="date_final" value={this.state.date_final}
                                           onChange={this.onChange} required />
                                </div>

                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Solicitud</strong></h5>
                                    <input type="file" className="form-control"
                                           accept="application/pdf" name="file" value={this.file}
                                           onChange={this.onChangeFile} required/>
                                </div>
                            </div>

                            <div className="modal-footer bg-form-presupuestal">
                                <button type="reset" className="btn btn-light"
                                        onClick={this.onResetForm}>
                                    Limpiar Formulario
                                </button>
                                <button type="submit" className="btn btn-dark">
                                    Enviar cambios
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical
});

export default connect(mapStateToProps, {change_date_initial} )(FormDateInitial);