import React, { Component } from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import DetailSabbatical from "./sabbatical/components/DetailSabbatical";
import DataApproved from "./sabbatical/components/DataApproved";
import ModificationsSabbatical from "./sabbatical/components/components/ModificationsSabbatical";
import ReportSabbatical from "./sabbatical/components/ReportSabbatical";
import { get_detail_sabbatical } from "../../actions/sabbatical/get_detail_sabbatical"


class ViewSabbatical extends Component {

    state = {
        pk: 0,
        option: 0,
        view: null,
        select_view: 1,
    }

    static propTypes = {
        get_detail_sabbatical: PropTypes.func.isRequired,
        sabbatical: PropTypes.object,
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({
            pk: params.pk,
            option: params.option,
            view: <DetailSabbatical pk={params.pk}/>
        });
        this.props.get_detail_sabbatical(params.pk);
    }

    onRedirectToListApproved = () => {
        return this.props.history.push('/approved-sabbatical')
    }

    onRedirectToListRequest = () => {
        return this.props.history.push('/request-sabbatical')
    }

    onRedirectToListSabbatical = () => {
        return this.props.history.push('/sabbatical')
    }


    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        switch (parseInt(e.target.value)) {

            case 1:
                this.setState({view: <DetailSabbatical pk={this.state.pk}/>});
                break;

            case 2:
                this.setState({
                    view: <DataApproved onRedirectToListRequest={this.onRedirectToListRequest}/>
                });
                break;

            case 3:
                this.setState({view: <ModificationsSabbatical />});
                break;

            case 4:
                this.setState({
                    view: <ReportSabbatical report={this.props.sabbatical.report}
                                            onRedirectToListSabbatical={this.onRedirectToListSabbatical}
                                            onRedirectToListApproved={this.onRedirectToListApproved}
                    />
                });
                break;

            default: {
                this.setState({
                    view: <section className="text-center my-auto">
                        <h4><strong>Sin coincidencias</strong></h4>
                    </section>
                })
            }
        }
    }

    onCheckToModifications = () => {
        return (
            this.props.sabbatical.status !== 'Periodo sabático en revisión'
                &&
            this.props.sabbatical.status !== 'Periodo sabático rechazado'
                &&
            this.props.sabbatical.status !== 'En espera de aceptación'
            )
    }

    render() {
        return (
            <section>
                <section className="filter-section-dsch-multi-form alert alert-warning mt-4">
                    <div className="col-8 mx-auto text-right pr-5 pt-2">
                        <h4>
                            <strong>
                                Solicitud de periodo sabático
                            </strong>
                        </h4>
                    </div>
                    <div className="col-4 mx-auto">
                        <Link to={{
                            pathname:
                                this.state.option === 1
                                    ? '/sabbatical'
                                    :
                                this.state.option === 2
                                    ?
                                    '/approved-sabbatical'
                                    :
                                    '/request-sabbatical'
                        }} >
                            <button className="btn-dsch float-right px-1">
                                Regresar
                            </button>
                        </Link>
                    </div>
                </section>

                <section className="alert filter-section-dsch-multi-form py-0">
                    <div className="col-4">
                        <div className="alert alert-warning col-12 mx-0">
                            <h6>
                                <strong>
                                    Opción de solicitud a visualizar
                                </strong>
                            </h6>
                            {
                                this.props.sabbatical !== null
                                    ?
                                    <select className="form-control" name="select_view"
                                        onChange={this.onChange} value={this.state.select_view}>
                                        <option key={1} value={1}>
                                            Solicitud de periodo sabático
                                        </option>

                                        <option key={2} value={2}>
                                            Información de aceptación
                                        </option>

                                        {
                                            this.onCheckToModifications()
                                                ?
                                                <option key={3} value={3}>
                                                    Modificaciones en solicitud
                                                </option>
                                                :
                                                null
                                        }

                                        {
                                            this.props.sabbatical.status === 'Periodo sabático finalizado'
                                                ?
                                                <option key={4} value={4}>
                                                    Agregar informe y probatorios
                                                </option>
                                                :
                                                null
                                        }

                                    </select>
                                    :
                                    null
                            }
                        </div>
                    </div>
                    <div className="card col-8 alert-warning mt-0">
                        {this.state.view}
                    </div>
                </section>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical
});

export default connect( mapStateToProps, { get_detail_sabbatical } )(ViewSabbatical);