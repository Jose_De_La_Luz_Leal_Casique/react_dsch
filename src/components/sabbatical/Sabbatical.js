import React, { Component } from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import ListSabbatical from "./sabbatical/components/ListSabbatical";
import { get_my_list_sabbatical } from "../../actions/sabbatical/get_my_list_sabbatical";


class Sabbatical extends Component {

    static propTypes = {
        user: PropTypes.object,
        get_my_list_sabbatical: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_my_list_sabbatical(this.props.user.user);
    }

    render() {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form alert alert-warning mt-4">
                    <div className="col-8 mx-auto text-right pr-5 pt-2">
                        <h4><strong>Mis Periodos Sabáticos</strong></h4>
                    </div>
                    <div className="col-4 mx-auto">
                        <Link to={
                            { pathname: '/new-sabbatical', state:{pk: 0, edit: false} }
                        } >
                            <button className="btn-dsch float-right px-1">
                                Solicitar Sabático
                            </button>
                        </Link>
                    </div>
                </section>

                <section className="col-12 mx-auto mt-3">
                    <ListSabbatical option={1}/>
                </section>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user,
});

export default connect(mapStateToProps, { get_my_list_sabbatical })(Sabbatical);
