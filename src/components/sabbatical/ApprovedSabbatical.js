import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../academic/components/Year";
import ListSabbatical from "./sabbatical/components/ListSabbatical";
import ReportSabbatical from "./reports/ReportSabbatical";
import { get_list_sabbatical_approved } from "../../actions/sabbatical/get_list_sabbatial_approved";


class ApprovedSabbatical extends Component {

    state = {
        year: new Date().getFullYear()
    }

    static propTypes = {
        user: PropTypes.object,
        permissions: PropTypes.array,
        get_list_sabbatical_approved: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_list_sabbatical_approved(this.props.user.user, this.state.year);
    }

    check_permission_commission = () => {
        if ( this.props.permissions.includes("investigation.admin_evaluation_commission")){
            return true
        }
        return false
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
        this.props.get_list_sabbatical_approved(this.props.user.user,  e.target.value);
    }

    render() {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form mt-2">
                    <div className="col-6 mx-auto mt-3 text-center">
                        <h4>
                            <strong>
                                Listado de periodos sabáticos aprobados
                            </strong>
                        </h4>
                        {
                            this.check_permission_commission()
                                ?
                                <button className="btn-dsch px-2"
                                    data-toggle="modal" data-target="#create-report-sabbatical">
                                    Generar reporte de periodos sabáticos
                                </button>
                                :
                                null
                        }
                    </div>
                    <section
                        className="alert alert-warning col-5 py-2 filter-section-dsch-multi-form mt-2">
                        <div className="col-9 pt-1 px-0">
                            <h6>
                                <strong>
                                    Seleccionar año para ver periodos sabáticos aprobados
                                </strong>
                            </h6>
                        </div>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </section>
                </section>

                <section className="col-12 mx-auto mt-3">
                    <ListSabbatical option={2}/>
                </section>

                <ReportSabbatical />
            </section>
        )
    }

}


const mapStateToProps = (state) => ({
    user: state.auth.user,
    permissions: state.auth.permissions
});

export default connect( mapStateToProps, { get_list_sabbatical_approved } )(ApprovedSabbatical);