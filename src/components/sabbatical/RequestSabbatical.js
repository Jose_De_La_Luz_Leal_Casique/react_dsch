import React, { Component } from "react";
import PropTypes from "prop-types";
import Year from "../academic/components/Year";
import ListSabbatical from "./sabbatical/components/ListSabbatical";
import {connect} from "react-redux";
import {get_list_sabbatical_request} from "../../actions/sabbatical/get_list_sabbatical_request";


class RequestSabbatical extends Component {

    state = {
        year: new Date().getFullYear()
    }

    static propTypes = {
        user: PropTypes.object,
        get_list_sabbatical_request: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_list_sabbatical_request(this.props.user.user, this.state.year);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
        this.props.get_list_sabbatical_request(this.props.user.user, e.target.value);
    }

    render() {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form mt-2">
                    <div className="col-6 mx-auto mt-3 text-center">
                        <h4>
                            <strong>
                                Ver solicitudes de periodos sabáticos
                            </strong>
                        </h4>
                    </div>
                    <section
                        className="alert alert-warning col-5 py-2 filter-section-dsch-multi-form mt-2">
                        <div className="col-9 pt-1 px-0">
                            <h6>
                                <strong>
                                    Seleccionar año para ver periodos sabáticos aprobados
                                </strong>
                            </h6>
                        </div>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </section>
                </section>

                <section className="col-12 mx-auto mt-3">
                    <ListSabbatical option={3}/>
                </section>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user,
});

export default connect( mapStateToProps, { get_list_sabbatical_request } )(RequestSabbatical);
