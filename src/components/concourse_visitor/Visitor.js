import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { create_concourse } from '../../actions/concourse/createconcourse';


export class Visitor extends  Component {

    state = {
        code: '',
        description: '',
        file: null,
        type_concourse: 4,
    }

    static propTypes = {
        create_concourse: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});
    onResetState = () => this.setState({
        code: '',
        description: '',
        file: null,
        date_created: '',
        date_send: '',
        type_concourse: 4,
    });


    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('code', this.state.code);
        data.append('description', this.state.description);
        data.append('file', this.state.file, this.state.file.name);
        data.append('type_concourse', this.state.type_concourse);

        this.onResetState();
        this.props.create_concourse(data);
        document.getElementById('form-curricular').reset();
    }

    render() {
        return (
            <div className="col-9 mx-auto alert alert-warning mt-5">
                <div className="text-center my-4 pb-2">
                    <h4><strong>Registro de Profesores Visitantes</strong></h4>
                </div>
                <hr></hr>

                <form onSubmit={this.onSubmit} id="form-curricular" className="pb-5 pt-3">
                    <section className="filter-section-dsch-multi-form">
                        <div className="col-4 p-1">
                            <strong className="ml-2">Folio Visitante:</strong>
                            <input type="text" minLength={10} name="code" required
                                   value={this.state.code} onChange={this.onChange}
                                   className="form-control"/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-2">Descripción:</strong>
                            <textarea className="form-control" name="description" rows={2}
                                   required value={this.state.description}
                                   onChange={this.onChange}/>
                        </div>

                        <div className="col-4 p-1">
                            <strong className="ml-2">Acuerdo de consejo Divisional:</strong>
                            <input type="file" className="form-control" name="file"
                                   required value={this.file} onChange={this.onChangeFile}
                                   accept="application/pdf" />
                        </div>
                    </section>

                    <section className="filter-section-dsch-multi-form mt-3">

                        <div className="col-2 p-1 text-center mx-auto">
                            <button className="btn-dsch mt-4 btn-block">Guardar Folio</button>
                        </div>
                    </section>

                </form>
            </div>
        )
    }
}

const mapStateProps = (state) => ({

});

export default connect(mapStateProps, { create_concourse })(Visitor);