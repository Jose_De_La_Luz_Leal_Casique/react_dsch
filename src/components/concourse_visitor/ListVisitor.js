import React, { Component } from 'react';
import { connect } from "react-redux";

import ConcourseList from "../concourse/ConcourseList";


export class ListVisitor extends  Component {

    render() {
        return (
            <ConcourseList type_concourse={4}/>
        )
    }
}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null )(ListVisitor);