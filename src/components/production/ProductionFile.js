import React, { Component } from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../academic/components/Year";
import { set_file_data_production } from "../../actions/production/set_file_data_production";


export class ProductionFile extends Component {

    state = {
        year: new  Date().getFullYear(),
        file: null

    }

    static propTypes = {
        set_file_data_production: PropTypes.func.isRequired
    }

    onLoadFile = (e) => {
        this.setState({file: e.target.files[0]});
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('year', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.set_file_data_production(data);

        } catch (e){
            if (e.TypeError === undefined){
                alert('No se adjuntó archivo.');
            }
        }
    }

    render() {
        return (
            <section className="filter-section-dsch-multi-form">
                <section className="col-5 mx-auto">
                    <section className="card mt-3">
                        <div className="modal-header title-detail">
                            <h5 className="m-auto">
                                <strong>
                                    Registro de Información mediante archivo
                                </strong>
                            </h5>
                        </div>
                        <form onSubmit={this.onSubmit}>
                            <div className="col-12 filter-section-dsch-form mt-4">
                                <h6 className="col-5 mt-3"><strong>Seleccionar año</strong></h6>
                                <Year
                                    select_year={this.state.year.toString()}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="col-12 filter-section-dsch-form mt-4">
                                <h6 className="col-5 mt-3"><strong>Agregar Archivo</strong></h6>
                                <input type="file" className="form-control" name="file" required
                                       onChange={this.onLoadFile} value={this.file}
                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                />
                            </div>
                            <div className="col-12 filter-section-dsch-form my-4">
                                <button className="btn btn-primary mx-auto">Guardar</button>
                            </div>
                        </form>
                    </section>
                </section>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { set_file_data_production } )(ProductionFile);