import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_published } from "../../../../actions/production/edit_published";


export class FormPublishedLectures extends Component {

    state = {
        file: null,
        title: '',
        editor: '',
        ed: '',
        congress: '',
        place: '',
        pages: '',
        editorial: '',
        type_job: 'Conferencia magistral',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_published: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            editor: this.props.obj.editor,
            ed: this.props.obj.ed,
            congress: this.props.obj.congress,
            place: this.props.obj.place,
            pages: this.props.obj.pages,
            editorial: this.props.obj.editorial,
            type_job: this.props.obj.type_job,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('editor', this.state.editor);
        data.append('ed', this.state.ed);
        data.append('congress', this.state.congress);
        data.append('pages', this.state.pages);
        data.append('place', this.state.place);
        data.append('editorial', this.state.editorial);
        data.append('type_job', this.state.type_job);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_published(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_published(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título del Conferencia </strong></h6>
                        <textarea className="form-control" name="title" rows={3}
                               value={this.state.title} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título de congreso </strong></h6>
                        <textarea className="form-control" name="congress"  rows={3}
                               value={this.state.congress} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Ed. o Eds.</strong></h6>
                        <input type="text" className="form-control" name="ed"
                               value={this.state.ed} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de editor</strong></h6>
                        <input type="text" className="form-control" name="editor"
                               value={this.state.editor} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Lugar </strong></h6>
                        <input type="text" className="form-control" name="place"
                               value={this.state.place} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Páginas </strong></h6>
                        <input type="text" className="form-control" name="pages"
                               value={this.state.pages} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Editorial</strong></h6>
                        <textarea  className="form-control" name="editorial" rows={2}
                               value={this.state.editorial} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Tipo de trabajo</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Conferencia</option>
                            <option key={1}>Conferencia magistral</option>
                            <option key={2}>Trabajo en evento especializado</option>
                        </select>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { edit_published } )(FormPublishedLectures);