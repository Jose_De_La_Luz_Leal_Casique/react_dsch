import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_books } from "../../../../actions/production/edit_book";


export class FormPublishedBooks extends Component {

    state = {
        file: null,
        title: '',
        edition: '',
        place: '',
        editorial: '',
        isbn: '',
        type_job: 'Investigación',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_books: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            edition:  this.props.obj.edition,
            place:  this.props.obj.place,
            editorial:  this.props.obj.editorial,
            isbn:  this.props.obj.isbn,
            type_job:  this.props.obj.type_job,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});
    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('edition', this.state.edition);
        data.append('place', this.state.place);
        data.append('editorial', this.state.editorial);
        alert(this.state.isbn)
        data.append('isbn', this.state.isbn);
        data.append('type_job', this.state.type_job);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_books(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_books(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título del Libro</strong></h6>
                        <textarea className="form-control" name="title" rows={2}
                               value={this.state.title} onChange={this.onChange} />
                    </div>
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Edición (a partir de segunda)</strong></h6>
                        <input type="text" className="form-control" name="edition"
                               value={this.state.edition} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Lugar</strong></h6>
                        <input type="text" className="form-control" name="place"
                               value={this.state.place} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Editorial</strong></h6>
                        <input type="text" className="form-control" name="editorial"
                               value={this.state.editorial} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>ISBN</strong></h6>
                        <input type="text" className="form-control" name="isbn"
                               value={this.state.isbn} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Tipo de trabajo:</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Investigación</option>
                            <option key={1}>Divulgación</option>
                        </select>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { edit_books } )(FormPublishedBooks);