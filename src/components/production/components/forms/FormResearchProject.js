import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Departaments from "../../../academic/components/Departaments";
import { ConfigDateSave, FormatDate } from "../../../FormatDate";
import { edit_research } from '../../../../actions/production/edit_research';


export class FormResearchProject extends Component {

    state = {
        file: null,
        departament: 'Humanidades',
        name: '',
        participants: '',
        approval: '',
        validity: '',
        section: '',
        means: '',
        line: '',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_research: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            departament: this.props.obj.departament,
            name: this.props.obj.name,
            participants: this.props.obj.participants,
            validity: this.props.obj.validity,
            section: this.props.obj.section,
            means: this.props.obj.means,
            line: this.props.obj.line,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('departament', this.state.departament);
        data.append('name', this.state.name);
        data.append('participants', this.state.participants);
        data.append('validity', this.state.validity);
        data.append('section', this.state.section);
        data.append('means', this.state.means);
        data.append('line', this.state.line);

        if (this.state.approval !== ''){
            data.append('approval', FormatDate(ConfigDateSave(this.state.approval)));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_research(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_research(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-1 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de proyecto aprobado</strong></h6>
                        <textarea className="form-control" name="name" rows={3}
                               value={this.state.name} onChange={this.onChange} />
                    </div>
                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Nombres de profesores participantes </strong></h6>
                        <textarea className="form-control" name="participants" rows={3}
                               value={this.state.participants} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                     <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Fecha de aprobación ({this.props.obj.approval})</strong></h6>
                        <input type="date" className="form-control" name="approval"
                               value={this.state.approval} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Sesión de consejo divisional</strong></h6>
                        <input type="text" className="form-control" name="section"
                               value={this.state.section} onChange={this.onChange} />
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Líneas de Investigación </strong></h6>
                        <textarea className="form-control" name="line" rows={2}
                               value={this.state.line} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Seleccionar Departamento:</strong></h6>
                        <Departaments
                            select_departament={this.state.departament.toString()}
                            onChange={this.onChange}
                        />
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Vigencia</strong></h6>
                        <input type="text" className="form-control" name="validity"
                               value={this.state.validity} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Monto de recursos</strong></h6>
                        <input type="text" className="form-control" name="means"
                               value={this.state.means} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-12 mx-auto pt-1">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_research })(FormResearchProject);