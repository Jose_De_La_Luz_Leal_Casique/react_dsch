import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_review } from '../../../../actions/production/edit_review';


export class FormBookReviews extends Component {

    state = {
        file: null,
        title: '',
        book: '',
        publication: '',
        number: '',
        volume: '',
        pages: ''
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_review: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            book: this.props.obj.book,
            publication: this.props.obj.publication,
            number: this.props.obj.number,
            volume: this.props.obj.volume,
            pages: this.props.obj.pages,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('book', this.state.book);
        data.append('publication', this.state.publication);
        data.append('number', this.state.number);
        data.append('pages', this.state.pages);
        data.append('volume', this.state.volume);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_review(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_review(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título de reseña</strong></h6>
                        <textarea className="form-control" name="title" rows={2}
                               value={this.state.title} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Título de Libro </strong></h6>
                        <textarea className="form-control" name="book" rows={2}
                               value={this.state.book} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título de publicación</strong></h6>
                        <textarea className="form-control" name="publication" rows={2}
                               value={this.state.publication} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Número</strong></h6>
                        <input type="text" className="form-control" name="number"
                               value={this.state.number} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Volumen</strong></h6>
                        <input type="text" className="form-control" name="volume"
                               value={this.state.volume} onChange={this.onChange} />
                    </div>
                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Páginas</strong></h6>
                        <input type="text" className="form-control" name="pages"
                               value={this.state.pages} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>

                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>

            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_review })(FormBookReviews);