import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_training } from "../../../../actions/production/edit_training";


export class FormTeacherTraining extends Component {

    state = {
        name: '',
        institute: '',
        type_activity: '',
        file: null,
        hours: 0
    }

    static propTypes = {
        obj: PropTypes.object.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_training: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            name: this.props.obj.name,
            institute: this.props.obj.institute,
            type_activity: this.props.obj.type_activity,
            hours: this.props.obj.hours
        });
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});
    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('name', this.state.name);
        data.append('institute', this.state.institute);
        data.append('type_activity', this.state.type_activity);
        data.append('hours', this.state.hours);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_training(this.props.obj.id, data);
            this.props.onEnableEdit();

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_training(this.props.obj.id, data);
                    this.props.onEnableEdit();
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >

                    <div className="col-8 mx-auto">
                        <label className="ml-1"><strong>Nombre de Actividad</strong></label>
                        <textarea className="form-control" name="name" rows={2}
                               value={this.state.name} onChange={this.onChange}
                               placeholder="Actividad de actualización o formación" />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="mt-1"><strong>Tipo de actividad</strong></h6>
                        <input type="text" className="form-control" name="type_activity"
                               value={this.state.type_activity} onChange={this.onChange} />
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>Instituto que impartió</strong></label>
                        <input type="text" className="form-control" name="institute"
                               value={this.state.institute} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <label className="ml-1"><strong>Número de Horas</strong></label>
                        <input type="number" className="form-control" name="hours"
                               value={this.state.hours} onChange={this.onChange} />
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1">Seleccionar probatorio</h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>

            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {edit_training} )(FormTeacherTraining);