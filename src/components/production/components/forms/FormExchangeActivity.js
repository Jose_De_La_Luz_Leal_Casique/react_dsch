import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_exchange } from '../../../../actions/production/edit_exchange';


export class FormExchangeActivity extends Component {

    state = {
        country: '',
        ies: '',
        activity: '',
        file: null,
        division: ''
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_exchange: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            country: this.props.obj.country,
            ies: this.props.obj.ies,
            activity: this.props.obj.activity,
            division: this.props.obj.division,
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('activity', this.state.activity);
        data.append('ies', this.state.ies);
        data.append('country', this.state.country);
        data.append('division', this.state.division);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_exchange(this.props.obj.id, data);
            this.props.onEnableEdit();

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_exchange(this.props.obj.id, data);
                    this.props.onEnableEdit();
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-3 mx-auto">
                        <label className="ml-1"><strong>División</strong></label>
                        <input type="text" className="form-control" name="division"
                               value={this.state.division} onChange={this.onChange} />
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Actividad de Intercambio</strong></h6>
                        <input type="text" className="form-control" name="activity"
                               value={this.state.activity} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>Nombe de IES receptora </strong></label>
                        <input type="text" className="form-control" name="ies"
                               value={this.state.ies} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>Cuidad, País de la IES</strong></label>
                        <input type="text" className="form-control" name="country"
                               value={this.state.country} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="mt-1">Seleccionar probatorio</h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>

                    <div className="col-4 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_exchange })(FormExchangeActivity);