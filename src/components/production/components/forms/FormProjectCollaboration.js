import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_p_collaboration } from '../../../../actions/production/edit_p_collaboration';


export class FormProjectCollaboration extends Component {

    state = {
        file: null,
        name: '',
        sector: 'Privado',
        organizations: '',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_p_collaboration: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            name: this.props.obj.name,
            sector: this.props.obj.sector,
            organizations: this.props.obj.organizations,
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('name', this.state.name);
        data.append('sector', this.state.sector);
        data.append('organizations', this.state.organizations);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_p_collaboration(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_p_collaboration(this.props.obj.id, data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Proyecto </strong></label>
                        <textarea className="form-control" name="name" rows={3}
                               value={this.state.name} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Instituciones pertenecientes a la red </strong></h6>
                        <input type="text" className="form-control" name="organizations"
                               value={this.state.organizations} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Seleccionar Sector</strong></label>
                        <select className="form-control" name="sector"
                                value={this.state.sector} onChange={this.onChange} >
                            <option key={0}>Público</option>
                            <option key={1}>Privado</option>
                        </select>
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-12 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_p_collaboration } )(FormProjectCollaboration);