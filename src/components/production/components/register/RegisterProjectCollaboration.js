import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import { add_project_collaboration } from '../../../../actions/production/add_project_collaboration';


export class RegisterProjectCollaboration extends Component {

    state = {
        year: new Date().getFullYear(),
        file: null,
        user: '',
        name: '',
        sector: 'Privado',
        organizations: '',
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_project_collaboration: PropTypes.func.isRequired
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('name', this.state.name);
        data.append('sector', this.state.sector);
        data.append('organizations', this.state.organizations);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_project_collaboration(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_project_collaboration(data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Nombre de profesor *</strong></label>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Proyecto *</strong></label>
                        <textarea className="form-control" name="name" required rows={3}
                               value={this.state.name} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Instituciones pertenecientes a la red </strong></h6>
                        <input type="text" className="form-control" name="organizations"
                               value={this.state.organizations} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Seleccionar Sector</strong></label>
                        <select className="form-control" name="sector"
                                value={this.state.sector} onChange={this.onChange} >
                            <option key={0}>Público</option>
                            <option key={1}>Privado</option>
                        </select>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-3 mx-auto">
                        <label className="ml-1"><strong>Selecionar Año:</strong></label>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-3 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_project_collaboration } )(RegisterProjectCollaboration);