import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import { add_exchange } from '../../../../actions/production/add_exchange';


export class RegisterExchangeActivity extends Component {

    state = {
        year: new Date().getFullYear(),
        user: '',
        country: '',
        ies: '',
        activity: '',
        file: null,
        division: ''
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_exchange: PropTypes.func.isRequired
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('activity', this.state.activity);
        data.append('ies', this.state.ies);
        data.append('country', this.state.country);
        data.append('division', this.state.division);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_exchange(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_exchange(data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Nombre de profesor *</strong></label>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>División *</strong></label>
                        <input type="text" className="form-control" name="division" required
                               value={this.state.division} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Actividad de Intercambio * </strong></h6>
                        <input type="text" className="form-control" name="activity" required
                               value={this.state.activity} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombe de IES receptora *</strong></label>
                        <input type="text" className="form-control" name="ies" required
                               value={this.state.ies} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Cuidad, País de la IES *</strong></label>
                        <input type="text" className="form-control" name="country" required
                               value={this.state.country} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1">Seleccionar probatorio</h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Selecionar Año:</strong></label>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-6 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right mr-3"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_exchange })(RegisterExchangeActivity);