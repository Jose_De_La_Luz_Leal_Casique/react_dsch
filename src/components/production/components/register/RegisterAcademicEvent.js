import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import { add_event } from '../../../../actions/production/add_event';


export class RegisterAcademicEvent extends Component {

    state = {
        year: new Date().getFullYear(),
        file: null,
        user: '',
        name: '',
        type_activity: '',
        country: '',
        institute: '',
        range_date: '',
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_event: PropTypes.func.isRequired
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('name', this.state.name);
        data.append('type_activity', this.state.type_activity);
        data.append('institute', this.state.institute);
        data.append('country', this.state.country);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_event(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_event(data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Nombre de profesor *</strong></label>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Evento *</strong></label>
                        <input type="text" className="form-control" name="name" required
                               value={this.state.name} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Instituto Sede </strong></h6>
                        <input type="text" className="form-control" name="institute"
                               value={this.state.institute} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>País/Cuidad</strong></label>
                        <input type="text" className="form-control" name="country"
                               value={this.state.country} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Fecha *</strong></label>
                        <input type="text" className="form-control" name="range_date" required
                               value={this.state.range_date} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Tipo de Actividad Participación *</strong></label>
                        <input type="text" className="form-control" name="type_activity" required
                               value={this.state.type_activity} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-3 mx-auto">
                        <label className="ml-1"><strong>Selecionar Año:</strong></label>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-3 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_event })(RegisterAcademicEvent);