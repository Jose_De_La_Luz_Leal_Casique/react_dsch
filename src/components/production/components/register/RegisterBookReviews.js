import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import { add_book_review } from '../../../../actions/production/add_book_review';


export class RegisterBookReviews extends Component {

    state = {
        year: new Date().getFullYear(),
        user: '',
        file: null,
        title: '',
        book: '',
        publication: '',
        number: '',
        volume: '',
        pages: ''
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_book_review: PropTypes.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});
    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('title', this.state.title);
        data.append('book', this.state.book);
        data.append('publication', this.state.publication);
        data.append('number', this.state.number);
        data.append('pages', this.state.pages);
        data.append('volume', this.state.volume);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_book_review(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_book_review(data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de autor *</strong></h6>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título de reseña *</strong></h6>
                        <textarea className="form-control" name="title" required rows={2}
                               value={this.state.title} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Título de Libro * </strong></h6>
                        <textarea className="form-control" name="book" required rows={2}
                               value={this.state.book} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de revista *</strong></h6>
                        <textarea className="form-control" name="publication" required rows={2}
                               value={this.state.publication} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Número</strong></h6>
                        <input type="text" className="form-control" name="number"
                               value={this.state.number} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Volumen</strong></h6>
                        <input type="text" className="form-control" name="volume"
                               value={this.state.volume} onChange={this.onChange} />
                    </div>
                    <div className="col-2 mx-auto">
                        <h6 className="ml-1"><strong>Páginas</strong></h6>
                        <input type="text" className="form-control" name="pages"
                               value={this.state.pages} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="ml-1"><strong>Selecionar Año:</strong></h6>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-6 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right mr-3"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_book_review })(RegisterBookReviews);