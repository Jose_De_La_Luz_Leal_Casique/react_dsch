import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import { add_training } from '../../../../actions/production/add_training';


export class RegisterTeacherTraining extends Component {

    state = {
        year: new Date().getFullYear(),
        user: '',
        name: '',
        institute: '',
        type_activity: '',
        file: null,
        hours: 0
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_training: PropTypes.func.isRequired
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('name', this.state.name);
        data.append('institute', this.state.institute);
        data.append('type_activity', this.state.type_activity);
        data.append('hours', this.state.hours);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_training(data);
        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_training(data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Nombre de profesor *</strong></label>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Actividad *</strong></label>
                        <input type="text" className="form-control" name="name" required
                               value={this.state.name} onChange={this.onChange}
                               placeholder="Actividad de actualización o formación" />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Tipo de actividad *</strong></h6>
                        <input type="text" className="form-control" name="type_activity" required
                               value={this.state.type_activity} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Instituto que impartió * </strong></label>
                        <input type="text" className="form-control" name="institute" required
                               value={this.state.institute} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Número de Horas *</strong></label>
                        <input type="number" className="form-control" name="hours" required
                               value={this.state.hours} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1">Seleccionar probatorio</h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <label className="ml-1"><strong>Selecionar Año:</strong></label>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-6 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right mr-3"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_training })(RegisterTeacherTraining);