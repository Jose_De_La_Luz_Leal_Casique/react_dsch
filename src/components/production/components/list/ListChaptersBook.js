import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ViewChaptersBook from "../views/ViewChaptersBook";


export class ListChaptersBook extends Component {

    state = {
        objetView: null
    }

    static propTypes = {
        production: PropTypes.object.isRequired,
        get_full_name: PropTypes.func.isRequired,
    }

    getObjectView = (obj) => this.setState({ objetView: obj});

    render() {
        return (

            <section>
                {
                    this.props.production.load
                        ?
                        this.props.production.chapters.length > 0
                            ?
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th width={410}>Profesor(es)</th>
                                    <th width={600}>Nombre de capítulo</th>
                                    <th width={150}></th>
                                </tr>
                                </thead>
                                <tbody className="table-scroll">
                                {
                                    this.props.production.chapters.map((register, index) => (
                                        <tr className="alert-primary" key={index}>
                                            <td width={410}>
                                                {register.profile.map((user, number) => (
                                                  <li className="list-profile" key={number}>
                                                      + {this.props.get_full_name(user)}
                                                  </li>
                                                ))}
                                            </td>
                                            <td width={600}>
                                                {register.name}
                                            </td>
                                            <td className="text-center" width={150}>
                                                <button className="btn-dsch"
                                                        data-toggle="modal"
                                                        data-target="#viewTeacherTraining"
                                                        onClick={
                                                            this.getObjectView.bind(
                                                                this, register)} >
                                                    Ver Detalle
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                            :
                            <div className="text-center my-5 alert-danger py-4 col-6 mx-auto">
                                <h4><strong>NO HAY COINCIDENCIAS</strong></h4>
                            </div>
                        :
                        <div className="text-center my-5 alert-danger py-4 col-6 mx-auto">
                            <h4><strong>NO HAY COINCIDENCIAS</strong></h4>
                        </div>
                }
                <ViewChaptersBook
                    register={this.state.objetView}
                    get_full_name={this.props.get_full_name}
                />
            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    production: state.production
});

export default connect(mapStateToProps, null )(ListChaptersBook);