import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {SERVER} from "../../../../actions/server";
import FormElectronicJournals from "../forms/FormElectronicJournals";


export class ViewElectronicJournals extends Component {

    state = {
        edit: false,
    }

    static propTypes = {
        register: PropTypes.object,
        get_full_name: PropTypes.func.isRequired,
        permissions: PropTypes.array
    }

    onEnableEdit = (e) => this.setState({edit: this.state.edit ? false : true});
    onEditFalse = (e) => this.setState({edit: false});

    isUrl = (string) => {
        let regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(string);
    }

    render() {
        let detailView = null;

        if (this.props.register != null){
            detailView = (
                <div className="col-11 m-auto">
                    <h6>
                        <strong>Profesor (s): </strong>
                        <ul>
                        {this.props.register.profile.map((user, index) =>(
                            <li key={user.id}>{this.props.get_full_name(user)}</li>
                        ))}
                        </ul>
                    </h6>
                    <h6><strong>Título de artículo: </strong>
                        {this.props.register.title}
                    </h6>
                    <h6><strong>Título de publicación: </strong> {this.props.register.magazine_name}</h6>
                    <h6><strong>Número: </strong>
                        { this.props.register.number === null
                            ? 'NO REGISTRADO' : this.props.register.number }
                    </h6>
                    <h6><strong>Volumen: </strong>
                        {this.props.register.volume === null
                            ? 'NO REGISTRADO' : this.props.register.volume }
                    </h6>
                    <h6><strong>Páginas: </strong>
                        { this.props.register.pages === null
                            ? 'NO REGISTRADO' : this.props.register.pages }
                    </h6>
                    <h6><strong>¿Es una revista indexada?: </strong>
                        {this.props.register.is_indexed_magazine}
                    </h6>
                    <h6><strong>Tipo de trabajo: </strong>
                        {this.props.register.type_job}
                    </h6>
                    {
                        this.props.register.doi === null
                             ?
                            <h6><strong>DOI (Digital Object Identifier): </strong>NO REGISTRADO</h6>
                             :
                             this.isUrl(this.props.register.doi)
                                 ?
                                 <a target="_blank" rel="noopener noreferrer"
                                    href={this.props.register.doi}>
                                     <button className="btn btn-dark my-2">Ver DOI (Digital Object Identifier)</button>
                                 </a>
                                 :
                                 <h6>
                                     <strong>DOI (Digital Object Identifier): </strong> {this.props.register.doi}
                                 </h6>
                    }
                    {
                        this.props.register.file !== null
                            ?
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.register.file}`}>
                                <strong>Ver Probatorio: </strong>
                            </a>
                            :
                            <h6><strong>Probatorio: </strong> PENDIENTE </h6>
                    }

                    {
                        this.props.permissions.includes('authentication.admin_production')
                        |
                        this.props.permissions.includes('authentication.admin_root')
                            ?
                            <button onClick={this.onEnableEdit} className="btn btn-primary float-right">
                                Editar
                            </button>
                            :
                            null
                    }
                </div>
            )
        }

        return (
            <div className="modal fade" id="viewTeacherTraining" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                <div className={`${this.state.edit ? 'modal-lg' : 'modal-md'} modal-dialog`} role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title " id="exampleModalLongTitle">
                                Publicaciones en Revistas
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={this.onEditFalse}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-dark">
                            {
                                this.state.edit
                                    ?
                                    <FormElectronicJournals
                                        obj={this.props.register}
                                        edit={this.state.edit}
                                        onEnableEdit={this.onEnableEdit}
                                    />
                                    :
                                    detailView
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, null  )(ViewElectronicJournals);