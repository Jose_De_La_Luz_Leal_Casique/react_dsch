import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import FormUnpublishedLectures from "../forms/FormUnpublishedLectures";
import {SERVER} from "../../../../actions/server";


export class ViewUnpublishedLectures extends Component {

    state = {
        edit: false,
    }

    static propTypes = {
        register: PropTypes.object,
        get_full_name: PropTypes.func.isRequired,
        permissions: PropTypes.array
    }

    onEnableEdit = (e) => this.setState({edit: this.state.edit ? false : true});
    onEditFalse = (e) => this.setState({edit: false});

    render() {
        let detailView = null;

        if (this.props.register != null){
            detailView = (
                <div className="col-11 m-auto">
                    <h6>
                        <strong>Profesor (s): </strong>
                        <ul>
                        {this.props.register.profile.map((user, index) =>(
                            <li key={user.id}>{this.props.get_full_name(user)}</li>
                        ))}
                        </ul>
                    </h6>
                    <h6><strong>Título de Conferencia: </strong>
                        {this.props.register.title}
                    </h6>
                    <h6><strong>Conferencia presentado en: </strong>
                        {this.props.register.present}
                    </h6>
                    <h6><strong>Nombre de congreso: </strong>
                        { this.props.register.congress === null
                            ? 'NO REGISTRADO' : this.props.register.congress }
                    </h6>
                    <h6><strong>Cuidad: </strong>
                        { this.props.register.city === null
                            ? 'NO REGISTRADO' : this.props.register.city }
                    </h6>
                    <h6><strong>País: </strong>
                        { this.props.register.country === null
                            ? 'NO REGISTRADO' : this.props.register.country }
                    </h6>
                    <h6><strong>Institución: </strong>
                        { this.props.register.institute === null
                            ? 'NO REGISTRADO' : this.props.register.institute }
                    </h6>
                    <h6><strong>Tipo de Trabajo: </strong>
                        {this.props.register.type_job}
                    </h6>
                    {
                        this.props.register.file !== null
                            ?
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.register.file}`}>
                                <strong>Ver Probatorio: </strong>
                            </a>
                            :
                            <h6><strong>Probatorio: </strong> PENDIENTE </h6>
                    }

                    {
                        this.props.permissions.includes('authentication.admin_production')
                        |
                        this.props.permissions.includes('authentication.admin_root')
                            ?
                            <button onClick={this.onEnableEdit} className="btn btn-primary float-right">
                                Editar
                            </button>
                            :
                            null
                    }
                </div>
            )
        }

        return (
            <div className="modal fade" id="viewTeacherTraining" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                <div className={`${this.state.edit ? 'modal-lg' : 'modal-md'} modal-dialog`} role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title " id="exampleModalLongTitle">
                                Conferencias Publicadas
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close"  onClick={this.onEditFalse} >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-dark">
                            {
                                this.state.edit
                                    ?
                                    <FormUnpublishedLectures
                                        obj={this.props.register}
                                        edit={this.state.edit}
                                        onEnableEdit={this.onEnableEdit}
                                    />
                                    :
                                    detailView
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, null  )(ViewUnpublishedLectures);
