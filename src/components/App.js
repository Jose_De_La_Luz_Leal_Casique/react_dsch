import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Provider } from 'react-redux';
import store from '../store';
import Alerts from './layout/Alerts';

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import PrivateRoute from './common/PrivateRoute';
import StatisticsRoute from './common/StatisticsRoute';
import ScholarshipRoute from "./common/ScholarshipRoute";
import EditorialRoute from "./common/EditorialRoute";
import ProductionRoute from "./common/ProductionRoute";
import AdjustmentsRoute from "./common/AdjustmentsRoute";
import ConcourseChairRoute from "./common/ConcourseChairRoute";
import ConcourseVisitorRoute from "./common/ConcourseVisitorRoute";
import ConcourseOppositionRoute from "./common/ConcourseOppositionRoute";
import ConcourseCurricularRoute from "./common/ConcourseCurricularRoute";

import Header from './layout/Header';
import Dashboard from "./layout/Dashboard";
import NotFound from './layout/NotFound';

import AcademicTrimester from './academic/AcademicTrimester';
import Academic from "./academic/Academic";
import AcademicEdit from "./academic/AcademicEdit";
import Report from "./academic/Report";

import Scholarship from "./scholarship/Scholarship";
import ScholarshipYear from "./scholarship/ScholarshipYear";
import ScholarshipNumber from "./scholarship/ScholarshipNumber";
import ScholarshipEdit from "./scholarship/ScholarshipEdit";

import Editorial from "./editorial/Editorial";
import ListRequestEditorial from "./editorial/ListRequestEditorial";
import EditorialEdit from "./editorial/EditorialEdit";
import EditorialEditView from "./editorial/components/EditorialEditView";

import ConcourseEdit from "./concourse/ConcourseEdit";
import Curricular from "./concourse_curricular/Curricular";
import ListCurricular from "./concourse_curricular/ListCurricular";
import Opposition from "./concourse_opposition/Opposition";
import ListOpposition from "./concourse_opposition/ListOpposition";
import Chair from "./concourse_chair/Chair";
import ListChair from "./concourse_chair/ListChair";
import Visitor from "./concourse_visitor/Visitor";
import ListVisitor from "./concourse_visitor/ListVisitor";

import Production from "./production/Production";
import CreateReport from "./production/CreateReport";

import Adjustments from "./adjustments/Adjustments";
import Edit from "./adjustments/components/Edit";

import ViewProject from "./Investigation/ViewProject";

import Login from './accounts/Login';
import Reset from './accounts/Reset';
import Permissions from "./accounts/Permissions";

import ViewHistoryAcademic from './generals/ViewHistoryAcademic';
import Personal from "./generals/Personal";
import ViewListRequestBook from "./generals/ViewListRequestBook";
import ViewReportPersonal from "./generals/ViewReportPersonal";

// investigation
import ViewHistoryProjectsInvestigation from "./generals/ViewHistoryProjectsInvestigation";
import NewProject from "./generals/investigation/NewProject";
import HistoryProject from "./Investigation/HistoryProject";
import ApprovedProject from "./Investigation/ApprovedProjects";
import ProjectRequest from "./Investigation/ProjectRequest";
import ReportsToInvestigation from "./Investigation/ReportsToInvestigation";

// sabbatical
import Sabbatical from "./sabbatical/Sabbatical";
import RequestSabbatical from "./sabbatical/RequestSabbatical";
import ApprovedSabbatical from "./sabbatical/ApprovedSabbatical";
import CreateSabbatical from "./sabbatical/sabbatical/CreateSabbatical";
import ViewSabbatical from "./sabbatical/ViewSabbatical";

import loadUser from '../actions/accounts/loadUser';


// config alerts
const alertOptions = {
    timeout: 3000,
    position: 'top center'
};


class App extends Component {

    componentDidMount() {
        store.dispatch(loadUser());
    }

    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={AlertTemplate} {...alertOptions}>
                    <Router>
                        <Fragment>
                            <Header />
                            <Alerts />
                            <div className="container-fluid">
                                <Switch>
                                    <PrivateRoute exact path="/" component={Dashboard} />
                                    <PrivateRoute exact path="/reset-password"
                                                  component={Reset} />
                                    <PrivateRoute exact path="/profile"
                                                  component={Personal} />
                                    <PrivateRoute exact path="/permissions"
                                                  component={Permissions} />
                                    <PrivateRoute exact path="/view-history-academic"
                                                  component={ViewHistoryAcademic} />
                                    <PrivateRoute exact path="/view-history-editorial"
                                                  component={ViewListRequestBook} />
                                    <PrivateRoute exact path="/reports"
                                                  component={ViewReportPersonal} />

                                    <PrivateRoute exact path="/project-list"
                                                  component={ViewHistoryProjectsInvestigation} />
                                    <PrivateRoute exact path="/pre-project"
                                                  component={NewProject} />
                                    <PrivateRoute exact path="/view-project"
                                                  component={ViewProject} />
                                    <PrivateRoute exact path={"/project-investigation-history"}
                                                  component={HistoryProject} />
                                    <PrivateRoute exact path={"/project-approved"}
                                                  component={ApprovedProject} />
                                    <PrivateRoute exact path={"/project-request"}
                                                  component={ProjectRequest} />
                                    <PrivateRoute exact path={"/reports-investigation"}
                                                  component={ReportsToInvestigation} />

                                    <PrivateRoute exact path={"/sabbatical"}
                                                  component={Sabbatical} />
                                    <PrivateRoute exact path={"/view-sabbatical"}
                                                  component={ViewSabbatical} />
                                    <PrivateRoute exact path={"/approved-sabbatical"}
                                                  component={ApprovedSabbatical} />
                                    <PrivateRoute exact path={"/request-sabbatical"}
                                                  component={RequestSabbatical} />
                                    <PrivateRoute exact path={"/new-sabbatical"}
                                                  component={CreateSabbatical} />

                                    <StatisticsRoute exact path="/academic"
                                                     component={Academic} />
                                    <StatisticsRoute exact path="/academic-edit"
                                                     component={AcademicEdit} />
                                    <StatisticsRoute exact path="/academic-trimester"
                                                     component={AcademicTrimester} />
                                    <StatisticsRoute exact path="/academic-totals"
                                                     component={Report} />

                                    <ScholarshipRoute exact path="/scholarship"
                                                      component={Scholarship} />
                                    <ScholarshipRoute exact path="/scholarship-edit"
                                                      component={ScholarshipEdit} />
                                    <ScholarshipRoute exact path="/scholarship-year"
                                                      component={ScholarshipYear} />
                                    <ScholarshipRoute exact path="/scholarship-number"
                                                      component={ScholarshipNumber} />

                                    <EditorialRoute exac path="/editorial"
                                                    component={Editorial} />
                                    <EditorialRoute exac path="/editorial-list"
                                                    component={ListRequestEditorial} />
                                    <EditorialRoute exac path="/editorial-edit"
                                                    component={EditorialEdit} />
                                    <EditorialRoute exac path="/editorial-view"
                                                    component={EditorialEditView} />

                                    <ConcourseCurricularRoute exact path="/curricular"
                                                    component={Curricular} />
                                    <ConcourseCurricularRoute exact path="/curricular-list"
                                                    component={ListCurricular} />
                                    <PrivateRoute exact path="/concourse-edit"
                                                    component={ConcourseEdit} />

                                    <ConcourseOppositionRoute exact path="/opposition"
                                                    component={Opposition} />
                                    <ConcourseOppositionRoute exact path="/opposition-list"
                                                    component={ListOpposition} />

                                    <ConcourseChairRoute exact path="/chair"
                                                    component={Chair} />
                                    <ConcourseChairRoute exact path="/chair-list"
                                                    component={ListChair} />

                                    <ConcourseVisitorRoute exact path="/visitor"
                                                    component={Visitor} />
                                    <ConcourseVisitorRoute exact path="/visitor-list"
                                                    component={ListVisitor} />

                                    <ProductionRoute exact path="/production"
                                                    component={Production} />
                                    <ProductionRoute exact path="/production-file"
                                                    component={CreateReport} />

                                    <AdjustmentsRoute exact path="/adjustments"
                                                    component={Adjustments} />
                                    <AdjustmentsRoute exact path="/adjustments-edit"
                                                    component={Edit} />

                                    <Route exact path="/login" component={Login} />
                                    <Route path="*" component={NotFound} />
                                </Switch>
                            </div>
                        </Fragment>
                    </Router>
                </AlertProvider>
            </Provider>
        );
    }
}

export default App;
