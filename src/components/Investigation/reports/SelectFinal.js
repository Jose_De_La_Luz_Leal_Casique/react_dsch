import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";



export class SelectFinal extends Component {

    static propTypes = {
        years: PropTypes.object.isRequired,
        select_final: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    }

    render() {
        return (
                this.props.years.load
                    ?
                    <select name="final" className="form-control"
                            value={this.props.select_final} onChange={this.props.onChange}>
                        {
                            this.props.years.years.map((obj, index) => (
                                <option key={index} value={obj.year} > {obj.year} </option>
                            ))
                        }
                    </select>
                    :
                    <section>
                        loading...
                    </section>
        )
    }

}

const mapStateToProps = (state) => ({
    years: state.years
});

export default connect(mapStateToProps, null )(SelectFinal);