import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";



export class SelectInitial extends Component {

    static propTypes = {
        years: PropTypes.object.isRequired,
        select_initial: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    }

    render() {
        return (
                this.props.years.load
                    ?
                    <select name="initial" className="form-control"
                            value={this.props.select_initial} onChange={this.props.onChange}>
                        {
                            this.props.years.years.map((obj, index) => (
                                <option key={index} value={obj.year} > {obj.year} </option>
                            ))
                        }
                    </select>
                    :
                    <section>
                        loading...
                    </section>
        )
    }

}

const mapStateToProps = (state) => ({
    years: state.years
});

export default connect(mapStateToProps, null )(SelectInitial);