import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {SERVER} from "../../../../actions/server";
import {FormatDate} from "../../../FormatDate";
import {
    terminate_project_investigation
} from "../../../../actions/investigation/terminate_project_investigation";


export class Information extends Component{

    static propTypes = {
        letters: PropTypes.array,
        obj: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
        onRedirectFromList: PropTypes.func.isRequired,
        terminate_project_investigation: PropTypes.func.isRequired,
    }

    checkPermission = () => {
        if (
            this.props.permissions.includes('investigation.admin_evaluation_commission')
                |
            this.props.permissions.includes('investigation.admin_leader_departament')
        ){
            return true;
        }
        return false;
    }

    onTerminateProjectInvestigation = () => {
        this.props.terminate_project_investigation(this.props.obj.id);
        setTimeout(this.props.onRedirectFromList, 600);
    }

    render() {
        return (
            <section className="col-12 ml-3 text-center m-0 p-0">
                <section className="card py-2">
                    <section className="col-12 filter-section-dsch-multi-form">
                        <section className="col-9 text-right">
                            <h4 className="py-2 mt-1">
                                <strong>
                                    Información General del Proyecto
                                </strong>
                            </h4>
                        </section>
                        <section className="col-3 ml-auto py-2">
                            {
                                this.props.obj.status === 'Aceptado' & this.checkPermission()
                                    ?
                                    <button className="float-right btn btn-dsch"
                                            onClick={this.onTerminateProjectInvestigation}>
                                        Finalizar Proyecto
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </section>
                    <section className="filter-section-dsch-multi-form col-12 my-1 mt-4">
                        <div className="col-3">
                            <h6><strong>Envío de Pre-Solicitud</strong></h6>
                            <h6 >{FormatDate(this.props.obj.send)}</h6>
                        </div>
                        <div className="col-3">
                            <h6><strong>Inicio de Proyecto</strong></h6>
                            <h6 >{FormatDate(this.props.obj.initial)}</h6>
                        </div>
                        <div className="col-3">
                            <h6><strong>Fin de Proyecto</strong></h6>
                            <h6 >{FormatDate(this.props.obj.fin)}</h6>
                        </div>
                        <div className="col-3">
                            <h6><strong>Duración de Proyecto </strong></h6>
                            <h6 >{this.props.obj.duration} años</h6>
                        </div>
                    </section>
                </section>

                <section className="card py-2 mt-3">
                    <h5 className="py-1">
                        <strong>
                            Formatos generales del proyecto
                        </strong>
                    </h5>
                    <section className="filter-section-dsch-multi-form col-12 my-1">
                        <div className="card alert text-center py-1 mt-0 mb-0 col-6">
                            <a className="btn btn-dark px-4"
                               href={
                                   this.props.pk !== 0
                                       ?
                                       `${SERVER}/investigation/pdf/${this.props.obj.id}/1/`
                                       :
                                       "#"
                               } target={this.props.pk !== 0 ? "_blank" : ''}
                                       rel="noopener noreferrer">
                                Ver formato de solicitud del proyecto
                            </a>
                        </div>
                        <div className="card alert text-center py-2 mt-0 mb-0 col-6">
                            <a href={`${SERVER}${this.props.obj.file}`} className="btn btn-dark"
                               target={"_blank"} rel="noopener noreferrer" >
                                Ver resolución por comisión evaluadora
                            </a>
                        </div>
                    </section>
                </section>

                <section className="card py-2 mt-3">
                    <h5 className="py-1">
                        <strong>
                            Cartas agregadas por departamentos
                        </strong>
                    </h5>
                    <section className="filter-section-dsch-multi-form col-12 my-1">
                        {
                            this.props.letters.length > 0
                                ?
                            this.props.letters.map((obj, index) => (
                                <div className="card alert text-center py-1 mt-0 mb-0 col-4 mx-auto"
                                     key={index}>
                                    <a href={`${SERVER}${obj.file}`}
                                       className="btn btn-dark"
                                       target={"_blank"}
                                       rel="noopener noreferrer" >
                                        Ver {obj.departament}
                                    </a>
                                </div>
                            ))
                                :
                                <section className="alert alert-danger col-12">
                                    loading...
                                </section>
                        }

                    </section>
                </section>
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    letters: state.investigation.letters,
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, { terminate_project_investigation })(Information);