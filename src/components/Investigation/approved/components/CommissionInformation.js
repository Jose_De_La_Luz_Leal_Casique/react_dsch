import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {ConfigDateSave, FormatDate} from "../../../FormatDate";
import { add_file_to_accept } from "../../../../actions/investigation/add_file_to_accept";
import {
    add_information_additional
} from "../../../../actions/investigation/add_information_additional";
import {SERVER} from "../../../../actions/server";

export class CommissionInformation extends Component{

    state = {
        register: '',
        session: '',
        agreement: '',
        created: '',
        file: null
    }

    static propTypes = {
        obj: PropTypes.object.isRequired,
        permissions: PropTypes.array,
        onRedirectFromList: PropTypes.func.isRequired,
        add_information_additional: PropTypes.func.isRequired,
        add_file_to_accept: PropTypes.func.isRequired,
    }


    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            session: this.state.session,
            register: this.state.register,
            agreement: this.state.agreement,
            created: ConfigDateSave(this.state.created)
        })
        this.props.add_information_additional(this.props.obj.id, data)
        window.setTimeout(this.props.onRedirectFromList, 600);
    }

    onSubmitFile = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('file', this.state.file, this.state.file.name)
        this.props.add_file_to_accept(data, this.props.obj.id);
        window.setTimeout(this.props.onRedirectFromList, 600);
    }

    render() {

        if (this.props.obj.additional === null) {

            return (
                <section className="col-12 ml-3 text-center m-0 p-0">
                    <section className="card py-2">
                        <h5 className="py-1">
                            <strong>
                                Información General del proyecto para aceptación
                            </strong>
                        </h5>
                    </section>

                    <section className="card py-4 mt-3 px-2">
                        {
                            this.props.permissions.includes(
                                'investigation.admin_evaluation_commission')
                                ?
                                <form className="col-11 alert mb-0 alert-warning mx-auto"
                                      onSubmit={this.onSubmit}>
                                    <h5>
                                        <strong>
                                            Registro de información para aprobación de proyecto
                                        </strong>
                                    </h5>
                                    <section className="col-12 filter-section-dsch-multi-form">
                                        <div className="col-6 mx-auto ">
                                            <h6 className="ml-1 mt-3 text-left">
                                                <strong>No. de Registro de Proyecto: </strong>
                                            </h6>
                                            <input type="text" className="form-control"
                                                   name="register" required
                                                   value={this.state.register}
                                                   onChange={this.onChange}
                                            />
                                        </div>

                                        <div className="col-6 mx-auto ">
                                            <h6 className="ml-1 mt-3 text-left">
                                                <strong>
                                                    No. De Sesión en la que fue aprobado:
                                                </strong>
                                            </h6>
                                            <input type="text" className="form-control"
                                                   name="session" required
                                                   value={this.state.session}
                                                   onChange={this.onChange}
                                            />
                                        </div>
                                    </section>

                                    <section className="filter-section-dsch-multi-form col-12">
                                        <div className="col-6 mx-auto ">
                                            <h6 className="ml-1 mt-3 text-left">
                                                <strong>
                                                    No. De Acuerdo con el que se aprobó:
                                                </strong>
                                            </h6>
                                            <input type="text" className="form-control"
                                                   name="agreement" required
                                                   value={this.state.agreement}
                                                   onChange={this.onChange}
                                            />
                                        </div>

                                        <div className="col-4 mx-auto ">
                                            <h6 className="ml-1 mt-3 text-left">
                                                <strong>Fecha de aprobación: </strong>
                                            </h6>
                                            <input type="date" className="form-control"
                                                   name="created" required
                                                   value={this.state.created}
                                                   onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="col-2 mx-auto pt-4">
                                            <button className="btn btn-warning mt-3">
                                                Guardar
                                            </button>
                                        </div>
                                    </section>

                                </form>
                                :
                                <section className="alert alert-danger py-4">
                                    <h5>
                                        <strong>
                                            Esta información no ha sido agregada
                                        </strong>
                                    </h5>
                                </section>
                        }
                    </section>

                </section>
            )
        } else {
            return (
                <section className="card px-2 py-3 text-center">
                    <h5 className="mt-2">
                        <strong>
                            Información Proporcionada por Oficina Técnica de Consejo Divisional
                        </strong>
                    </h5>
                    <table className="table mt-3">
                        <thead className="thead-dark">
                        <tr>
                            <th>No. de Registro de Proyecto</th>
                            <th>No. De Sesión en la que fue aprobado</th>
                            <th>No. De Acuerdo con el que se aprobó</th>
                            <th >Fecha de aprobación</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr className="alert-warning">
                            <td>
                                {this.props.obj.additional.register}
                            </td>
                            <td>
                                {this.props.obj.additional.session}
                            </td>
                            <td>
                                {this.props.obj.additional.agreement}
                            </td>
                            <td>
                                {FormatDate(this.props.obj.additional.created)}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <section className="mt-2 col-12 my-4">
                        {
                            this.props.obj.file_accept === null
                                ?
                                this.props.permissions.includes(
                                    'investigation.admin_evaluation_commission')
                                    ?
                                    <form className="col-12 alert mb-0 alert-warning mx-auto"
                                          onSubmit={this.onSubmitFile}>
                                        <h5>
                                            <strong>
                                                Registro de oficio de aprobación de proyecto
                                            </strong>
                                        </h5>
                                        <section
                                            className="filter-section-dsch-multi-form col-12 my-2">
                                            <div className="col-1 mx-auto "></div>
                                            <div className="col-6 mx-auto ">
                                                <input type="file" className="form-control"
                                                       name="file" required
                                                       value={this.file}
                                                       onChange={this.onChangeFile}
                                                       accept="application/pdf"
                                                />
                                            </div>
                                            <div className="col-3 mx-auto pt-1">
                                                <button className="btn btn-warning">
                                                    Guardar Oficio
                                                </button>
                                            </div>
                                            <div className="col-1 mx-auto "></div>
                                        </section>
                                    </form>
                                    :
                                    <section className="py-4 ">
                                        <strong>
                                            Oficio de aprobación pendiente
                                        </strong>
                                    </section>
                                :
                                <div className="col-12 bg-dark mb-4">
                                    <a href={`${SERVER}${this.props.obj.file_accept}`}
                                       className="btn btn-dark float-right"
                                       target={"_blank"} rel="noopener noreferrer" >
                                        Ver oficio de aprobación
                                    </a>
                                </div>
                        }
                    </section>
                </section>
            )
        }
    }

}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { add_information_additional, add_file_to_accept })(CommissionInformation);