import React from "react";

export default class NotView extends React.Component {

    render() {
        return(
            <section className="card-body py-5 alert alert-danger text-center ml-4">
                <h4>
                    <strong>
                        Sin Opción a mostrar (No tiene permisos para ver esto).
                    </strong>
                </h4>
            </section>
        )
    }

}