import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalDataPresupuestal from "./ModalDataPresupuestal";
import FormEditPresupuestal from "./FormEditPresupuestal";


export  class ListPresupuestal extends Component{

    state = {
        obj: null,
        edit: null
    }


    static propTypes = {
        presupuestal: PropTypes.array,
        permissions: PropTypes.array.isRequired,
    }

    onSendObjToView = (obj) => this.setState({ obj: obj });

    onSendObjToEdit = (obj) => {
        alert('Editar: ' + obj.code);
        // this.setState({edit: obj});
    }

    render() {
        return (
            <section className="col-12 card pb-0 pt-3">
                {
                    this.props.presupuestal.length > 0
                        ?
                        <table className="table table-striped">
                            <thead className="text-center">
                            <tr>
                                <th width={129}> Estructura</th>
                                <th width={198}>Partida SubEspecífica</th>
                                <th width={346}>Nombre de Partida</th>
                                <th width={150}>Monto</th>
                                <th width={55}></th>
                            </tr>
                            </thead>
                            <tbody className="text-center table-scroll-presupuestal">
                            {
                                this.props.presupuestal.map((obj, index) => (
                                    <tr key={index}>
                                        <td width={130} className="pt-4">{ obj.code }</td>
                                        <td width={200} className="pt-4">{ obj.sub_code }</td>
                                        <td width={350} className="pt-4">{ obj.name }</td>
                                        <td width={150} className="pt-4">$ { obj.amount }.00</td>
                                        <td width={40}>
                                            <div className="btn-group dropleft">
                                                <button type="button"
                                                        className="btn btn-secondary dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                </button>
                                                <div className="dropdown-menu">
                                                    { /*
                                                        this.props.permissions.includes(
                                                            'investigation.admin_leader_departament')
                                                            ?
                                                            <a className="dropdown-item"
                                                               href="#"
                                                               data-toggle="modal"
                                                               data-target="#modal-Edit-Presupuestal"
                                                               onClick={
                                                                   this.onSendObjToEdit.bind(
                                                                       this, obj)}>
                                                                Editar Registro
                                                            </a>
                                                            :
                                                            null
                                                    */}
                                                    <a className="dropdown-item"
                                                       href="#"
                                                       data-toggle="modal"
                                                       data-target="#modal-Data-Presupuestal"
                                                       onClick={
                                                           this.onSendObjToView.bind(
                                                               this, obj)}>
                                                        Ver información completa
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                        :
                        <section className="col-10 mx-auto alert alert-danger py-4">
                            <h5>
                                <strong>
                                    SIN REGISTRO DE ASIGNACIÓN DE PRESUPUESTO
                                </strong>
                            </h5>
                        </section>
                }
                <ModalDataPresupuestal obj={this.state.obj}/>
                {/*
                    this.state.edit !== null
                        ? <FormEditPresupuestal obj={this.state.edit}/> : null
                */}
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    presupuestal: state.investigation.presupuestal
});

export default connect(mapStateToProps, null)(ListPresupuestal);