import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { FormatDate } from "../../../../FormatDate";
import {SERVER} from "../../../../../actions/server";


export  class ListAdvance extends Component{

    state = {
        obj: null
    }


    static propTypes = {
        advance: PropTypes.array
    }

    onSendObjToView = (obj) => this.setState({ obj: obj });

    render() {
        return (
            <section className="col-12 card pb-0 pt-3">
                {
                    this.props.advance.length > 0
                        ?
                        <table className="table table-striped">
                            <thead className="text-center">
                            <tr>
                                <th width={200}>Fecha de Registro</th>
                                <th width={200}>Informe de proyecto</th>
                                <th width={250}>Archivo de probatorios</th>
                                <th width={230}>Tipo de Informe</th>
                            </tr>
                            </thead>
                            <tbody className="text-center table-scroll-presupuestal">
                            {
                                this.props.advance.map((obj, index) => (
                                    <tr key={index}>
                                        <td className="pt-4" width={200}>
                                            { FormatDate(obj.updated) }
                                        </td>
                                        <td className="pt-4" width={200}>
                                            Informe de { obj.advance ? 'Conclusión' : 'Avance' }
                                        </td>

                                        <td width={250}>
                                            <a href={`${SERVER}${obj.zip}`}
                                               className="btn btn-dark float-right"
                                               target={"_blank"} rel="noopener noreferrer" >
                                                Ver archivo con probatorios
                                            </a>
                                        </td>
                                        <td width={230}>
                                            <a href={`${SERVER}${obj.file}`}
                                               className="btn btn-dark float-right"
                                               target={"_blank"} rel="noopener noreferrer" >
                                                Ver informe registrado
                                            </a>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                        :
                        <section className="col-10 mx-auto alert alert-danger py-4">
                            <h5>
                                <strong>
                                    SIN REGISTRO DE INFORMES
                                </strong>
                            </h5>
                        </section>
                }
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    advance: state.investigation.advance
});

export default connect(mapStateToProps, null)(ListAdvance);