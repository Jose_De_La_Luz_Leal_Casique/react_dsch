import React, { Component } from "react";
import PropTypes from "prop-types";
import {ConfigDateSave, FormatDate} from "../../../../FormatDate";


export default class FormPresupuestal extends Component{

    static propTypes = {
        onChange: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        onChangeFile: PropTypes.func.isRequired,
        obj: PropTypes.object,
        pk: PropTypes.number,
        code: PropTypes.string,
        sub_code: PropTypes.string,
        name: PropTypes.string,
        amount: PropTypes.string,
        date: PropTypes.string,
        assign: PropTypes.string,
        observations: PropTypes.string,
        file: PropTypes.any,
    }

    render() {

        return (
            <form onSubmit={this.props.onSubmit} id="form-presupuestal" className="modal-body">
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-3 mx-auto">
                        <label className="ml-1">
                            <strong>Estructura</strong>
                        </label>
                        <input type="text" className="form-control" name="code"
                               value={this.props.code} onChange={this.props.onChange}
                               maxLength={9} minLength={8} required/>
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1">
                            <strong>Partida Subespecífica</strong>
                        </label>
                        <input type="text" className="form-control" name="sub_code"
                               value={this.props.sub_code} onChange={this.props.onChange}
                               maxLength={8} minLength={7} required/>
                    </div>

                    <div className="col-5 mx-auto">
                        <label className="ml-1">
                            <strong>Nombre de subpartida</strong>
                        </label>
                        <input type="text" className="form-control" name="name"
                               value={this.props.name} onChange={this.props.onChange} required />
                    </div>
                </div>

                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-3 mx-auto">
                        <label className="ml-1"><strong>Monto</strong></label>
                        <input type="text" className="form-control" name="amount"
                               value={this.props.amount} onChange={this.props.onChange} required />
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1">
                            <strong>Fecha de asignación</strong>
                        </label>
                        <input type="date" className="form-control" name="date"
                               value={this.props.date} onChange={this.props.onChange} required />
                        <div className="ml-2 text-danger">
                            <small>
                                {
                                    this.props.date !== ''
                                        ?
                                        <strong>
                                            Fecha: {
                                            FormatDate(ConfigDateSave(this.props.date))
                                        }
                                        </strong>
                                        :
                                        null
                                }
                            </small>
                        </div>
                    </div>
                    <div className="col-5 mx-auto">
                        <label className="ml-1">
                            <strong>Forma de asignación</strong>
                        </label>
                        <select className="form-control" name="assign"
                                value={this.props.assign} onChange={this.props.onChange}>
                            <option key={1}>Directa de Jefe de Departamento</option>
                            <option key={2}>Por Comisión de Presupuesto</option>
                        </select>
                    </div>
                </div>

                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <label className="ml-1">
                            <strong>Documento de asignación</strong>
                        </label>
                        <input type="file" className="form-control" name="file"
                               value={this.file} accept="application/pdf"
                               onChange={this.props.onChangeFile} required />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1">
                            <strong>Observaciones</strong>
                        </label>
                        <textarea className="form-control" name="observations"  rows={3}
                                  value={this.props.observations} onChange={this.props.onChange}
                                  required />
                    </div>
                </div>
                <div className="bg-form-presupuestal py-3 m-form-footer">
                    <button type="reset" className="btn btn-dark ml-auto"
                            data-dismiss="modal" onClick={this.onClearToForm}>
                        Cerrar y limpiar información
                    </button>
                    <button type="submit" className="btn btn-light ml-5">
                        Guardar presupuesto
                    </button>
                </div>
            </form>
        )
    }

}
