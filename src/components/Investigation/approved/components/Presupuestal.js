import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ListPresupuestal from "./components/ListPresupuestal";
import ModalPresupuestal from "./components/ModalPresupuestal";
import { add_presupuestal } from "../../../../actions/investigation/add_presupuestal";


export class Presupuestal extends Component{

    static propTypes = {
        presupuestal: PropTypes.array,
        permissions: PropTypes.array.isRequired,
        pk: PropTypes.number,
        add_presupuestal: PropTypes.func.isRequired,

    }

    render() {
        return (
            <section className="col-12 ml-3 text-center m-0 p-0">
                <section className="card pt-2 pb-0">
                    <section className="col-12 filter-section-dsch-multi-form">
                        <section className="col-8 text-right">
                            <h4 className="py-2 mt-1">
                                <strong>
                                    Asignación Presupuestal
                                </strong>
                            </h4>
                        </section>
                        <section className="col-3 ml-auto py-2">
                            {
                                this.props.permissions.includes(
                                    'investigation.admin_leader_departament')
                                    ?
                                    <button className="float-right btn btn-dsch"
                                            data-toggle="modal"
                                            data-target="#modal-Presupuestal">
                                        Agregar presupuesto
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </section>
                </section>
                <ListPresupuestal permissions={this.props.permissions}/>
                <ModalPresupuestal
                    add_presupuestal={this.props.add_presupuestal}
                    pk={this.props.pk}
                />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_presupuestal })(Presupuestal);