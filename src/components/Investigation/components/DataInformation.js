import React, { Component } from "react";
import PropTypes from "prop-types";
import {SERVER} from "../../../actions/server";


export class ViewOptionsProcess extends Component {

    static propTypes = {
        pk: PropTypes.number,
        obj: PropTypes.object,
    }

    render() {
        return (
            <section className="col-3 ml-4 card mt-2 py-3">

                <div className={`${
                    this.props.obj.status === 'Aceptado' ? 'alert-success' : 'alert-danger'
                } alert text-center`
                }>
                    <h5><strong>Proyecto { this.props.obj.status }</strong></h5>
                </div>

                <div className="card py-3 px-4 text-center mt-3">
                    <h6>Oficio de justificación de decisión</h6>
                    <a className="btn btn-dark mt-2"
                       href={`${SERVER}${this.props.obj.file }`}
                       target="_blank"
                       rel="noopener noreferrer">
                        <h5>
                            <strong>Ver Oficio</strong>
                        </h5>
                    </a>
                </div>
            </section>
        )
    }
}

export default ViewOptionsProcess;