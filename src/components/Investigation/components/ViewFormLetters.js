import React, { Component } from "react";
import {FormatDate} from "../../FormatDate";
import {SERVER} from "../../../actions/server";
import PropTypes from "prop-types";


export class ViewFormLetters extends Component {

    static propTypes = {
        user: PropTypes.object,
        obj: PropTypes.object,
        letters: PropTypes.array,
        onLoadFileLetter: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        permissions: PropTypes.array,
        edit: PropTypes.bool,
        btnCancel: PropTypes.bool,
        departament: PropTypes.string
    }


    render() {
        return (
            <section className="col-5 ml-4 card mt-2 py-3">
                {
                    this.props.permissions.includes('investigation.admin_leader_departament')
                        &
                    !this.props.obj.revision
                        ?
                        <section className="card py-3 mb-4 text-center">
                            <h5>
                                <strong>
                                    Cartas por departamento - {this.props.departament}
                                </strong>
                            </h5>

                            <form className="filter-section-dsch-multi-form mt-2"
                                  onSubmit={this.props.onSubmit} id="form-letter">
                                <input type="file" className="col-9 form-control"
                                       required onChange={this.props.onLoadFileLetter}
                                       accept="application/pdf"
                                />
                                <div className="col-3">
                                    <button className="btn btn-warning mt-1">
                                        Enviar
                                    </button>
                                </div>
                            </form>
                        </section>
                        :
                        null
                }
                {
                    this.props.letters.length > 0
                        ?
                        this.props.letters.map((obj, index) => (
                            <section key={index}
                                     className="alert alert-warning ">
                                <div className="filter-section-dsch-multi-form">
                                    <div className="col-10">
                                        <h6>
                                            <strong>
                                                Departamento de {obj.departament}
                                            </strong>
                                        </h6>
                                        <h6 className="ml-3">
                                            Fecha de Registro: {FormatDate(obj.updated)}
                                        </h6>
                                    </div>
                                    <div className="col-2">
                                        <a className="btn btn-dark"
                                           href={`${SERVER}${obj.file}`}
                                           target="_blank" rel="noopener noreferrer">
                                            Ver Carta
                                        </a>
                                    </div>
                                </div>
                            </section>
                        ))
                        :
                        <section className="alert alert-danger mt-3 py-3 text-center">
                            <strong>CARTAS NO AGREGADAS</strong>
                        </section>
                }
            </section>
        )
    }
}

export default ViewFormLetters;