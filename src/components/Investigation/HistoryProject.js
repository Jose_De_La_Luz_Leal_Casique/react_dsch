import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import DataInformation from "./approved/DataInformation";
import { get_presupuestal } from "../../actions/investigation/get_presupuestal";
import {
    get_letters_project_investigation
} from "../../actions/investigation/get_letters_project_investigation";



export class HistoryProject extends Component {

    state = {
        pk: 0,
        obj: null,
        option: 0,
    }

    static propTypes = {
        user: PropTypes.object.isRequired,
        letters: PropTypes.array,
        permissions: PropTypes.array,
        presupuestal: PropTypes.array,
        get_letters_project_investigation: PropTypes.func.isRequired,
        get_presupuestal: PropTypes.func.isRequired,
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({
            pk: params.pk,
            obj: params.obj
        });
        this.props.get_letters_project_investigation(params.pk);
        this.props.get_presupuestal(params.pk);
    }

    onRedirectFromList = () => {
        if (this.props.permissions.includes('investigation.admin_leader_departament') |
            this.props.permissions.includes('investigation.admin_evaluation_commission')
        ) {
            return this.props.history.push('/project-request');

        }else {
            return this.props.history.push('/project-list');
        }

    }

    render() {
        if (this.state.obj !== null) {
            return (
                <section className="mt-3 ">
                    <div className="col-12 mx-auto alert-warning alert text-center ">
                        <h4><strong>PROYECTO 2: </strong>{this.state.obj.title}</h4>
                    </div>
                    <section className="filter-section-dsch-multi-form col-12">
                        <DataInformation
                            pk={this.state.pk}
                            obj={this.state.obj}
                            letters={this.props.letters}
                            presupuestal={this.props.presupuestal}
                            permissions={this.props.permissions}
                            onRedirectFromList={this.onRedirectFromList}
                        />
                    </section>
                </section>
            )
        }
        else {
            return <section>loading...</section>
        }
    }

}


const mapStateToProps = (state) => ({
    user: state.auth.user,
    letters: state.investigation.letters,
    permissions: state.auth.permissions,
    presupuestal: state.investigation.presupuestal,
});

export default connect(
    mapStateToProps, { get_letters_project_investigation, get_presupuestal })(HistoryProject);