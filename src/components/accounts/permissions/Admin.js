import React, { Component } from "react";
import PropTypes from "prop-types";


export class Admin extends Component {

    state = {
        number: '',
        password: '',
    }

    static propTypes = {
        auth: PropTypes.object.isRequired,
        change_user_admin: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    GetNameUser = (obj) => {
        return (
            obj.user+ ": " + obj.name + " " + obj.paternal_surname +" " + obj.maternal_surname
        )
    }

    onSubmit = (e) => {
        e.preventDefault();
        let number = this.state.number;
        let password = this.state.password;
        let obj = this.props.auth.user;

        const data = JSON.stringify({number, password });
        this.props.change_user_admin(obj.user, data)
    }

    render() {
        if (this.props.auth !== null){
            return (
                <section className="col-12 py-4">
                    <div className="alert alert-danger text-center">
                        <h5 className="mt-3">
                            <strong>
                                ¡Antes de continuar tenga en cuenta los siguientes puntos!
                            </strong>
                        </h5>
                        <ul className="text-justify col-11 mx-auto"
                            style={
                                {'lineHeight': '140%'}
                            }>
                            <li className="my-2">
                                Esta función sólo se debe utilizar al concluir un ciclo
                                administrativo, ya que al cambiar permisos a otro usuario,
                                este tendrá control administrativo dentro del sistema.
                            </li>
                            <li className="my-2">
                                Esta pantalla se usa para sustituir al usuario administrador,
                                es decir, este usuario tendrá permisos para decidir que usuarios
                                podrán acceder y administrar los módulos existentes.
                            </li>
                        </ul>

                        <h5 className="mt-4">
                            <strong>
                                Para continuarse hará uso del formulario que se muestra en la
                                parte inferior.
                            </strong>
                        </h5>
                        <ul className="text-justify col-11 mx-auto"
                            style={
                                {'lineHeight': '140%'}
                            }>
                            <li className="my-2">
                                En el primer campo se escribirá el número económico del
                                usuario que será administrador.
                            </li>
                            <li className="my-2">
                                En el segundo campo, por seguridad y con la finalidad
                                de evitar que cualquier usuario pueda realizar esta delicada
                                modificación, se escribirá la contraseña del usuario administrador
                                actual <strong>({this.GetNameUser(this.props.auth.user)}).</strong>
                            </li>
                        </ul>
                    </div>

                    <form onSubmit={this.onSubmit}
                          className="card col-12 mt-4 alert-dark">
                        <div className="filter-section-dsch-multi-form card-body">
                            <div className="col-5">
                                <h6 className="ml-1">
                                    <strong>
                                        Nuevo Usuario Administrador:
                                    </strong>
                                </h6>
                                <input type="text" className="form-control" name="number"
                                       placeholder="Escribir No. Económico" minLength={4}
                                       value={this.state.number} onChange={this.onChange}
                                       required/>
                            </div>
                            <div className="col-5">
                                <h6 className="ml-1">
                                    <strong>
                                        Usuario Administrador Actual:
                                    </strong>
                                </h6>
                                <input type="password" className="form-control" name="password"
                                       placeholder="Escribir Contraseña" minLength={8}
                                       value={this.state.password} onChange={this.onChange}
                                       required/>
                            </div>
                            <div className="col-2 pt-4">
                                <button className="btn btn-dark mb-0">Enviar</button>
                            </div>
                        </div>
                    </form>
                </section>
            )
        } else {
            return (
                <section className="col-12 py-3 my-5">
                    <div className="alert alert-danger text-center py-5">
                        <h5>
                            <strong>
                                Cargando Información de Usuario Administrador
                            </strong>
                        </h5>
                    </div>
                </section>
            )
        }
    }

}


export default Admin;
