import React, { Component } from "react";
import PropTypes from "prop-types";

import CardUser from "./CardUser";


export class Commission extends Component {

    static propTypes = {
        privileges: PropTypes.object.isRequired,
    }

    render() {

        if (this.props.privileges.load){
            return (
                <section className="col-12 py-3">
                    <div className="filter-section-dsch-multi-form">
                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Consejo Editorial'}
                                option={7}
                                text={
                                    "Personal que podrá ver el historial de solicitudes " +
                                    "realizadas a editorial con el fin de dar seguimiento."
                                }
                                privileges={
                                    this.props.privileges.list.permission_device_editorial
                                }
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Comisión Evaluadora - Investigación'}
                                option={11}
                                text={
                                    "Personal que podrá ver el historial de solicitudes " +
                                    "realizadas a editorial con el fin de dar seguimiento."
                                }
                                privileges={
                                    this.props.privileges.list.permission_evaluation_commission
                                }
                            />
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="py-3">
                    <section
                        className="col-8 alert alert-warning mx-auto mt-5 py-5 text-center">
                        <h1><strong>loading...</strong></h1>
                    </section>
                </section>
            )
        }
    }

}


export default Commission;
