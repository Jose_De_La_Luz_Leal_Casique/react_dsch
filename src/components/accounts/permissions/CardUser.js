import React, {Component, Fragment} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { remove_permission } from "../../../actions/accounts/remove_permission";
import { assign_permission } from "../../../actions/accounts/assign_permission";
import { clear_permissions } from "../../../actions/accounts/clear_permissions";



export class CardUser extends Component {

    static propTypes = {
        module: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        option: PropTypes.number.isRequired,
        privileges: PropTypes.object,
        remove_permission: PropTypes.func.isRequired,
        assign_permission: PropTypes.func.isRequired,
        clear_permissions: PropTypes.func.isRequired,
    }

    getConfigPresentation = (obj) => {
        return obj.user + ': ' + obj.full_name;
    }

    assignPermission = (e) => {
        e.preventDefault();
        let user = window.prompt('Número económico de profesor');

        if (user === null){
            alert('NO SE AGREGÓ NÚMERO ECONÓMICO');
            return false;
        }
        let option = this.props.option;
        const data = JSON.stringify({user, option});
        this.props.assign_permission(data);
    }

    removePermission = (e) => {
        e.preventDefault();
        let user = this.props.privileges.user;
        let option = this.props.option;
        const data = JSON.stringify({user, option});
        this.props.remove_permission(data);
    }

    clearPermissions = (e) => {
        e.preventDefault();
        this.props.clear_permissions(this.props.privileges.user);
    }

    render() {
        return (
            <div className="card">
                <div className="card-header filter-section-dsch-multi-form">
                    <strong>{ this.props.module }</strong>
                    <div className="dropdown  dropleft  options-card">
                        <button className="dropdown-toggle dropdown-toggle-card"
                                type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        </button>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {
                                this.props.privileges !== null
                                    ?
                                    <a className="dropdown-item" style={{"cursor": "pointer"}}
                                       href="" onClick={this.removePermission}>
                                        Quitar permiso de { this.props.module }
                                    </a>
                                    :
                                    <a className="dropdown-item" style={{"cursor": "pointer"}}
                                       href="" onClick={this.assignPermission}>
                                        Dar permiso de { this.props.module }
                                    </a>
                            }
                            {
                                this.props.privileges !== null
                                    ?
                                    <Fragment>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item"
                                           style={{"cursor": "pointer"}}
                                           href="#" onClick={this.clearPermissions}>
                                            Quitar todos los permisos
                                        </a>
                                        </Fragment>
                                    :
                                    null
                            }
                        </div>
                    </div>
                </div>

                <div className="card-body" style={{"height": "90px", "marginBottom": "10px"}}>
                    <blockquote className=" mb-0">
                        <h6>
                            <strong>
                                {
                                    this.props.privileges !== null
                                        ?
                                        this.getConfigPresentation(this.props.privileges)
                                        :
                                        'Usuario No Asignado'
                                }
                            </strong>
                        </h6>
                        <div className="ml-1"
                             style={
                                 {
                                     "color": "rgba(0,0,0,.7)",
                                     "lineHeight": "90%",
                                     "textAlign": "justify"
                                 }
                             }>
                            <small>
                                { this.props.text }
                            </small>
                        </div>
                    </blockquote>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { remove_permission, assign_permission, clear_permissions }
)(CardUser);