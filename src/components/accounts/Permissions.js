import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import Modules from "./permissions/Modules";
import Leaders from "./permissions/Leaders";
import Commission from "./permissions/Commission";
import Admin from "./permissions/Admin";
import { get_permissions } from "../../actions/accounts/get_permissions";
import { change_user_admin } from "../../actions/accounts/change_user_admin";


export class Permissions extends Component {

    state = {
        view: 1
    }

    static propTypes = {
        get_permissions: PropTypes.func.isRequired,
        change_user_admin: PropTypes.func.isRequired,
        privileges: PropTypes.object.isRequired,
        auth: PropTypes.object.isRequired,
    }

    componentDidMount() {
        if (!this.props.privileges.load)this.props.get_permissions();
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    render() {

        let view = null;

        switch (parseInt(this.state.view)) {
            case 1:
                view = <Modules privileges={this.props.privileges}/>;
                break;

            case 2:
                view = <Leaders privileges={this.props.privileges} />;
                break;

            case 3:
                view = <Commission privileges={this.props.privileges} />;
                break;

            case 4:
                view = <Admin
                    auth={this.props.auth}
                    change_user_admin={this.props.change_user_admin}
                />
                break;

            default:
                view = null;

        }

        return (
            <section className="mt-3">
                {
                    this.props.privileges.load
                        ?
                        <section className="filter-section-dsch-multi-form">
                            <div className="col-4">
                                <section className="card text-center py-3 px-3">
                                    <h5>
                                        <strong>Permisos para accesos internos</strong>
                                    </h5>
                                    <div className="mt-2 alert alert-dark">
                                        <h6>
                                            <strong>Seleccionar opción para visualizar</strong>
                                        </h6>
                                        <select className="col-12 form-control" name="view"
                                                value={this.state.view} onChange={this.onChange}>
                                            <option value={1} key={1}>
                                                Permisos para acceder a módulos
                                            </option>
                                            <option value={2} key={2}>
                                                Permisos para jefes de departamento
                                            </option>
                                            <option value={3} key={3}>
                                                Permisos para comisiones
                                            </option>
                                            <option value={4} key={4}>
                                                Permisos de Administrador
                                            </option>
                                        </select>
                                    </div>

                                </section>
                            </div>
                            <div className="col-8 card">
                                {view}
                            </div>
                        </section>
                        :
                        <section
                            className="col-8 alert alert-warning mx-auto mt-5 py-5 text-center">
                            <h1><strong>loading...</strong></h1>
                        </section>
                }
            </section>
        )
    }

}


const mapStateToProps = (state) => ({
    privileges: state.privileges,
    auth: state.auth
});

export default connect(
    mapStateToProps, { get_permissions, change_user_admin }
)(Permissions);
