import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { createMessage } from '../../actions/messages';
import resetPassword from '../../actions/accounts/reset';


export class Reset extends Component{

    state = {
        password: '',
        password2: ''
    }

    static propTypes = {
        user: PropTypes.object,
        resetPassword: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();

        if (this.state.password !== this.state.password2){
            this.props.createMessage({ PasswordNotMatch: 'La Contraseñas No Coinciden' });

        } else{
            this.props.resetPassword(this.props.user.user, this.state.password);
        }
    };

    render() {

        const { password, password2 } = this.state;

        return (
            <section className="mt-1 row">
                <section className="col-sm-11 col-md-6 m-auto card m-dsch">
                    <p className="subtitle-dsch"><strong>Cambiar Contraseña</strong></p>
                    <form className="card-body" onSubmit={this.onSubmit}>

                        <div className="form-group group-dsch">
                            <label className="text-dsch-input">Nueva Contraseña:</label>
                            <input type="password" className="form-control" name="password" required
                                   onChange={this.onChange} value={password}/>
                        </div>

                        <div className="form-group group-dsch">
                            <label className="text-dsch-input">Confirmar Contraseña:</label>
                            <input type="password" className="form-control" name="password2" required
                                   onChange={this.onChange} value={password2}/>
                        </div>

                        <div className="form-group">
                            <button type="submit" className="btn-dsch float-right">
                                Acceder
                            </button>
                        </div>
                    </form>
                </section>
            </section>
        )
    }
};

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps, { createMessage, resetPassword })(Reset);
