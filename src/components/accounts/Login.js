import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import login from '../../actions/accounts/login';


export class Login extends Component {
    state = {
        number: '',
        password: ''
    };

    static propTypes = {
        isAuthenticated: PropTypes.bool,
        login: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.login(this.state.number, this.state.password);
    };

    render() {

        if (this.props.isAuthenticated){
            return <Redirect to="/" />;
        }

        const { number, password } = this.state;

        return (
            <section className="mt-1 row">
                <section className="col-sm-11 col-md-6 m-auto card m-dsch">
                    <p className="subtitle-dsch"><strong>Inicio de Sesión</strong></p>
                    <form className="card-body" onSubmit={this.onSubmit}>
                        <div className="form-group group-dsch">
                            <label className="text-dsch-input">Número Económico:</label>
                            <input type="text" className="form-control" name="number" required
                                   onChange={this.onChange} value={number}/>
                        </div>

                        <div className="form-group group-dsch">
                            <label className="text-dsch-input">Contraseña:</label>
                            <input type="password" className="form-control" name="password" required
                                   onChange={this.onChange} value={password}/>
                        </div>

                        <div className="form-group">
                            <button type="submit" className="btn-dsch float-right">
                                Acceder
                            </button>
                        </div>
                    </form>
                </section>
            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(Login);
