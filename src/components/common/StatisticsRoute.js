import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';


const StatisticsRoute = ({ component: Component, permissions, ...rest }) => (
    <Route
        {...rest}

        render={(props) =>{

            try {
                if (
                    permissions.includes('authentication.admin_academic') |
                    permissions.includes('authentication.admin_root') |
                    permissions.includes('authentication.view_academic_humanities') |
                    permissions.includes('authentication.view_academic_social_sciences') |
                    permissions.includes('authentication.view_academic_institutional_studies') |
                    permissions.includes('authentication.view_academic_general')
                ){
                    return <Component {...props} />

                } else{
                    return <Redirect to='/' />
                }

            } catch (e) {
                return <Redirect to='/' />
            }

        } }
    />
);

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, {})(StatisticsRoute);

