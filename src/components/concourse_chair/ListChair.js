import React, { Component } from 'react';
import { connect } from "react-redux";

import ConcourseList from "../concourse/ConcourseList";


export class ListChair extends  Component {

    render() {
        return (
            <ConcourseList type_concourse={2}/>
        )
    }
}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null )(ListChair);